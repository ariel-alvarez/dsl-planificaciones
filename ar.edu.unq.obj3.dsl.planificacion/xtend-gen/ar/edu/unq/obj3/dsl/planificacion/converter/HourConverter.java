package ar.edu.unq.obj3.dsl.planificacion.converter;

import java.util.List;
import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractLexerBasedConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class HourConverter extends DefaultTerminalConverters {
  @ValueConverter(rule = "HOUR")
  public IValueConverter<Integer> HOUR() {
    return new AbstractLexerBasedConverter<Integer>() {
      public String toString(final Integer value) throws ValueConverterException {
        String _plus = (Integer.valueOf(((value).intValue() / 60)) + ":");
        return (_plus + Integer.valueOf(((value).intValue() % 60)));
      }
      
      public Integer toValue(final String string, final INode node) throws ValueConverterException {
        int _xblockexpression = (int) 0;
        {
          String _replace = string.replace("\"", "");
          String[] _split = _replace.split(":");
          final List<String> horaMinuto = IterableExtensions.<String>toList(((Iterable<String>)Conversions.doWrapArray(_split)));
          int _size = horaMinuto.size();
          boolean _notEquals = (_size != 2);
          if (_notEquals) {
            throw new ValueConverterException("Formato de hora inválido, debe usar hh:mm", node, null);
          }
          String _get = horaMinuto.get(0);
          final int hora = Integer.parseInt(_get);
          String _get_1 = horaMinuto.get(1);
          final int minutos = Integer.parseInt(_get_1);
          if (((hora >= 24) || (hora < 0))) {
            throw new ValueConverterException("La hora debe ir de 0 a 23", node, null);
          }
          if (((minutos < 0) || (minutos >= 60))) {
            throw new ValueConverterException("Los minutos deben ser de 0 a 59", node, null);
          }
          _xblockexpression = ((hora * 60) + minutos);
        }
        return Integer.valueOf(_xblockexpression);
      }
    };
  }
}
