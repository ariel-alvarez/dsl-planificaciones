package ar.edu.unq.obj3.dsl.planificacion.validation;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Track;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;

@SuppressWarnings("all")
public class ActividadExtension {
  public Track getTrack(final Actividad actividad) {
    EObject _eContainer = actividad.eContainer();
    return ((Track) _eContainer);
  }
  
  public boolean tieneOradoresConMismasOrganizaciones(final Actividad actividad) {
    boolean _xblockexpression = false;
    {
      EList<Orador> _oradores = actividad.getOradores();
      final Function1<Orador, Organizacion> _function = new Function1<Orador, Organizacion>() {
        public Organizacion apply(final Orador it) {
          return it.getOrganizacion();
        }
      };
      final List<Organizacion> oradores = ListExtensions.<Orador, Organizacion>map(_oradores, _function);
      _xblockexpression = this.tieneRepetidos(oradores);
    }
    return _xblockexpression;
  }
  
  public Integer duracion(final Track track) {
    EList<Actividad> _actividades = track.getActividades();
    final Function1<Actividad, Integer> _function = new Function1<Actividad, Integer>() {
      public Integer apply(final Actividad it) {
        int _fin = it.getFin();
        int _comienzo = it.getComienzo();
        return Integer.valueOf((_fin - _comienzo));
      }
    };
    List<Integer> _map = ListExtensions.<Actividad, Integer>map(_actividades, _function);
    final Function2<Integer, Integer, Integer> _function_1 = new Function2<Integer, Integer, Integer>() {
      public Integer apply(final Integer x, final Integer y) {
        return Integer.valueOf(((x).intValue() + (y).intValue()));
      }
    };
    return IterableExtensions.<Integer>reduce(_map, _function_1);
  }
  
  public int horas(final Integer minutos) {
    return ((minutos).intValue() / 60);
  }
  
  public <T extends Object> ArrayList<T> operator_plus(final List<T> e1, final List<T> e2) {
    ArrayList<T> _xblockexpression = null;
    {
      final ArrayList<T> union = new ArrayList<T>();
      union.addAll(e1);
      union.addAll(e2);
      _xblockexpression = union;
    }
    return _xblockexpression;
  }
  
  public boolean tieneRepetidos(final List<?> e) {
    int _size = e.size();
    Set<?> _set = IterableExtensions.toSet(e);
    int _size_1 = _set.size();
    return (_size != _size_1);
  }
}
