/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Track</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getTitle <em>Title</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getEspacio <em>Espacio</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getActividades <em>Actividades</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getTrack()
 * @model
 * @generated
 */
public interface Track extends EObject
{
  /**
   * Returns the value of the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Title</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Title</em>' attribute.
   * @see #setTitle(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getTrack_Title()
   * @model
   * @generated
   */
  String getTitle();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getTitle <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Title</em>' attribute.
   * @see #getTitle()
   * @generated
   */
  void setTitle(String value);

  /**
   * Returns the value of the '<em><b>Espacio</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Espacio</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Espacio</em>' reference.
   * @see #setEspacio(Espacio)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getTrack_Espacio()
   * @model
   * @generated
   */
  Espacio getEspacio();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getEspacio <em>Espacio</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Espacio</em>' reference.
   * @see #getEspacio()
   * @generated
   */
  void setEspacio(Espacio value);

  /**
   * Returns the value of the '<em><b>Actividades</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Actividades</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Actividades</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getTrack_Actividades()
   * @model containment="true"
   * @generated
   */
  EList<Actividad> getActividades();

} // Track
