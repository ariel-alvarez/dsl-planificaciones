/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evento Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getTitle <em>Title</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getEspacios <em>Espacios</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOrganizaciones <em>Organizaciones</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOradores <em>Oradores</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getDias <em>Dias</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel()
 * @model
 * @generated
 */
public interface EventoModel extends EObject
{
  /**
   * Returns the value of the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Title</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Title</em>' attribute.
   * @see #setTitle(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel_Title()
   * @model
   * @generated
   */
  String getTitle();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getTitle <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Title</em>' attribute.
   * @see #getTitle()
   * @generated
   */
  void setTitle(String value);

  /**
   * Returns the value of the '<em><b>Espacios</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Espacios</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Espacios</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel_Espacios()
   * @model containment="true"
   * @generated
   */
  EList<Espacio> getEspacios();

  /**
   * Returns the value of the '<em><b>Organizaciones</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Organizaciones</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Organizaciones</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel_Organizaciones()
   * @model containment="true"
   * @generated
   */
  EList<Organizacion> getOrganizaciones();

  /**
   * Returns the value of the '<em><b>Oradores</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Oradores</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Oradores</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel_Oradores()
   * @model containment="true"
   * @generated
   */
  EList<Orador> getOradores();

  /**
   * Returns the value of the '<em><b>Dias</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dias</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dias</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEventoModel_Dias()
   * @model containment="true"
   * @generated
   */
  EList<Dia> getDias();

} // EventoModel
