/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Organizacion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl#getRazonSocial <em>Razon Social</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OrganizacionImpl extends MinimalEObjectImpl.Container implements Organizacion
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getRazonSocial() <em>Razon Social</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRazonSocial()
   * @generated
   * @ordered
   */
  protected static final String RAZON_SOCIAL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRazonSocial() <em>Razon Social</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRazonSocial()
   * @generated
   * @ordered
   */
  protected String razonSocial = RAZON_SOCIAL_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OrganizacionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.ORGANIZACION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ORGANIZACION__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getRazonSocial()
  {
    return razonSocial;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRazonSocial(String newRazonSocial)
  {
    String oldRazonSocial = razonSocial;
    razonSocial = newRazonSocial;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ORGANIZACION__RAZON_SOCIAL, oldRazonSocial, razonSocial));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORGANIZACION__NAME:
        return getName();
      case PlanificacionPackage.ORGANIZACION__RAZON_SOCIAL:
        return getRazonSocial();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORGANIZACION__NAME:
        setName((String)newValue);
        return;
      case PlanificacionPackage.ORGANIZACION__RAZON_SOCIAL:
        setRazonSocial((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORGANIZACION__NAME:
        setName(NAME_EDEFAULT);
        return;
      case PlanificacionPackage.ORGANIZACION__RAZON_SOCIAL:
        setRazonSocial(RAZON_SOCIAL_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORGANIZACION__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case PlanificacionPackage.ORGANIZACION__RAZON_SOCIAL:
        return RAZON_SOCIAL_EDEFAULT == null ? razonSocial != null : !RAZON_SOCIAL_EDEFAULT.equals(razonSocial);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", razonSocial: ");
    result.append(razonSocial);
    result.append(')');
    return result.toString();
  }

} //OrganizacionImpl
