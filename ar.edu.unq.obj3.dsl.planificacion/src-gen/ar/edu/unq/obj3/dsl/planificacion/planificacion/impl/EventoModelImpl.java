/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evento Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl#getEspacios <em>Espacios</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl#getOrganizaciones <em>Organizaciones</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl#getOradores <em>Oradores</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl#getDias <em>Dias</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EventoModelImpl extends MinimalEObjectImpl.Container implements EventoModel
{
  /**
   * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected static final String TITLE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected String title = TITLE_EDEFAULT;

  /**
   * The cached value of the '{@link #getEspacios() <em>Espacios</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEspacios()
   * @generated
   * @ordered
   */
  protected EList<Espacio> espacios;

  /**
   * The cached value of the '{@link #getOrganizaciones() <em>Organizaciones</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrganizaciones()
   * @generated
   * @ordered
   */
  protected EList<Organizacion> organizaciones;

  /**
   * The cached value of the '{@link #getOradores() <em>Oradores</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOradores()
   * @generated
   * @ordered
   */
  protected EList<Orador> oradores;

  /**
   * The cached value of the '{@link #getDias() <em>Dias</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDias()
   * @generated
   * @ordered
   */
  protected EList<Dia> dias;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EventoModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.EVENTO_MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTitle()
  {
    return title;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTitle(String newTitle)
  {
    String oldTitle = title;
    title = newTitle;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.EVENTO_MODEL__TITLE, oldTitle, title));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Espacio> getEspacios()
  {
    if (espacios == null)
    {
      espacios = new EObjectContainmentEList<Espacio>(Espacio.class, this, PlanificacionPackage.EVENTO_MODEL__ESPACIOS);
    }
    return espacios;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Organizacion> getOrganizaciones()
  {
    if (organizaciones == null)
    {
      organizaciones = new EObjectContainmentEList<Organizacion>(Organizacion.class, this, PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES);
    }
    return organizaciones;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Orador> getOradores()
  {
    if (oradores == null)
    {
      oradores = new EObjectContainmentEList<Orador>(Orador.class, this, PlanificacionPackage.EVENTO_MODEL__ORADORES);
    }
    return oradores;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Dia> getDias()
  {
    if (dias == null)
    {
      dias = new EObjectContainmentEList<Dia>(Dia.class, this, PlanificacionPackage.EVENTO_MODEL__DIAS);
    }
    return dias;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PlanificacionPackage.EVENTO_MODEL__ESPACIOS:
        return ((InternalEList<?>)getEspacios()).basicRemove(otherEnd, msgs);
      case PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES:
        return ((InternalEList<?>)getOrganizaciones()).basicRemove(otherEnd, msgs);
      case PlanificacionPackage.EVENTO_MODEL__ORADORES:
        return ((InternalEList<?>)getOradores()).basicRemove(otherEnd, msgs);
      case PlanificacionPackage.EVENTO_MODEL__DIAS:
        return ((InternalEList<?>)getDias()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.EVENTO_MODEL__TITLE:
        return getTitle();
      case PlanificacionPackage.EVENTO_MODEL__ESPACIOS:
        return getEspacios();
      case PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES:
        return getOrganizaciones();
      case PlanificacionPackage.EVENTO_MODEL__ORADORES:
        return getOradores();
      case PlanificacionPackage.EVENTO_MODEL__DIAS:
        return getDias();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.EVENTO_MODEL__TITLE:
        setTitle((String)newValue);
        return;
      case PlanificacionPackage.EVENTO_MODEL__ESPACIOS:
        getEspacios().clear();
        getEspacios().addAll((Collection<? extends Espacio>)newValue);
        return;
      case PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES:
        getOrganizaciones().clear();
        getOrganizaciones().addAll((Collection<? extends Organizacion>)newValue);
        return;
      case PlanificacionPackage.EVENTO_MODEL__ORADORES:
        getOradores().clear();
        getOradores().addAll((Collection<? extends Orador>)newValue);
        return;
      case PlanificacionPackage.EVENTO_MODEL__DIAS:
        getDias().clear();
        getDias().addAll((Collection<? extends Dia>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.EVENTO_MODEL__TITLE:
        setTitle(TITLE_EDEFAULT);
        return;
      case PlanificacionPackage.EVENTO_MODEL__ESPACIOS:
        getEspacios().clear();
        return;
      case PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES:
        getOrganizaciones().clear();
        return;
      case PlanificacionPackage.EVENTO_MODEL__ORADORES:
        getOradores().clear();
        return;
      case PlanificacionPackage.EVENTO_MODEL__DIAS:
        getDias().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.EVENTO_MODEL__TITLE:
        return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
      case PlanificacionPackage.EVENTO_MODEL__ESPACIOS:
        return espacios != null && !espacios.isEmpty();
      case PlanificacionPackage.EVENTO_MODEL__ORGANIZACIONES:
        return organizaciones != null && !organizaciones.isEmpty();
      case PlanificacionPackage.EVENTO_MODEL__ORADORES:
        return oradores != null && !oradores.isEmpty();
      case PlanificacionPackage.EVENTO_MODEL__DIAS:
        return dias != null && !dias.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (title: ");
    result.append(title);
    result.append(')');
    return result.toString();
  }

} //EventoModelImpl
