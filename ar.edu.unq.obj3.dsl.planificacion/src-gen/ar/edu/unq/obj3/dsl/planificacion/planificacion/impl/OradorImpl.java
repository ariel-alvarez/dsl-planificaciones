/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Orador</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl#getNombreCompleto <em>Nombre Completo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl#getOrganizacion <em>Organizacion</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OradorImpl extends MinimalEObjectImpl.Container implements Orador
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getNombreCompleto() <em>Nombre Completo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNombreCompleto()
   * @generated
   * @ordered
   */
  protected static final String NOMBRE_COMPLETO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNombreCompleto() <em>Nombre Completo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNombreCompleto()
   * @generated
   * @ordered
   */
  protected String nombreCompleto = NOMBRE_COMPLETO_EDEFAULT;

  /**
   * The cached value of the '{@link #getOrganizacion() <em>Organizacion</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOrganizacion()
   * @generated
   * @ordered
   */
  protected Organizacion organizacion;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OradorImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.ORADOR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ORADOR__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNombreCompleto()
  {
    return nombreCompleto;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNombreCompleto(String newNombreCompleto)
  {
    String oldNombreCompleto = nombreCompleto;
    nombreCompleto = newNombreCompleto;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ORADOR__NOMBRE_COMPLETO, oldNombreCompleto, nombreCompleto));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Organizacion getOrganizacion()
  {
    if (organizacion != null && organizacion.eIsProxy())
    {
      InternalEObject oldOrganizacion = (InternalEObject)organizacion;
      organizacion = (Organizacion)eResolveProxy(oldOrganizacion);
      if (organizacion != oldOrganizacion)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, PlanificacionPackage.ORADOR__ORGANIZACION, oldOrganizacion, organizacion));
      }
    }
    return organizacion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Organizacion basicGetOrganizacion()
  {
    return organizacion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOrganizacion(Organizacion newOrganizacion)
  {
    Organizacion oldOrganizacion = organizacion;
    organizacion = newOrganizacion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ORADOR__ORGANIZACION, oldOrganizacion, organizacion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORADOR__NAME:
        return getName();
      case PlanificacionPackage.ORADOR__NOMBRE_COMPLETO:
        return getNombreCompleto();
      case PlanificacionPackage.ORADOR__ORGANIZACION:
        if (resolve) return getOrganizacion();
        return basicGetOrganizacion();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORADOR__NAME:
        setName((String)newValue);
        return;
      case PlanificacionPackage.ORADOR__NOMBRE_COMPLETO:
        setNombreCompleto((String)newValue);
        return;
      case PlanificacionPackage.ORADOR__ORGANIZACION:
        setOrganizacion((Organizacion)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORADOR__NAME:
        setName(NAME_EDEFAULT);
        return;
      case PlanificacionPackage.ORADOR__NOMBRE_COMPLETO:
        setNombreCompleto(NOMBRE_COMPLETO_EDEFAULT);
        return;
      case PlanificacionPackage.ORADOR__ORGANIZACION:
        setOrganizacion((Organizacion)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ORADOR__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case PlanificacionPackage.ORADOR__NOMBRE_COMPLETO:
        return NOMBRE_COMPLETO_EDEFAULT == null ? nombreCompleto != null : !NOMBRE_COMPLETO_EDEFAULT.equals(nombreCompleto);
      case PlanificacionPackage.ORADOR__ORGANIZACION:
        return organizacion != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", nombreCompleto: ");
    result.append(nombreCompleto);
    result.append(')');
    return result.toString();
  }

} //OradorImpl
