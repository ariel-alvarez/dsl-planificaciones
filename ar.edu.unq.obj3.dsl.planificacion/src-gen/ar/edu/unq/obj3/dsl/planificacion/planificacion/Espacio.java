/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Espacio</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getCapacidad <em>Capacidad</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#isLaboratorio <em>Laboratorio</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEspacio()
 * @model
 * @generated
 */
public interface Espacio extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEspacio_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Capacidad</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Capacidad</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Capacidad</em>' attribute.
   * @see #setCapacidad(int)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEspacio_Capacidad()
   * @model
   * @generated
   */
  int getCapacidad();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getCapacidad <em>Capacidad</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Capacidad</em>' attribute.
   * @see #getCapacidad()
   * @generated
   */
  void setCapacidad(int value);

  /**
   * Returns the value of the '<em><b>Laboratorio</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Laboratorio</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Laboratorio</em>' attribute.
   * @see #setLaboratorio(boolean)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getEspacio_Laboratorio()
   * @model
   * @generated
   */
  boolean isLaboratorio();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#isLaboratorio <em>Laboratorio</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Laboratorio</em>' attribute.
   * @see #isLaboratorio()
   * @generated
   */
  void setLaboratorio(boolean value);

} // Espacio
