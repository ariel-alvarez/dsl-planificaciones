/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actividad</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getComienzo <em>Comienzo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getFin <em>Fin</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isMesa <em>Mesa</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isCharla <em>Charla</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isTaller <em>Taller</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getTitulo <em>Titulo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOradores <em>Oradores</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOyentes <em>Oyentes</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad()
 * @model
 * @generated
 */
public interface Actividad extends EObject
{
  /**
   * Returns the value of the '<em><b>Comienzo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Comienzo</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Comienzo</em>' attribute.
   * @see #setComienzo(int)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Comienzo()
   * @model
   * @generated
   */
  int getComienzo();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getComienzo <em>Comienzo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Comienzo</em>' attribute.
   * @see #getComienzo()
   * @generated
   */
  void setComienzo(int value);

  /**
   * Returns the value of the '<em><b>Fin</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Fin</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fin</em>' attribute.
   * @see #setFin(int)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Fin()
   * @model
   * @generated
   */
  int getFin();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getFin <em>Fin</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fin</em>' attribute.
   * @see #getFin()
   * @generated
   */
  void setFin(int value);

  /**
   * Returns the value of the '<em><b>Mesa</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mesa</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mesa</em>' attribute.
   * @see #setMesa(boolean)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Mesa()
   * @model
   * @generated
   */
  boolean isMesa();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isMesa <em>Mesa</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mesa</em>' attribute.
   * @see #isMesa()
   * @generated
   */
  void setMesa(boolean value);

  /**
   * Returns the value of the '<em><b>Charla</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Charla</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Charla</em>' attribute.
   * @see #setCharla(boolean)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Charla()
   * @model
   * @generated
   */
  boolean isCharla();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isCharla <em>Charla</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Charla</em>' attribute.
   * @see #isCharla()
   * @generated
   */
  void setCharla(boolean value);

  /**
   * Returns the value of the '<em><b>Taller</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Taller</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Taller</em>' attribute.
   * @see #setTaller(boolean)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Taller()
   * @model
   * @generated
   */
  boolean isTaller();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isTaller <em>Taller</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Taller</em>' attribute.
   * @see #isTaller()
   * @generated
   */
  void setTaller(boolean value);

  /**
   * Returns the value of the '<em><b>Titulo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Titulo</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Titulo</em>' attribute.
   * @see #setTitulo(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Titulo()
   * @model
   * @generated
   */
  String getTitulo();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getTitulo <em>Titulo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Titulo</em>' attribute.
   * @see #getTitulo()
   * @generated
   */
  void setTitulo(String value);

  /**
   * Returns the value of the '<em><b>Oradores</b></em>' reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Oradores</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Oradores</em>' reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Oradores()
   * @model
   * @generated
   */
  EList<Orador> getOradores();

  /**
   * Returns the value of the '<em><b>Oyentes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Oyentes</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Oyentes</em>' attribute.
   * @see #setOyentes(int)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getActividad_Oyentes()
   * @model
   * @generated
   */
  int getOyentes();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOyentes <em>Oyentes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Oyentes</em>' attribute.
   * @see #getOyentes()
   * @generated
   */
  void setOyentes(int value);

} // Actividad
