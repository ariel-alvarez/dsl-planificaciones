/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionFactory
 * @model kind="package"
 * @generated
 */
public interface PlanificacionPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "planificacion";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.edu.ar/unq/obj3/dsl/planificacion/Planificacion";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "planificacion";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  PlanificacionPackage eINSTANCE = ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl.init();

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl <em>Evento Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getEventoModel()
   * @generated
   */
  int EVENTO_MODEL = 0;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL__TITLE = 0;

  /**
   * The feature id for the '<em><b>Espacios</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL__ESPACIOS = 1;

  /**
   * The feature id for the '<em><b>Organizaciones</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL__ORGANIZACIONES = 2;

  /**
   * The feature id for the '<em><b>Oradores</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL__ORADORES = 3;

  /**
   * The feature id for the '<em><b>Dias</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL__DIAS = 4;

  /**
   * The number of structural features of the '<em>Evento Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENTO_MODEL_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.DiaImpl <em>Dia</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.DiaImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getDia()
   * @generated
   */
  int DIA = 1;

  /**
   * The feature id for the '<em><b>Dia</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIA__DIA = 0;

  /**
   * The feature id for the '<em><b>Bloques</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIA__BLOQUES = 1;

  /**
   * The number of structural features of the '<em>Dia</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DIA_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl <em>Espacio</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getEspacio()
   * @generated
   */
  int ESPACIO = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ESPACIO__NAME = 0;

  /**
   * The feature id for the '<em><b>Capacidad</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ESPACIO__CAPACIDAD = 1;

  /**
   * The feature id for the '<em><b>Laboratorio</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ESPACIO__LABORATORIO = 2;

  /**
   * The number of structural features of the '<em>Espacio</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ESPACIO_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl <em>Track</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getTrack()
   * @generated
   */
  int TRACK = 3;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRACK__TITLE = 0;

  /**
   * The feature id for the '<em><b>Espacio</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRACK__ESPACIO = 1;

  /**
   * The feature id for the '<em><b>Actividades</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRACK__ACTIVIDADES = 2;

  /**
   * The number of structural features of the '<em>Track</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TRACK_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl <em>Actividad</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getActividad()
   * @generated
   */
  int ACTIVIDAD = 4;

  /**
   * The feature id for the '<em><b>Comienzo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__COMIENZO = 0;

  /**
   * The feature id for the '<em><b>Fin</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__FIN = 1;

  /**
   * The feature id for the '<em><b>Mesa</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__MESA = 2;

  /**
   * The feature id for the '<em><b>Charla</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__CHARLA = 3;

  /**
   * The feature id for the '<em><b>Taller</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__TALLER = 4;

  /**
   * The feature id for the '<em><b>Titulo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__TITULO = 5;

  /**
   * The feature id for the '<em><b>Oradores</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__ORADORES = 6;

  /**
   * The feature id for the '<em><b>Oyentes</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD__OYENTES = 7;

  /**
   * The number of structural features of the '<em>Actividad</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVIDAD_FEATURE_COUNT = 8;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl <em>Organizacion</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getOrganizacion()
   * @generated
   */
  int ORGANIZACION = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORGANIZACION__NAME = 0;

  /**
   * The feature id for the '<em><b>Razon Social</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORGANIZACION__RAZON_SOCIAL = 1;

  /**
   * The number of structural features of the '<em>Organizacion</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORGANIZACION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl <em>Orador</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getOrador()
   * @generated
   */
  int ORADOR = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORADOR__NAME = 0;

  /**
   * The feature id for the '<em><b>Nombre Completo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORADOR__NOMBRE_COMPLETO = 1;

  /**
   * The feature id for the '<em><b>Organizacion</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORADOR__ORGANIZACION = 2;

  /**
   * The number of structural features of the '<em>Orador</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ORADOR_FEATURE_COUNT = 3;


  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel <em>Evento Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Evento Model</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel
   * @generated
   */
  EClass getEventoModel();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getTitle()
   * @see #getEventoModel()
   * @generated
   */
  EAttribute getEventoModel_Title();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getEspacios <em>Espacios</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Espacios</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getEspacios()
   * @see #getEventoModel()
   * @generated
   */
  EReference getEventoModel_Espacios();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOrganizaciones <em>Organizaciones</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Organizaciones</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOrganizaciones()
   * @see #getEventoModel()
   * @generated
   */
  EReference getEventoModel_Organizaciones();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOradores <em>Oradores</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Oradores</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getOradores()
   * @see #getEventoModel()
   * @generated
   */
  EReference getEventoModel_Oradores();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getDias <em>Dias</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Dias</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel#getDias()
   * @see #getEventoModel()
   * @generated
   */
  EReference getEventoModel_Dias();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia <em>Dia</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Dia</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia
   * @generated
   */
  EClass getDia();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getDia <em>Dia</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dia</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getDia()
   * @see #getDia()
   * @generated
   */
  EAttribute getDia_Dia();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getBloques <em>Bloques</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Bloques</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getBloques()
   * @see #getDia()
   * @generated
   */
  EReference getDia_Bloques();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio <em>Espacio</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Espacio</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio
   * @generated
   */
  EClass getEspacio();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getName()
   * @see #getEspacio()
   * @generated
   */
  EAttribute getEspacio_Name();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getCapacidad <em>Capacidad</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Capacidad</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#getCapacidad()
   * @see #getEspacio()
   * @generated
   */
  EAttribute getEspacio_Capacidad();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#isLaboratorio <em>Laboratorio</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Laboratorio</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio#isLaboratorio()
   * @see #getEspacio()
   * @generated
   */
  EAttribute getEspacio_Laboratorio();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track <em>Track</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Track</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Track
   * @generated
   */
  EClass getTrack();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getTitle()
   * @see #getTrack()
   * @generated
   */
  EAttribute getTrack_Title();

  /**
   * Returns the meta object for the reference '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getEspacio <em>Espacio</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Espacio</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getEspacio()
   * @see #getTrack()
   * @generated
   */
  EReference getTrack_Espacio();

  /**
   * Returns the meta object for the containment reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getActividades <em>Actividades</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Actividades</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Track#getActividades()
   * @see #getTrack()
   * @generated
   */
  EReference getTrack_Actividades();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad <em>Actividad</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Actividad</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad
   * @generated
   */
  EClass getActividad();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getComienzo <em>Comienzo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Comienzo</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getComienzo()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Comienzo();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getFin <em>Fin</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Fin</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getFin()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Fin();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isMesa <em>Mesa</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mesa</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isMesa()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Mesa();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isCharla <em>Charla</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Charla</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isCharla()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Charla();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isTaller <em>Taller</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Taller</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#isTaller()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Taller();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getTitulo <em>Titulo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Titulo</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getTitulo()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Titulo();

  /**
   * Returns the meta object for the reference list '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOradores <em>Oradores</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Oradores</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOradores()
   * @see #getActividad()
   * @generated
   */
  EReference getActividad_Oradores();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOyentes <em>Oyentes</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Oyentes</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad#getOyentes()
   * @see #getActividad()
   * @generated
   */
  EAttribute getActividad_Oyentes();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion <em>Organizacion</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Organizacion</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion
   * @generated
   */
  EClass getOrganizacion();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getName()
   * @see #getOrganizacion()
   * @generated
   */
  EAttribute getOrganizacion_Name();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getRazonSocial <em>Razon Social</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Razon Social</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getRazonSocial()
   * @see #getOrganizacion()
   * @generated
   */
  EAttribute getOrganizacion_RazonSocial();

  /**
   * Returns the meta object for class '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador <em>Orador</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Orador</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador
   * @generated
   */
  EClass getOrador();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getName()
   * @see #getOrador()
   * @generated
   */
  EAttribute getOrador_Name();

  /**
   * Returns the meta object for the attribute '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getNombreCompleto <em>Nombre Completo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nombre Completo</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getNombreCompleto()
   * @see #getOrador()
   * @generated
   */
  EAttribute getOrador_NombreCompleto();

  /**
   * Returns the meta object for the reference '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getOrganizacion <em>Organizacion</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Organizacion</em>'.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getOrganizacion()
   * @see #getOrador()
   * @generated
   */
  EReference getOrador_Organizacion();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  PlanificacionFactory getPlanificacionFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl <em>Evento Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EventoModelImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getEventoModel()
     * @generated
     */
    EClass EVENTO_MODEL = eINSTANCE.getEventoModel();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EVENTO_MODEL__TITLE = eINSTANCE.getEventoModel_Title();

    /**
     * The meta object literal for the '<em><b>Espacios</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENTO_MODEL__ESPACIOS = eINSTANCE.getEventoModel_Espacios();

    /**
     * The meta object literal for the '<em><b>Organizaciones</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENTO_MODEL__ORGANIZACIONES = eINSTANCE.getEventoModel_Organizaciones();

    /**
     * The meta object literal for the '<em><b>Oradores</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENTO_MODEL__ORADORES = eINSTANCE.getEventoModel_Oradores();

    /**
     * The meta object literal for the '<em><b>Dias</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EVENTO_MODEL__DIAS = eINSTANCE.getEventoModel_Dias();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.DiaImpl <em>Dia</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.DiaImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getDia()
     * @generated
     */
    EClass DIA = eINSTANCE.getDia();

    /**
     * The meta object literal for the '<em><b>Dia</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DIA__DIA = eINSTANCE.getDia_Dia();

    /**
     * The meta object literal for the '<em><b>Bloques</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DIA__BLOQUES = eINSTANCE.getDia_Bloques();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl <em>Espacio</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getEspacio()
     * @generated
     */
    EClass ESPACIO = eINSTANCE.getEspacio();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ESPACIO__NAME = eINSTANCE.getEspacio_Name();

    /**
     * The meta object literal for the '<em><b>Capacidad</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ESPACIO__CAPACIDAD = eINSTANCE.getEspacio_Capacidad();

    /**
     * The meta object literal for the '<em><b>Laboratorio</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ESPACIO__LABORATORIO = eINSTANCE.getEspacio_Laboratorio();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl <em>Track</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getTrack()
     * @generated
     */
    EClass TRACK = eINSTANCE.getTrack();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TRACK__TITLE = eINSTANCE.getTrack_Title();

    /**
     * The meta object literal for the '<em><b>Espacio</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRACK__ESPACIO = eINSTANCE.getTrack_Espacio();

    /**
     * The meta object literal for the '<em><b>Actividades</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TRACK__ACTIVIDADES = eINSTANCE.getTrack_Actividades();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl <em>Actividad</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getActividad()
     * @generated
     */
    EClass ACTIVIDAD = eINSTANCE.getActividad();

    /**
     * The meta object literal for the '<em><b>Comienzo</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__COMIENZO = eINSTANCE.getActividad_Comienzo();

    /**
     * The meta object literal for the '<em><b>Fin</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__FIN = eINSTANCE.getActividad_Fin();

    /**
     * The meta object literal for the '<em><b>Mesa</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__MESA = eINSTANCE.getActividad_Mesa();

    /**
     * The meta object literal for the '<em><b>Charla</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__CHARLA = eINSTANCE.getActividad_Charla();

    /**
     * The meta object literal for the '<em><b>Taller</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__TALLER = eINSTANCE.getActividad_Taller();

    /**
     * The meta object literal for the '<em><b>Titulo</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__TITULO = eINSTANCE.getActividad_Titulo();

    /**
     * The meta object literal for the '<em><b>Oradores</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ACTIVIDAD__ORADORES = eINSTANCE.getActividad_Oradores();

    /**
     * The meta object literal for the '<em><b>Oyentes</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ACTIVIDAD__OYENTES = eINSTANCE.getActividad_Oyentes();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl <em>Organizacion</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OrganizacionImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getOrganizacion()
     * @generated
     */
    EClass ORGANIZACION = eINSTANCE.getOrganizacion();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORGANIZACION__NAME = eINSTANCE.getOrganizacion_Name();

    /**
     * The meta object literal for the '<em><b>Razon Social</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORGANIZACION__RAZON_SOCIAL = eINSTANCE.getOrganizacion_RazonSocial();

    /**
     * The meta object literal for the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl <em>Orador</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.OradorImpl
     * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionPackageImpl#getOrador()
     * @generated
     */
    EClass ORADOR = eINSTANCE.getOrador();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORADOR__NAME = eINSTANCE.getOrador_Name();

    /**
     * The meta object literal for the '<em><b>Nombre Completo</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ORADOR__NOMBRE_COMPLETO = eINSTANCE.getOrador_NombreCompleto();

    /**
     * The meta object literal for the '<em><b>Organizacion</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ORADOR__ORGANIZACION = eINSTANCE.getOrador_Organizacion();

  }

} //PlanificacionPackage
