/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Espacio</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl#getCapacidad <em>Capacidad</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.EspacioImpl#isLaboratorio <em>Laboratorio</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EspacioImpl extends MinimalEObjectImpl.Container implements Espacio
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getCapacidad() <em>Capacidad</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCapacidad()
   * @generated
   * @ordered
   */
  protected static final int CAPACIDAD_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getCapacidad() <em>Capacidad</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCapacidad()
   * @generated
   * @ordered
   */
  protected int capacidad = CAPACIDAD_EDEFAULT;

  /**
   * The default value of the '{@link #isLaboratorio() <em>Laboratorio</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLaboratorio()
   * @generated
   * @ordered
   */
  protected static final boolean LABORATORIO_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isLaboratorio() <em>Laboratorio</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isLaboratorio()
   * @generated
   * @ordered
   */
  protected boolean laboratorio = LABORATORIO_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EspacioImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.ESPACIO;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ESPACIO__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getCapacidad()
  {
    return capacidad;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCapacidad(int newCapacidad)
  {
    int oldCapacidad = capacidad;
    capacidad = newCapacidad;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ESPACIO__CAPACIDAD, oldCapacidad, capacidad));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isLaboratorio()
  {
    return laboratorio;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLaboratorio(boolean newLaboratorio)
  {
    boolean oldLaboratorio = laboratorio;
    laboratorio = newLaboratorio;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ESPACIO__LABORATORIO, oldLaboratorio, laboratorio));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ESPACIO__NAME:
        return getName();
      case PlanificacionPackage.ESPACIO__CAPACIDAD:
        return getCapacidad();
      case PlanificacionPackage.ESPACIO__LABORATORIO:
        return isLaboratorio();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ESPACIO__NAME:
        setName((String)newValue);
        return;
      case PlanificacionPackage.ESPACIO__CAPACIDAD:
        setCapacidad((Integer)newValue);
        return;
      case PlanificacionPackage.ESPACIO__LABORATORIO:
        setLaboratorio((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ESPACIO__NAME:
        setName(NAME_EDEFAULT);
        return;
      case PlanificacionPackage.ESPACIO__CAPACIDAD:
        setCapacidad(CAPACIDAD_EDEFAULT);
        return;
      case PlanificacionPackage.ESPACIO__LABORATORIO:
        setLaboratorio(LABORATORIO_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ESPACIO__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case PlanificacionPackage.ESPACIO__CAPACIDAD:
        return capacidad != CAPACIDAD_EDEFAULT;
      case PlanificacionPackage.ESPACIO__LABORATORIO:
        return laboratorio != LABORATORIO_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", capacidad: ");
    result.append(capacidad);
    result.append(", laboratorio: ");
    result.append(laboratorio);
    result.append(')');
    return result.toString();
  }

} //EspacioImpl
