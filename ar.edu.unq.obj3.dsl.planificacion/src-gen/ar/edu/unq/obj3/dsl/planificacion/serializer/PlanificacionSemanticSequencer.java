package ar.edu.unq.obj3.dsl.planificacion.serializer;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.EventoModel;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Track;
import ar.edu.unq.obj3.dsl.planificacion.services.PlanificacionGrammarAccess;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class PlanificacionSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private PlanificacionGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == PlanificacionPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case PlanificacionPackage.ACTIVIDAD:
				if(context == grammarAccess.getActividadRule()) {
					sequence_Actividad(context, (Actividad) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.DIA:
				if(context == grammarAccess.getDiaRule()) {
					sequence_Dia(context, (Dia) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.ESPACIO:
				if(context == grammarAccess.getEspacioRule()) {
					sequence_Espacio(context, (Espacio) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.EVENTO_MODEL:
				if(context == grammarAccess.getEventoModelRule()) {
					sequence_EventoModel(context, (EventoModel) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.ORADOR:
				if(context == grammarAccess.getOradorRule()) {
					sequence_Orador(context, (Orador) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.ORGANIZACION:
				if(context == grammarAccess.getOrganizacionRule()) {
					sequence_Organizacion(context, (Organizacion) semanticObject); 
					return; 
				}
				else break;
			case PlanificacionPackage.TRACK:
				if(context == grammarAccess.getTrackRule()) {
					sequence_Track(context, (Track) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (
	 *         comienzo=HOUR 
	 *         fin=HOUR 
	 *         (mesa?='mesa' | charla?='charla' | taller?='taller') 
	 *         titulo=STRING 
	 *         oradores+=[Orador|ID]+ 
	 *         oyentes=INT
	 *     )
	 */
	protected void sequence_Actividad(EObject context, Actividad semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (dia=STRING bloques+=Track+)
	 */
	protected void sequence_Dia(EObject context, Dia semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID capacidad=INT laboratorio?='con computadoras'?)
	 */
	protected void sequence_Espacio(EObject context, Espacio semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (title=STRING espacios+=Espacio+ organizaciones+=Organizacion+ oradores+=Orador+ dias+=Dia+)
	 */
	protected void sequence_EventoModel(EObject context, EventoModel semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID nombreCompleto=STRING organizacion=[Organizacion|ID])
	 */
	protected void sequence_Orador(EObject context, Orador semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, PlanificacionPackage.Literals.ORADOR__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PlanificacionPackage.Literals.ORADOR__NAME));
			if(transientValues.isValueTransient(semanticObject, PlanificacionPackage.Literals.ORADOR__NOMBRE_COMPLETO) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PlanificacionPackage.Literals.ORADOR__NOMBRE_COMPLETO));
			if(transientValues.isValueTransient(semanticObject, PlanificacionPackage.Literals.ORADOR__ORGANIZACION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PlanificacionPackage.Literals.ORADOR__ORGANIZACION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOradorAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOradorAccess().getNombreCompletoSTRINGTerminalRuleCall_3_0(), semanticObject.getNombreCompleto());
		feeder.accept(grammarAccess.getOradorAccess().getOrganizacionOrganizacionIDTerminalRuleCall_4_0_1(), semanticObject.getOrganizacion());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID razonSocial=STRING)
	 */
	protected void sequence_Organizacion(EObject context, Organizacion semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, PlanificacionPackage.Literals.ORGANIZACION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PlanificacionPackage.Literals.ORGANIZACION__NAME));
			if(transientValues.isValueTransient(semanticObject, PlanificacionPackage.Literals.ORGANIZACION__RAZON_SOCIAL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, PlanificacionPackage.Literals.ORGANIZACION__RAZON_SOCIAL));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOrganizacionAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOrganizacionAccess().getRazonSocialSTRINGTerminalRuleCall_3_0(), semanticObject.getRazonSocial());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (title=STRING espacio=[Espacio|ID] actividades+=Actividad+)
	 */
	protected void sequence_Track(EObject context, Track semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
