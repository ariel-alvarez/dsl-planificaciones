/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dia</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getDia <em>Dia</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getBloques <em>Bloques</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getDia()
 * @model
 * @generated
 */
public interface Dia extends EObject
{
  /**
   * Returns the value of the '<em><b>Dia</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dia</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dia</em>' attribute.
   * @see #setDia(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getDia_Dia()
   * @model
   * @generated
   */
  String getDia();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Dia#getDia <em>Dia</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dia</em>' attribute.
   * @see #getDia()
   * @generated
   */
  void setDia(String value);

  /**
   * Returns the value of the '<em><b>Bloques</b></em>' containment reference list.
   * The list contents are of type {@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Track}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bloques</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bloques</em>' containment reference list.
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getDia_Bloques()
   * @model containment="true"
   * @generated
   */
  EList<Track> getBloques();

} // Dia
