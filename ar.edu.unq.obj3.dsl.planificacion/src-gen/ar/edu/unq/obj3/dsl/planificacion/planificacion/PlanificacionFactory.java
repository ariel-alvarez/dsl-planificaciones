/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage
 * @generated
 */
public interface PlanificacionFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  PlanificacionFactory eINSTANCE = ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.PlanificacionFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Evento Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Evento Model</em>'.
   * @generated
   */
  EventoModel createEventoModel();

  /**
   * Returns a new object of class '<em>Dia</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Dia</em>'.
   * @generated
   */
  Dia createDia();

  /**
   * Returns a new object of class '<em>Espacio</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Espacio</em>'.
   * @generated
   */
  Espacio createEspacio();

  /**
   * Returns a new object of class '<em>Track</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Track</em>'.
   * @generated
   */
  Track createTrack();

  /**
   * Returns a new object of class '<em>Actividad</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Actividad</em>'.
   * @generated
   */
  Actividad createActividad();

  /**
   * Returns a new object of class '<em>Organizacion</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Organizacion</em>'.
   * @generated
   */
  Organizacion createOrganizacion();

  /**
   * Returns a new object of class '<em>Orador</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Orador</em>'.
   * @generated
   */
  Orador createOrador();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  PlanificacionPackage getPlanificacionPackage();

} //PlanificacionFactory
