/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Actividad</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#getComienzo <em>Comienzo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#getFin <em>Fin</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#isMesa <em>Mesa</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#isCharla <em>Charla</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#isTaller <em>Taller</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#getTitulo <em>Titulo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#getOradores <em>Oradores</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.ActividadImpl#getOyentes <em>Oyentes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActividadImpl extends MinimalEObjectImpl.Container implements Actividad
{
  /**
   * The default value of the '{@link #getComienzo() <em>Comienzo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComienzo()
   * @generated
   * @ordered
   */
  protected static final int COMIENZO_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getComienzo() <em>Comienzo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getComienzo()
   * @generated
   * @ordered
   */
  protected int comienzo = COMIENZO_EDEFAULT;

  /**
   * The default value of the '{@link #getFin() <em>Fin</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFin()
   * @generated
   * @ordered
   */
  protected static final int FIN_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getFin() <em>Fin</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFin()
   * @generated
   * @ordered
   */
  protected int fin = FIN_EDEFAULT;

  /**
   * The default value of the '{@link #isMesa() <em>Mesa</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMesa()
   * @generated
   * @ordered
   */
  protected static final boolean MESA_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isMesa() <em>Mesa</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isMesa()
   * @generated
   * @ordered
   */
  protected boolean mesa = MESA_EDEFAULT;

  /**
   * The default value of the '{@link #isCharla() <em>Charla</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCharla()
   * @generated
   * @ordered
   */
  protected static final boolean CHARLA_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isCharla() <em>Charla</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCharla()
   * @generated
   * @ordered
   */
  protected boolean charla = CHARLA_EDEFAULT;

  /**
   * The default value of the '{@link #isTaller() <em>Taller</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTaller()
   * @generated
   * @ordered
   */
  protected static final boolean TALLER_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isTaller() <em>Taller</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isTaller()
   * @generated
   * @ordered
   */
  protected boolean taller = TALLER_EDEFAULT;

  /**
   * The default value of the '{@link #getTitulo() <em>Titulo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitulo()
   * @generated
   * @ordered
   */
  protected static final String TITULO_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTitulo() <em>Titulo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitulo()
   * @generated
   * @ordered
   */
  protected String titulo = TITULO_EDEFAULT;

  /**
   * The cached value of the '{@link #getOradores() <em>Oradores</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOradores()
   * @generated
   * @ordered
   */
  protected EList<Orador> oradores;

  /**
   * The default value of the '{@link #getOyentes() <em>Oyentes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOyentes()
   * @generated
   * @ordered
   */
  protected static final int OYENTES_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getOyentes() <em>Oyentes</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOyentes()
   * @generated
   * @ordered
   */
  protected int oyentes = OYENTES_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ActividadImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.ACTIVIDAD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getComienzo()
  {
    return comienzo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setComienzo(int newComienzo)
  {
    int oldComienzo = comienzo;
    comienzo = newComienzo;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__COMIENZO, oldComienzo, comienzo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getFin()
  {
    return fin;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFin(int newFin)
  {
    int oldFin = fin;
    fin = newFin;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__FIN, oldFin, fin));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isMesa()
  {
    return mesa;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMesa(boolean newMesa)
  {
    boolean oldMesa = mesa;
    mesa = newMesa;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__MESA, oldMesa, mesa));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isCharla()
  {
    return charla;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCharla(boolean newCharla)
  {
    boolean oldCharla = charla;
    charla = newCharla;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__CHARLA, oldCharla, charla));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isTaller()
  {
    return taller;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTaller(boolean newTaller)
  {
    boolean oldTaller = taller;
    taller = newTaller;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__TALLER, oldTaller, taller));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTitulo()
  {
    return titulo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTitulo(String newTitulo)
  {
    String oldTitulo = titulo;
    titulo = newTitulo;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__TITULO, oldTitulo, titulo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Orador> getOradores()
  {
    if (oradores == null)
    {
      oradores = new EObjectResolvingEList<Orador>(Orador.class, this, PlanificacionPackage.ACTIVIDAD__ORADORES);
    }
    return oradores;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getOyentes()
  {
    return oyentes;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOyentes(int newOyentes)
  {
    int oldOyentes = oyentes;
    oyentes = newOyentes;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.ACTIVIDAD__OYENTES, oldOyentes, oyentes));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ACTIVIDAD__COMIENZO:
        return getComienzo();
      case PlanificacionPackage.ACTIVIDAD__FIN:
        return getFin();
      case PlanificacionPackage.ACTIVIDAD__MESA:
        return isMesa();
      case PlanificacionPackage.ACTIVIDAD__CHARLA:
        return isCharla();
      case PlanificacionPackage.ACTIVIDAD__TALLER:
        return isTaller();
      case PlanificacionPackage.ACTIVIDAD__TITULO:
        return getTitulo();
      case PlanificacionPackage.ACTIVIDAD__ORADORES:
        return getOradores();
      case PlanificacionPackage.ACTIVIDAD__OYENTES:
        return getOyentes();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ACTIVIDAD__COMIENZO:
        setComienzo((Integer)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__FIN:
        setFin((Integer)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__MESA:
        setMesa((Boolean)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__CHARLA:
        setCharla((Boolean)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__TALLER:
        setTaller((Boolean)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__TITULO:
        setTitulo((String)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__ORADORES:
        getOradores().clear();
        getOradores().addAll((Collection<? extends Orador>)newValue);
        return;
      case PlanificacionPackage.ACTIVIDAD__OYENTES:
        setOyentes((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ACTIVIDAD__COMIENZO:
        setComienzo(COMIENZO_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__FIN:
        setFin(FIN_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__MESA:
        setMesa(MESA_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__CHARLA:
        setCharla(CHARLA_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__TALLER:
        setTaller(TALLER_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__TITULO:
        setTitulo(TITULO_EDEFAULT);
        return;
      case PlanificacionPackage.ACTIVIDAD__ORADORES:
        getOradores().clear();
        return;
      case PlanificacionPackage.ACTIVIDAD__OYENTES:
        setOyentes(OYENTES_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.ACTIVIDAD__COMIENZO:
        return comienzo != COMIENZO_EDEFAULT;
      case PlanificacionPackage.ACTIVIDAD__FIN:
        return fin != FIN_EDEFAULT;
      case PlanificacionPackage.ACTIVIDAD__MESA:
        return mesa != MESA_EDEFAULT;
      case PlanificacionPackage.ACTIVIDAD__CHARLA:
        return charla != CHARLA_EDEFAULT;
      case PlanificacionPackage.ACTIVIDAD__TALLER:
        return taller != TALLER_EDEFAULT;
      case PlanificacionPackage.ACTIVIDAD__TITULO:
        return TITULO_EDEFAULT == null ? titulo != null : !TITULO_EDEFAULT.equals(titulo);
      case PlanificacionPackage.ACTIVIDAD__ORADORES:
        return oradores != null && !oradores.isEmpty();
      case PlanificacionPackage.ACTIVIDAD__OYENTES:
        return oyentes != OYENTES_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (comienzo: ");
    result.append(comienzo);
    result.append(", fin: ");
    result.append(fin);
    result.append(", mesa: ");
    result.append(mesa);
    result.append(", charla: ");
    result.append(charla);
    result.append(", taller: ");
    result.append(taller);
    result.append(", titulo: ");
    result.append(titulo);
    result.append(", oyentes: ");
    result.append(oyentes);
    result.append(')');
    return result.toString();
  }

} //ActividadImpl
