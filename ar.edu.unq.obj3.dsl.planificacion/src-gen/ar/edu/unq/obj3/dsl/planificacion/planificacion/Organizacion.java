/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organizacion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getRazonSocial <em>Razon Social</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrganizacion()
 * @model
 * @generated
 */
public interface Organizacion extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrganizacion_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Razon Social</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Razon Social</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Razon Social</em>' attribute.
   * @see #setRazonSocial(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrganizacion_RazonSocial()
   * @model
   * @generated
   */
  String getRazonSocial();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Organizacion#getRazonSocial <em>Razon Social</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Razon Social</em>' attribute.
   * @see #getRazonSocial()
   * @generated
   */
  void setRazonSocial(String value);

} // Organizacion
