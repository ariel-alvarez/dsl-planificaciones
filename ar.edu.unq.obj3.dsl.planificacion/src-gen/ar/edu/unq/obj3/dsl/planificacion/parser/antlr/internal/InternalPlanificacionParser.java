package ar.edu.unq.obj3.dsl.planificacion.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ar.edu.unq.obj3.dsl.planificacion.services.PlanificacionGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPlanificacionParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Evento'", "'{'", "'espacios'", "'organizaciones'", "'oradores'", "'}'", "'Dia'", "'Espacio'", "'capacidad'", "'con computadoras'", "'Bloque'", "'en'", "'['", "'-'", "']'", "'mesa'", "'charla'", "'taller'", "'se esperan'", "'oyentes'", "'Org'", "':'", "'Orador'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPlanificacionParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPlanificacionParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPlanificacionParser.tokenNames; }
    public String getGrammarFileName() { return "../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g"; }



     	private PlanificacionGrammarAccess grammarAccess;
     	
        public InternalPlanificacionParser(TokenStream input, PlanificacionGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "EventoModel";	
       	}
       	
       	@Override
       	protected PlanificacionGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleEventoModel"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:67:1: entryRuleEventoModel returns [EObject current=null] : iv_ruleEventoModel= ruleEventoModel EOF ;
    public final EObject entryRuleEventoModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventoModel = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:68:2: (iv_ruleEventoModel= ruleEventoModel EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:69:2: iv_ruleEventoModel= ruleEventoModel EOF
            {
             newCompositeNode(grammarAccess.getEventoModelRule()); 
            pushFollow(FOLLOW_ruleEventoModel_in_entryRuleEventoModel75);
            iv_ruleEventoModel=ruleEventoModel();

            state._fsp--;

             current =iv_ruleEventoModel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventoModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventoModel"


    // $ANTLR start "ruleEventoModel"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:76:1: ruleEventoModel returns [EObject current=null] : (otherlv_0= 'Evento' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= '{' otherlv_3= 'espacios' ( (lv_espacios_4_0= ruleEspacio ) )+ otherlv_5= 'organizaciones' ( (lv_organizaciones_6_0= ruleOrganizacion ) )+ otherlv_7= 'oradores' ( (lv_oradores_8_0= ruleOrador ) )+ ( (lv_dias_9_0= ruleDia ) )+ otherlv_10= '}' ) ;
    public final EObject ruleEventoModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_title_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_espacios_4_0 = null;

        EObject lv_organizaciones_6_0 = null;

        EObject lv_oradores_8_0 = null;

        EObject lv_dias_9_0 = null;


         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:79:28: ( (otherlv_0= 'Evento' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= '{' otherlv_3= 'espacios' ( (lv_espacios_4_0= ruleEspacio ) )+ otherlv_5= 'organizaciones' ( (lv_organizaciones_6_0= ruleOrganizacion ) )+ otherlv_7= 'oradores' ( (lv_oradores_8_0= ruleOrador ) )+ ( (lv_dias_9_0= ruleDia ) )+ otherlv_10= '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:80:1: (otherlv_0= 'Evento' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= '{' otherlv_3= 'espacios' ( (lv_espacios_4_0= ruleEspacio ) )+ otherlv_5= 'organizaciones' ( (lv_organizaciones_6_0= ruleOrganizacion ) )+ otherlv_7= 'oradores' ( (lv_oradores_8_0= ruleOrador ) )+ ( (lv_dias_9_0= ruleDia ) )+ otherlv_10= '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:80:1: (otherlv_0= 'Evento' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= '{' otherlv_3= 'espacios' ( (lv_espacios_4_0= ruleEspacio ) )+ otherlv_5= 'organizaciones' ( (lv_organizaciones_6_0= ruleOrganizacion ) )+ otherlv_7= 'oradores' ( (lv_oradores_8_0= ruleOrador ) )+ ( (lv_dias_9_0= ruleDia ) )+ otherlv_10= '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:80:3: otherlv_0= 'Evento' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= '{' otherlv_3= 'espacios' ( (lv_espacios_4_0= ruleEspacio ) )+ otherlv_5= 'organizaciones' ( (lv_organizaciones_6_0= ruleOrganizacion ) )+ otherlv_7= 'oradores' ( (lv_oradores_8_0= ruleOrador ) )+ ( (lv_dias_9_0= ruleDia ) )+ otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleEventoModel122); 

                	newLeafNode(otherlv_0, grammarAccess.getEventoModelAccess().getEventoKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:84:1: ( (lv_title_1_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:85:1: (lv_title_1_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:85:1: (lv_title_1_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:86:3: lv_title_1_0= RULE_STRING
            {
            lv_title_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleEventoModel139); 

            			newLeafNode(lv_title_1_0, grammarAccess.getEventoModelAccess().getTitleSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEventoModelRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"title",
                    		lv_title_1_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleEventoModel156); 

                	newLeafNode(otherlv_2, grammarAccess.getEventoModelAccess().getLeftCurlyBracketKeyword_2());
                
            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleEventoModel168); 

                	newLeafNode(otherlv_3, grammarAccess.getEventoModelAccess().getEspaciosKeyword_3());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:110:1: ( (lv_espacios_4_0= ruleEspacio ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==18) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:111:1: (lv_espacios_4_0= ruleEspacio )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:111:1: (lv_espacios_4_0= ruleEspacio )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:112:3: lv_espacios_4_0= ruleEspacio
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEventoModelAccess().getEspaciosEspacioParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleEspacio_in_ruleEventoModel189);
            	    lv_espacios_4_0=ruleEspacio();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEventoModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"espacios",
            	            		lv_espacios_4_0, 
            	            		"Espacio");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleEventoModel202); 

                	newLeafNode(otherlv_5, grammarAccess.getEventoModelAccess().getOrganizacionesKeyword_5());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:132:1: ( (lv_organizaciones_6_0= ruleOrganizacion ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==31) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:133:1: (lv_organizaciones_6_0= ruleOrganizacion )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:133:1: (lv_organizaciones_6_0= ruleOrganizacion )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:134:3: lv_organizaciones_6_0= ruleOrganizacion
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEventoModelAccess().getOrganizacionesOrganizacionParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOrganizacion_in_ruleEventoModel223);
            	    lv_organizaciones_6_0=ruleOrganizacion();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEventoModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"organizaciones",
            	            		lv_organizaciones_6_0, 
            	            		"Organizacion");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            otherlv_7=(Token)match(input,15,FOLLOW_15_in_ruleEventoModel236); 

                	newLeafNode(otherlv_7, grammarAccess.getEventoModelAccess().getOradoresKeyword_7());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:154:1: ( (lv_oradores_8_0= ruleOrador ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==33) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:155:1: (lv_oradores_8_0= ruleOrador )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:155:1: (lv_oradores_8_0= ruleOrador )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:156:3: lv_oradores_8_0= ruleOrador
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEventoModelAccess().getOradoresOradorParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOrador_in_ruleEventoModel257);
            	    lv_oradores_8_0=ruleOrador();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEventoModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"oradores",
            	            		lv_oradores_8_0, 
            	            		"Orador");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:172:3: ( (lv_dias_9_0= ruleDia ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:173:1: (lv_dias_9_0= ruleDia )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:173:1: (lv_dias_9_0= ruleDia )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:174:3: lv_dias_9_0= ruleDia
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getEventoModelAccess().getDiasDiaParserRuleCall_9_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleDia_in_ruleEventoModel279);
            	    lv_dias_9_0=ruleDia();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getEventoModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"dias",
            	            		lv_dias_9_0, 
            	            		"Dia");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            otherlv_10=(Token)match(input,16,FOLLOW_16_in_ruleEventoModel292); 

                	newLeafNode(otherlv_10, grammarAccess.getEventoModelAccess().getRightCurlyBracketKeyword_10());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventoModel"


    // $ANTLR start "entryRuleDia"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:202:1: entryRuleDia returns [EObject current=null] : iv_ruleDia= ruleDia EOF ;
    public final EObject entryRuleDia() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDia = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:203:2: (iv_ruleDia= ruleDia EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:204:2: iv_ruleDia= ruleDia EOF
            {
             newCompositeNode(grammarAccess.getDiaRule()); 
            pushFollow(FOLLOW_ruleDia_in_entryRuleDia328);
            iv_ruleDia=ruleDia();

            state._fsp--;

             current =iv_ruleDia; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDia338); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDia"


    // $ANTLR start "ruleDia"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:211:1: ruleDia returns [EObject current=null] : (otherlv_0= 'Dia' ( (lv_dia_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_bloques_3_0= ruleTrack ) )+ otherlv_4= '}' ) ;
    public final EObject ruleDia() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_dia_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_bloques_3_0 = null;


         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:214:28: ( (otherlv_0= 'Dia' ( (lv_dia_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_bloques_3_0= ruleTrack ) )+ otherlv_4= '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:215:1: (otherlv_0= 'Dia' ( (lv_dia_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_bloques_3_0= ruleTrack ) )+ otherlv_4= '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:215:1: (otherlv_0= 'Dia' ( (lv_dia_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_bloques_3_0= ruleTrack ) )+ otherlv_4= '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:215:3: otherlv_0= 'Dia' ( (lv_dia_1_0= RULE_STRING ) ) otherlv_2= '{' ( (lv_bloques_3_0= ruleTrack ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleDia375); 

                	newLeafNode(otherlv_0, grammarAccess.getDiaAccess().getDiaKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:219:1: ( (lv_dia_1_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:220:1: (lv_dia_1_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:220:1: (lv_dia_1_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:221:3: lv_dia_1_0= RULE_STRING
            {
            lv_dia_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleDia392); 

            			newLeafNode(lv_dia_1_0, grammarAccess.getDiaAccess().getDiaSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDiaRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"dia",
                    		lv_dia_1_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleDia409); 

                	newLeafNode(otherlv_2, grammarAccess.getDiaAccess().getLeftCurlyBracketKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:241:1: ( (lv_bloques_3_0= ruleTrack ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==21) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:242:1: (lv_bloques_3_0= ruleTrack )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:242:1: (lv_bloques_3_0= ruleTrack )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:243:3: lv_bloques_3_0= ruleTrack
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDiaAccess().getBloquesTrackParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleTrack_in_ruleDia430);
            	    lv_bloques_3_0=ruleTrack();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDiaRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"bloques",
            	            		lv_bloques_3_0, 
            	            		"Track");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleDia443); 

                	newLeafNode(otherlv_4, grammarAccess.getDiaAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDia"


    // $ANTLR start "entryRuleEspacio"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:271:1: entryRuleEspacio returns [EObject current=null] : iv_ruleEspacio= ruleEspacio EOF ;
    public final EObject entryRuleEspacio() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEspacio = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:272:2: (iv_ruleEspacio= ruleEspacio EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:273:2: iv_ruleEspacio= ruleEspacio EOF
            {
             newCompositeNode(grammarAccess.getEspacioRule()); 
            pushFollow(FOLLOW_ruleEspacio_in_entryRuleEspacio479);
            iv_ruleEspacio=ruleEspacio();

            state._fsp--;

             current =iv_ruleEspacio; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEspacio489); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEspacio"


    // $ANTLR start "ruleEspacio"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:280:1: ruleEspacio returns [EObject current=null] : (otherlv_0= 'Espacio' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'capacidad' ( (lv_capacidad_3_0= RULE_INT ) ) ( (lv_laboratorio_4_0= 'con computadoras' ) )? ) ;
    public final EObject ruleEspacio() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_capacidad_3_0=null;
        Token lv_laboratorio_4_0=null;

         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:283:28: ( (otherlv_0= 'Espacio' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'capacidad' ( (lv_capacidad_3_0= RULE_INT ) ) ( (lv_laboratorio_4_0= 'con computadoras' ) )? ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:284:1: (otherlv_0= 'Espacio' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'capacidad' ( (lv_capacidad_3_0= RULE_INT ) ) ( (lv_laboratorio_4_0= 'con computadoras' ) )? )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:284:1: (otherlv_0= 'Espacio' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'capacidad' ( (lv_capacidad_3_0= RULE_INT ) ) ( (lv_laboratorio_4_0= 'con computadoras' ) )? )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:284:3: otherlv_0= 'Espacio' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'capacidad' ( (lv_capacidad_3_0= RULE_INT ) ) ( (lv_laboratorio_4_0= 'con computadoras' ) )?
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleEspacio526); 

                	newLeafNode(otherlv_0, grammarAccess.getEspacioAccess().getEspacioKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:288:1: ( (lv_name_1_0= RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:289:1: (lv_name_1_0= RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:289:1: (lv_name_1_0= RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:290:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEspacio543); 

            			newLeafNode(lv_name_1_0, grammarAccess.getEspacioAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEspacioRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleEspacio560); 

                	newLeafNode(otherlv_2, grammarAccess.getEspacioAccess().getCapacidadKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:310:1: ( (lv_capacidad_3_0= RULE_INT ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:311:1: (lv_capacidad_3_0= RULE_INT )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:311:1: (lv_capacidad_3_0= RULE_INT )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:312:3: lv_capacidad_3_0= RULE_INT
            {
            lv_capacidad_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleEspacio577); 

            			newLeafNode(lv_capacidad_3_0, grammarAccess.getEspacioAccess().getCapacidadINTTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEspacioRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"capacidad",
                    		lv_capacidad_3_0, 
                    		"INT");
            	    

            }


            }

            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:328:2: ( (lv_laboratorio_4_0= 'con computadoras' ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==20) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:329:1: (lv_laboratorio_4_0= 'con computadoras' )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:329:1: (lv_laboratorio_4_0= 'con computadoras' )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:330:3: lv_laboratorio_4_0= 'con computadoras'
                    {
                    lv_laboratorio_4_0=(Token)match(input,20,FOLLOW_20_in_ruleEspacio600); 

                            newLeafNode(lv_laboratorio_4_0, grammarAccess.getEspacioAccess().getLaboratorioConComputadorasKeyword_4_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getEspacioRule());
                    	        }
                           		setWithLastConsumed(current, "laboratorio", true, "con computadoras");
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEspacio"


    // $ANTLR start "entryRuleTrack"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:351:1: entryRuleTrack returns [EObject current=null] : iv_ruleTrack= ruleTrack EOF ;
    public final EObject entryRuleTrack() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTrack = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:352:2: (iv_ruleTrack= ruleTrack EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:353:2: iv_ruleTrack= ruleTrack EOF
            {
             newCompositeNode(grammarAccess.getTrackRule()); 
            pushFollow(FOLLOW_ruleTrack_in_entryRuleTrack650);
            iv_ruleTrack=ruleTrack();

            state._fsp--;

             current =iv_ruleTrack; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTrack660); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTrack"


    // $ANTLR start "ruleTrack"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:360:1: ruleTrack returns [EObject current=null] : (otherlv_0= 'Bloque' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= 'en' ( (otherlv_3= RULE_ID ) ) otherlv_4= '{' ( (lv_actividades_5_0= ruleActividad ) )+ otherlv_6= '}' ) ;
    public final EObject ruleTrack() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_title_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_actividades_5_0 = null;


         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:363:28: ( (otherlv_0= 'Bloque' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= 'en' ( (otherlv_3= RULE_ID ) ) otherlv_4= '{' ( (lv_actividades_5_0= ruleActividad ) )+ otherlv_6= '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:364:1: (otherlv_0= 'Bloque' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= 'en' ( (otherlv_3= RULE_ID ) ) otherlv_4= '{' ( (lv_actividades_5_0= ruleActividad ) )+ otherlv_6= '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:364:1: (otherlv_0= 'Bloque' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= 'en' ( (otherlv_3= RULE_ID ) ) otherlv_4= '{' ( (lv_actividades_5_0= ruleActividad ) )+ otherlv_6= '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:364:3: otherlv_0= 'Bloque' ( (lv_title_1_0= RULE_STRING ) ) otherlv_2= 'en' ( (otherlv_3= RULE_ID ) ) otherlv_4= '{' ( (lv_actividades_5_0= ruleActividad ) )+ otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleTrack697); 

                	newLeafNode(otherlv_0, grammarAccess.getTrackAccess().getBloqueKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:368:1: ( (lv_title_1_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:369:1: (lv_title_1_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:369:1: (lv_title_1_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:370:3: lv_title_1_0= RULE_STRING
            {
            lv_title_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleTrack714); 

            			newLeafNode(lv_title_1_0, grammarAccess.getTrackAccess().getTitleSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTrackRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"title",
                    		lv_title_1_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_22_in_ruleTrack731); 

                	newLeafNode(otherlv_2, grammarAccess.getTrackAccess().getEnKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:390:1: ( (otherlv_3= RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:391:1: (otherlv_3= RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:391:1: (otherlv_3= RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:392:3: otherlv_3= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getTrackRule());
            	        }
                    
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTrack751); 

            		newLeafNode(otherlv_3, grammarAccess.getTrackAccess().getEspacioEspacioCrossReference_3_0()); 
            	

            }


            }

            otherlv_4=(Token)match(input,12,FOLLOW_12_in_ruleTrack763); 

                	newLeafNode(otherlv_4, grammarAccess.getTrackAccess().getLeftCurlyBracketKeyword_4());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:407:1: ( (lv_actividades_5_0= ruleActividad ) )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:408:1: (lv_actividades_5_0= ruleActividad )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:408:1: (lv_actividades_5_0= ruleActividad )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:409:3: lv_actividades_5_0= ruleActividad
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTrackAccess().getActividadesActividadParserRuleCall_5_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleActividad_in_ruleTrack784);
            	    lv_actividades_5_0=ruleActividad();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTrackRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"actividades",
            	            		lv_actividades_5_0, 
            	            		"Actividad");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            otherlv_6=(Token)match(input,16,FOLLOW_16_in_ruleTrack797); 

                	newLeafNode(otherlv_6, grammarAccess.getTrackAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTrack"


    // $ANTLR start "entryRuleActividad"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:437:1: entryRuleActividad returns [EObject current=null] : iv_ruleActividad= ruleActividad EOF ;
    public final EObject entryRuleActividad() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleActividad = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:438:2: (iv_ruleActividad= ruleActividad EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:439:2: iv_ruleActividad= ruleActividad EOF
            {
             newCompositeNode(grammarAccess.getActividadRule()); 
            pushFollow(FOLLOW_ruleActividad_in_entryRuleActividad833);
            iv_ruleActividad=ruleActividad();

            state._fsp--;

             current =iv_ruleActividad; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleActividad843); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActividad"


    // $ANTLR start "ruleActividad"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:446:1: ruleActividad returns [EObject current=null] : (otherlv_0= '[' ( (lv_comienzo_1_0= ruleHOUR ) ) otherlv_2= '-' ( (lv_fin_3_0= ruleHOUR ) ) otherlv_4= ']' ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) ) ( (lv_titulo_8_0= RULE_STRING ) ) otherlv_9= 'oradores' ( (otherlv_10= RULE_ID ) )+ otherlv_11= 'se esperan' ( (lv_oyentes_12_0= RULE_INT ) ) otherlv_13= 'oyentes' ) ;
    public final EObject ruleActividad() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_mesa_5_0=null;
        Token lv_charla_6_0=null;
        Token lv_taller_7_0=null;
        Token lv_titulo_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token lv_oyentes_12_0=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_comienzo_1_0 = null;

        AntlrDatatypeRuleToken lv_fin_3_0 = null;


         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:449:28: ( (otherlv_0= '[' ( (lv_comienzo_1_0= ruleHOUR ) ) otherlv_2= '-' ( (lv_fin_3_0= ruleHOUR ) ) otherlv_4= ']' ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) ) ( (lv_titulo_8_0= RULE_STRING ) ) otherlv_9= 'oradores' ( (otherlv_10= RULE_ID ) )+ otherlv_11= 'se esperan' ( (lv_oyentes_12_0= RULE_INT ) ) otherlv_13= 'oyentes' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:450:1: (otherlv_0= '[' ( (lv_comienzo_1_0= ruleHOUR ) ) otherlv_2= '-' ( (lv_fin_3_0= ruleHOUR ) ) otherlv_4= ']' ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) ) ( (lv_titulo_8_0= RULE_STRING ) ) otherlv_9= 'oradores' ( (otherlv_10= RULE_ID ) )+ otherlv_11= 'se esperan' ( (lv_oyentes_12_0= RULE_INT ) ) otherlv_13= 'oyentes' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:450:1: (otherlv_0= '[' ( (lv_comienzo_1_0= ruleHOUR ) ) otherlv_2= '-' ( (lv_fin_3_0= ruleHOUR ) ) otherlv_4= ']' ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) ) ( (lv_titulo_8_0= RULE_STRING ) ) otherlv_9= 'oradores' ( (otherlv_10= RULE_ID ) )+ otherlv_11= 'se esperan' ( (lv_oyentes_12_0= RULE_INT ) ) otherlv_13= 'oyentes' )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:450:3: otherlv_0= '[' ( (lv_comienzo_1_0= ruleHOUR ) ) otherlv_2= '-' ( (lv_fin_3_0= ruleHOUR ) ) otherlv_4= ']' ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) ) ( (lv_titulo_8_0= RULE_STRING ) ) otherlv_9= 'oradores' ( (otherlv_10= RULE_ID ) )+ otherlv_11= 'se esperan' ( (lv_oyentes_12_0= RULE_INT ) ) otherlv_13= 'oyentes'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleActividad880); 

                	newLeafNode(otherlv_0, grammarAccess.getActividadAccess().getLeftSquareBracketKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:454:1: ( (lv_comienzo_1_0= ruleHOUR ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:455:1: (lv_comienzo_1_0= ruleHOUR )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:455:1: (lv_comienzo_1_0= ruleHOUR )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:456:3: lv_comienzo_1_0= ruleHOUR
            {
             
            	        newCompositeNode(grammarAccess.getActividadAccess().getComienzoHOURParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleHOUR_in_ruleActividad901);
            lv_comienzo_1_0=ruleHOUR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getActividadRule());
            	        }
                   		set(
                   			current, 
                   			"comienzo",
                    		lv_comienzo_1_0, 
                    		"HOUR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,24,FOLLOW_24_in_ruleActividad913); 

                	newLeafNode(otherlv_2, grammarAccess.getActividadAccess().getHyphenMinusKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:476:1: ( (lv_fin_3_0= ruleHOUR ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:477:1: (lv_fin_3_0= ruleHOUR )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:477:1: (lv_fin_3_0= ruleHOUR )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:478:3: lv_fin_3_0= ruleHOUR
            {
             
            	        newCompositeNode(grammarAccess.getActividadAccess().getFinHOURParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleHOUR_in_ruleActividad934);
            lv_fin_3_0=ruleHOUR();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getActividadRule());
            	        }
                   		set(
                   			current, 
                   			"fin",
                    		lv_fin_3_0, 
                    		"HOUR");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleActividad946); 

                	newLeafNode(otherlv_4, grammarAccess.getActividadAccess().getRightSquareBracketKeyword_4());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:498:1: ( ( (lv_mesa_5_0= 'mesa' ) ) | ( (lv_charla_6_0= 'charla' ) ) | ( (lv_taller_7_0= 'taller' ) ) )
            int alt8=3;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt8=1;
                }
                break;
            case 27:
                {
                alt8=2;
                }
                break;
            case 28:
                {
                alt8=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:498:2: ( (lv_mesa_5_0= 'mesa' ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:498:2: ( (lv_mesa_5_0= 'mesa' ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:499:1: (lv_mesa_5_0= 'mesa' )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:499:1: (lv_mesa_5_0= 'mesa' )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:500:3: lv_mesa_5_0= 'mesa'
                    {
                    lv_mesa_5_0=(Token)match(input,26,FOLLOW_26_in_ruleActividad965); 

                            newLeafNode(lv_mesa_5_0, grammarAccess.getActividadAccess().getMesaMesaKeyword_5_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getActividadRule());
                    	        }
                           		setWithLastConsumed(current, "mesa", true, "mesa");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:514:6: ( (lv_charla_6_0= 'charla' ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:514:6: ( (lv_charla_6_0= 'charla' ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:515:1: (lv_charla_6_0= 'charla' )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:515:1: (lv_charla_6_0= 'charla' )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:516:3: lv_charla_6_0= 'charla'
                    {
                    lv_charla_6_0=(Token)match(input,27,FOLLOW_27_in_ruleActividad1002); 

                            newLeafNode(lv_charla_6_0, grammarAccess.getActividadAccess().getCharlaCharlaKeyword_5_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getActividadRule());
                    	        }
                           		setWithLastConsumed(current, "charla", true, "charla");
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:530:6: ( (lv_taller_7_0= 'taller' ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:530:6: ( (lv_taller_7_0= 'taller' ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:531:1: (lv_taller_7_0= 'taller' )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:531:1: (lv_taller_7_0= 'taller' )
                    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:532:3: lv_taller_7_0= 'taller'
                    {
                    lv_taller_7_0=(Token)match(input,28,FOLLOW_28_in_ruleActividad1039); 

                            newLeafNode(lv_taller_7_0, grammarAccess.getActividadAccess().getTallerTallerKeyword_5_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getActividadRule());
                    	        }
                           		setWithLastConsumed(current, "taller", true, "taller");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:545:3: ( (lv_titulo_8_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:546:1: (lv_titulo_8_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:546:1: (lv_titulo_8_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:547:3: lv_titulo_8_0= RULE_STRING
            {
            lv_titulo_8_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleActividad1070); 

            			newLeafNode(lv_titulo_8_0, grammarAccess.getActividadAccess().getTituloSTRINGTerminalRuleCall_6_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActividadRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"titulo",
                    		lv_titulo_8_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_9=(Token)match(input,15,FOLLOW_15_in_ruleActividad1087); 

                	newLeafNode(otherlv_9, grammarAccess.getActividadAccess().getOradoresKeyword_7());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:567:1: ( (otherlv_10= RULE_ID ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:568:1: (otherlv_10= RULE_ID )
            	    {
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:568:1: (otherlv_10= RULE_ID )
            	    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:569:3: otherlv_10= RULE_ID
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getActividadRule());
            	    	        }
            	            
            	    otherlv_10=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleActividad1107); 

            	    		newLeafNode(otherlv_10, grammarAccess.getActividadAccess().getOradoresOradorCrossReference_8_0()); 
            	    	

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);

            otherlv_11=(Token)match(input,29,FOLLOW_29_in_ruleActividad1120); 

                	newLeafNode(otherlv_11, grammarAccess.getActividadAccess().getSeEsperanKeyword_9());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:584:1: ( (lv_oyentes_12_0= RULE_INT ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:585:1: (lv_oyentes_12_0= RULE_INT )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:585:1: (lv_oyentes_12_0= RULE_INT )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:586:3: lv_oyentes_12_0= RULE_INT
            {
            lv_oyentes_12_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleActividad1137); 

            			newLeafNode(lv_oyentes_12_0, grammarAccess.getActividadAccess().getOyentesINTTerminalRuleCall_10_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getActividadRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"oyentes",
                    		lv_oyentes_12_0, 
                    		"INT");
            	    

            }


            }

            otherlv_13=(Token)match(input,30,FOLLOW_30_in_ruleActividad1154); 

                	newLeafNode(otherlv_13, grammarAccess.getActividadAccess().getOyentesKeyword_11());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActividad"


    // $ANTLR start "entryRuleOrganizacion"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:614:1: entryRuleOrganizacion returns [EObject current=null] : iv_ruleOrganizacion= ruleOrganizacion EOF ;
    public final EObject entryRuleOrganizacion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrganizacion = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:615:2: (iv_ruleOrganizacion= ruleOrganizacion EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:616:2: iv_ruleOrganizacion= ruleOrganizacion EOF
            {
             newCompositeNode(grammarAccess.getOrganizacionRule()); 
            pushFollow(FOLLOW_ruleOrganizacion_in_entryRuleOrganizacion1190);
            iv_ruleOrganizacion=ruleOrganizacion();

            state._fsp--;

             current =iv_ruleOrganizacion; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrganizacion1200); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrganizacion"


    // $ANTLR start "ruleOrganizacion"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:623:1: ruleOrganizacion returns [EObject current=null] : (otherlv_0= 'Org' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_razonSocial_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleOrganizacion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_razonSocial_3_0=null;

         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:626:28: ( (otherlv_0= 'Org' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_razonSocial_3_0= RULE_STRING ) ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:627:1: (otherlv_0= 'Org' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_razonSocial_3_0= RULE_STRING ) ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:627:1: (otherlv_0= 'Org' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_razonSocial_3_0= RULE_STRING ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:627:3: otherlv_0= 'Org' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_razonSocial_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,31,FOLLOW_31_in_ruleOrganizacion1237); 

                	newLeafNode(otherlv_0, grammarAccess.getOrganizacionAccess().getOrgKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:631:1: ( (lv_name_1_0= RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:632:1: (lv_name_1_0= RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:632:1: (lv_name_1_0= RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:633:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOrganizacion1254); 

            			newLeafNode(lv_name_1_0, grammarAccess.getOrganizacionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOrganizacionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32_in_ruleOrganizacion1271); 

                	newLeafNode(otherlv_2, grammarAccess.getOrganizacionAccess().getColonKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:653:1: ( (lv_razonSocial_3_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:654:1: (lv_razonSocial_3_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:654:1: (lv_razonSocial_3_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:655:3: lv_razonSocial_3_0= RULE_STRING
            {
            lv_razonSocial_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleOrganizacion1288); 

            			newLeafNode(lv_razonSocial_3_0, grammarAccess.getOrganizacionAccess().getRazonSocialSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOrganizacionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"razonSocial",
                    		lv_razonSocial_3_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrganizacion"


    // $ANTLR start "entryRuleOrador"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:679:1: entryRuleOrador returns [EObject current=null] : iv_ruleOrador= ruleOrador EOF ;
    public final EObject entryRuleOrador() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrador = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:680:2: (iv_ruleOrador= ruleOrador EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:681:2: iv_ruleOrador= ruleOrador EOF
            {
             newCompositeNode(grammarAccess.getOradorRule()); 
            pushFollow(FOLLOW_ruleOrador_in_entryRuleOrador1329);
            iv_ruleOrador=ruleOrador();

            state._fsp--;

             current =iv_ruleOrador; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrador1339); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrador"


    // $ANTLR start "ruleOrador"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:688:1: ruleOrador returns [EObject current=null] : (otherlv_0= 'Orador' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_nombreCompleto_3_0= RULE_STRING ) ) ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleOrador() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_nombreCompleto_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:691:28: ( (otherlv_0= 'Orador' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_nombreCompleto_3_0= RULE_STRING ) ) ( (otherlv_4= RULE_ID ) ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:692:1: (otherlv_0= 'Orador' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_nombreCompleto_3_0= RULE_STRING ) ) ( (otherlv_4= RULE_ID ) ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:692:1: (otherlv_0= 'Orador' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_nombreCompleto_3_0= RULE_STRING ) ) ( (otherlv_4= RULE_ID ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:692:3: otherlv_0= 'Orador' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' ( (lv_nombreCompleto_3_0= RULE_STRING ) ) ( (otherlv_4= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,33,FOLLOW_33_in_ruleOrador1376); 

                	newLeafNode(otherlv_0, grammarAccess.getOradorAccess().getOradorKeyword_0());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:696:1: ( (lv_name_1_0= RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:697:1: (lv_name_1_0= RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:697:1: (lv_name_1_0= RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:698:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOrador1393); 

            			newLeafNode(lv_name_1_0, grammarAccess.getOradorAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOradorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32_in_ruleOrador1410); 

                	newLeafNode(otherlv_2, grammarAccess.getOradorAccess().getColonKeyword_2());
                
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:718:1: ( (lv_nombreCompleto_3_0= RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:719:1: (lv_nombreCompleto_3_0= RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:719:1: (lv_nombreCompleto_3_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:720:3: lv_nombreCompleto_3_0= RULE_STRING
            {
            lv_nombreCompleto_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleOrador1427); 

            			newLeafNode(lv_nombreCompleto_3_0, grammarAccess.getOradorAccess().getNombreCompletoSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOradorRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"nombreCompleto",
                    		lv_nombreCompleto_3_0, 
                    		"STRING");
            	    

            }


            }

            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:736:2: ( (otherlv_4= RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:737:1: (otherlv_4= RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:737:1: (otherlv_4= RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:738:3: otherlv_4= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getOradorRule());
            	        }
                    
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOrador1452); 

            		newLeafNode(otherlv_4, grammarAccess.getOradorAccess().getOrganizacionOrganizacionCrossReference_4_0()); 
            	

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrador"


    // $ANTLR start "entryRuleHOUR"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:757:1: entryRuleHOUR returns [String current=null] : iv_ruleHOUR= ruleHOUR EOF ;
    public final String entryRuleHOUR() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleHOUR = null;


        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:758:2: (iv_ruleHOUR= ruleHOUR EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:759:2: iv_ruleHOUR= ruleHOUR EOF
            {
             newCompositeNode(grammarAccess.getHOURRule()); 
            pushFollow(FOLLOW_ruleHOUR_in_entryRuleHOUR1489);
            iv_ruleHOUR=ruleHOUR();

            state._fsp--;

             current =iv_ruleHOUR.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleHOUR1500); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHOUR"


    // $ANTLR start "ruleHOUR"
    // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:766:1: ruleHOUR returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleHOUR() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;

         enterRule(); 
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:769:28: (this_STRING_0= RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion/src-gen/ar/edu/unq/obj3/dsl/planificacion/parser/antlr/internal/InternalPlanificacion.g:770:5: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleHOUR1539); 

            		current.merge(this_STRING_0);
                
             
                newLeafNode(this_STRING_0, grammarAccess.getHOURAccess().getSTRINGTerminalRuleCall()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHOUR"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleEventoModel_in_entryRuleEventoModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventoModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleEventoModel122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleEventoModel139 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleEventoModel156 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleEventoModel168 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ruleEspacio_in_ruleEventoModel189 = new BitSet(new long[]{0x0000000000044000L});
    public static final BitSet FOLLOW_14_in_ruleEventoModel202 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_ruleOrganizacion_in_ruleEventoModel223 = new BitSet(new long[]{0x0000000080008000L});
    public static final BitSet FOLLOW_15_in_ruleEventoModel236 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_ruleOrador_in_ruleEventoModel257 = new BitSet(new long[]{0x0000000200020000L});
    public static final BitSet FOLLOW_ruleDia_in_ruleEventoModel279 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_16_in_ruleEventoModel292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDia_in_entryRuleDia328 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDia338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleDia375 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleDia392 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleDia409 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_ruleTrack_in_ruleDia430 = new BitSet(new long[]{0x0000000000210000L});
    public static final BitSet FOLLOW_16_in_ruleDia443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEspacio_in_entryRuleEspacio479 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEspacio489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleEspacio526 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEspacio543 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleEspacio560 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleEspacio577 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_20_in_ruleEspacio600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTrack_in_entryRuleTrack650 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTrack660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleTrack697 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleTrack714 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleTrack731 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTrack751 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleTrack763 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_ruleActividad_in_ruleTrack784 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_16_in_ruleTrack797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActividad_in_entryRuleActividad833 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleActividad843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleActividad880 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleHOUR_in_ruleActividad901 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_ruleActividad913 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleHOUR_in_ruleActividad934 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleActividad946 = new BitSet(new long[]{0x000000001C000000L});
    public static final BitSet FOLLOW_26_in_ruleActividad965 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_27_in_ruleActividad1002 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_28_in_ruleActividad1039 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleActividad1070 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleActividad1087 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleActividad1107 = new BitSet(new long[]{0x0000000020000020L});
    public static final BitSet FOLLOW_29_in_ruleActividad1120 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleActividad1137 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleActividad1154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrganizacion_in_entryRuleOrganizacion1190 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrganizacion1200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleOrganizacion1237 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOrganizacion1254 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleOrganizacion1271 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleOrganizacion1288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrador_in_entryRuleOrador1329 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrador1339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleOrador1376 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOrador1393 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleOrador1410 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleOrador1427 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOrador1452 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHOUR_in_entryRuleHOUR1489 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleHOUR1500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleHOUR1539 = new BitSet(new long[]{0x0000000000000002L});

}