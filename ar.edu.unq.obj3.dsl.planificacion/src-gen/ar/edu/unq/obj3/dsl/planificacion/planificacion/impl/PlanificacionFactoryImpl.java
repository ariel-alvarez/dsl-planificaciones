/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlanificacionFactoryImpl extends EFactoryImpl implements PlanificacionFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static PlanificacionFactory init()
  {
    try
    {
      PlanificacionFactory thePlanificacionFactory = (PlanificacionFactory)EPackage.Registry.INSTANCE.getEFactory(PlanificacionPackage.eNS_URI);
      if (thePlanificacionFactory != null)
      {
        return thePlanificacionFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new PlanificacionFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlanificacionFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case PlanificacionPackage.EVENTO_MODEL: return createEventoModel();
      case PlanificacionPackage.DIA: return createDia();
      case PlanificacionPackage.ESPACIO: return createEspacio();
      case PlanificacionPackage.TRACK: return createTrack();
      case PlanificacionPackage.ACTIVIDAD: return createActividad();
      case PlanificacionPackage.ORGANIZACION: return createOrganizacion();
      case PlanificacionPackage.ORADOR: return createOrador();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EventoModel createEventoModel()
  {
    EventoModelImpl eventoModel = new EventoModelImpl();
    return eventoModel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Dia createDia()
  {
    DiaImpl dia = new DiaImpl();
    return dia;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Espacio createEspacio()
  {
    EspacioImpl espacio = new EspacioImpl();
    return espacio;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Track createTrack()
  {
    TrackImpl track = new TrackImpl();
    return track;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Actividad createActividad()
  {
    ActividadImpl actividad = new ActividadImpl();
    return actividad;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Organizacion createOrganizacion()
  {
    OrganizacionImpl organizacion = new OrganizacionImpl();
    return organizacion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Orador createOrador()
  {
    OradorImpl orador = new OradorImpl();
    return orador;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public PlanificacionPackage getPlanificacionPackage()
  {
    return (PlanificacionPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static PlanificacionPackage getPackage()
  {
    return PlanificacionPackage.eINSTANCE;
  }

} //PlanificacionFactoryImpl
