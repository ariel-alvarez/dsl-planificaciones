/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion.impl;

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Espacio;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage;
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Track;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Track</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl#getEspacio <em>Espacio</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.impl.TrackImpl#getActividades <em>Actividades</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TrackImpl extends MinimalEObjectImpl.Container implements Track
{
  /**
   * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected static final String TITLE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected String title = TITLE_EDEFAULT;

  /**
   * The cached value of the '{@link #getEspacio() <em>Espacio</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEspacio()
   * @generated
   * @ordered
   */
  protected Espacio espacio;

  /**
   * The cached value of the '{@link #getActividades() <em>Actividades</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getActividades()
   * @generated
   * @ordered
   */
  protected EList<Actividad> actividades;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TrackImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PlanificacionPackage.Literals.TRACK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getTitle()
  {
    return title;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTitle(String newTitle)
  {
    String oldTitle = title;
    title = newTitle;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.TRACK__TITLE, oldTitle, title));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Espacio getEspacio()
  {
    if (espacio != null && espacio.eIsProxy())
    {
      InternalEObject oldEspacio = (InternalEObject)espacio;
      espacio = (Espacio)eResolveProxy(oldEspacio);
      if (espacio != oldEspacio)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, PlanificacionPackage.TRACK__ESPACIO, oldEspacio, espacio));
      }
    }
    return espacio;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Espacio basicGetEspacio()
  {
    return espacio;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEspacio(Espacio newEspacio)
  {
    Espacio oldEspacio = espacio;
    espacio = newEspacio;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.TRACK__ESPACIO, oldEspacio, espacio));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Actividad> getActividades()
  {
    if (actividades == null)
    {
      actividades = new EObjectContainmentEList<Actividad>(Actividad.class, this, PlanificacionPackage.TRACK__ACTIVIDADES);
    }
    return actividades;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PlanificacionPackage.TRACK__ACTIVIDADES:
        return ((InternalEList<?>)getActividades()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PlanificacionPackage.TRACK__TITLE:
        return getTitle();
      case PlanificacionPackage.TRACK__ESPACIO:
        if (resolve) return getEspacio();
        return basicGetEspacio();
      case PlanificacionPackage.TRACK__ACTIVIDADES:
        return getActividades();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PlanificacionPackage.TRACK__TITLE:
        setTitle((String)newValue);
        return;
      case PlanificacionPackage.TRACK__ESPACIO:
        setEspacio((Espacio)newValue);
        return;
      case PlanificacionPackage.TRACK__ACTIVIDADES:
        getActividades().clear();
        getActividades().addAll((Collection<? extends Actividad>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.TRACK__TITLE:
        setTitle(TITLE_EDEFAULT);
        return;
      case PlanificacionPackage.TRACK__ESPACIO:
        setEspacio((Espacio)null);
        return;
      case PlanificacionPackage.TRACK__ACTIVIDADES:
        getActividades().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PlanificacionPackage.TRACK__TITLE:
        return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
      case PlanificacionPackage.TRACK__ESPACIO:
        return espacio != null;
      case PlanificacionPackage.TRACK__ACTIVIDADES:
        return actividades != null && !actividades.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (title: ");
    result.append(title);
    result.append(')');
    return result.toString();
  }

} //TrackImpl
