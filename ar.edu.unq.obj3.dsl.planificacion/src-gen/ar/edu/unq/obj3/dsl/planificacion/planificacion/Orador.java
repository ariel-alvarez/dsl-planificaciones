/**
 */
package ar.edu.unq.obj3.dsl.planificacion.planificacion;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Orador</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getName <em>Name</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getNombreCompleto <em>Nombre Completo</em>}</li>
 *   <li>{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getOrganizacion <em>Organizacion</em>}</li>
 * </ul>
 * </p>
 *
 * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrador()
 * @model
 * @generated
 */
public interface Orador extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrador_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Nombre Completo</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nombre Completo</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nombre Completo</em>' attribute.
   * @see #setNombreCompleto(String)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrador_NombreCompleto()
   * @model
   * @generated
   */
  String getNombreCompleto();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getNombreCompleto <em>Nombre Completo</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nombre Completo</em>' attribute.
   * @see #getNombreCompleto()
   * @generated
   */
  void setNombreCompleto(String value);

  /**
   * Returns the value of the '<em><b>Organizacion</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Organizacion</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Organizacion</em>' reference.
   * @see #setOrganizacion(Organizacion)
   * @see ar.edu.unq.obj3.dsl.planificacion.planificacion.PlanificacionPackage#getOrador_Organizacion()
   * @model
   * @generated
   */
  Organizacion getOrganizacion();

  /**
   * Sets the value of the '{@link ar.edu.unq.obj3.dsl.planificacion.planificacion.Orador#getOrganizacion <em>Organizacion</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Organizacion</em>' reference.
   * @see #getOrganizacion()
   * @generated
   */
  void setOrganizacion(Organizacion value);

} // Orador
