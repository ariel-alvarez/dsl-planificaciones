package ar.edu.unq.obj3.dsl.planificacion.validation

import ar.edu.unq.obj3.dsl.planificacion.planificacion.Track
import ar.edu.unq.obj3.dsl.planificacion.planificacion.Actividad
import java.util.List
import java.util.ArrayList

class ActividadExtension {
		def Track getTrack(Actividad actividad){
			return (actividad.eContainer as Track);
		}
		
		def boolean tieneOradoresConMismasOrganizaciones(Actividad actividad){
			val oradores = actividad.oradores.map[organizacion]
			oradores.tieneRepetidos
		}
		
		def duracion(Track track){
			track.actividades.map[fin-comienzo].reduce[x, y | x + y]
		}
		
		def horas(Integer minutos ){
			minutos/60
		}
		
		def <T> operator_plus(List<T> e1, List<T> e2){
			val union = new ArrayList<T>();
			union.addAll(e1)
			union.addAll(e2)
			union
		}
		
		def tieneRepetidos(List<?> e){
			e.size != e.toSet.size
		}

}