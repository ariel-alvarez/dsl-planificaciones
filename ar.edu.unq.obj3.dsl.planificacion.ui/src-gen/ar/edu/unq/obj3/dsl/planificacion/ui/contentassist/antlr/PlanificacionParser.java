/*
* generated by Xtext
*/
package ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.AbstractContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.FollowElement;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;

import com.google.inject.Inject;

import ar.edu.unq.obj3.dsl.planificacion.services.PlanificacionGrammarAccess;

public class PlanificacionParser extends AbstractContentAssistParser {
	
	@Inject
	private PlanificacionGrammarAccess grammarAccess;
	
	private Map<AbstractElement, String> nameMappings;
	
	@Override
	protected ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal.InternalPlanificacionParser createParser() {
		ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal.InternalPlanificacionParser result = new ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal.InternalPlanificacionParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}
	
	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getActividadAccess().getAlternatives_5(), "rule__Actividad__Alternatives_5");
					put(grammarAccess.getEventoModelAccess().getGroup(), "rule__EventoModel__Group__0");
					put(grammarAccess.getDiaAccess().getGroup(), "rule__Dia__Group__0");
					put(grammarAccess.getEspacioAccess().getGroup(), "rule__Espacio__Group__0");
					put(grammarAccess.getTrackAccess().getGroup(), "rule__Track__Group__0");
					put(grammarAccess.getActividadAccess().getGroup(), "rule__Actividad__Group__0");
					put(grammarAccess.getOrganizacionAccess().getGroup(), "rule__Organizacion__Group__0");
					put(grammarAccess.getOradorAccess().getGroup(), "rule__Orador__Group__0");
					put(grammarAccess.getEventoModelAccess().getTitleAssignment_1(), "rule__EventoModel__TitleAssignment_1");
					put(grammarAccess.getEventoModelAccess().getEspaciosAssignment_4(), "rule__EventoModel__EspaciosAssignment_4");
					put(grammarAccess.getEventoModelAccess().getOrganizacionesAssignment_6(), "rule__EventoModel__OrganizacionesAssignment_6");
					put(grammarAccess.getEventoModelAccess().getOradoresAssignment_8(), "rule__EventoModel__OradoresAssignment_8");
					put(grammarAccess.getEventoModelAccess().getDiasAssignment_9(), "rule__EventoModel__DiasAssignment_9");
					put(grammarAccess.getDiaAccess().getDiaAssignment_1(), "rule__Dia__DiaAssignment_1");
					put(grammarAccess.getDiaAccess().getBloquesAssignment_3(), "rule__Dia__BloquesAssignment_3");
					put(grammarAccess.getEspacioAccess().getNameAssignment_1(), "rule__Espacio__NameAssignment_1");
					put(grammarAccess.getEspacioAccess().getCapacidadAssignment_3(), "rule__Espacio__CapacidadAssignment_3");
					put(grammarAccess.getEspacioAccess().getLaboratorioAssignment_4(), "rule__Espacio__LaboratorioAssignment_4");
					put(grammarAccess.getTrackAccess().getTitleAssignment_1(), "rule__Track__TitleAssignment_1");
					put(grammarAccess.getTrackAccess().getEspacioAssignment_3(), "rule__Track__EspacioAssignment_3");
					put(grammarAccess.getTrackAccess().getActividadesAssignment_5(), "rule__Track__ActividadesAssignment_5");
					put(grammarAccess.getActividadAccess().getComienzoAssignment_1(), "rule__Actividad__ComienzoAssignment_1");
					put(grammarAccess.getActividadAccess().getFinAssignment_3(), "rule__Actividad__FinAssignment_3");
					put(grammarAccess.getActividadAccess().getMesaAssignment_5_0(), "rule__Actividad__MesaAssignment_5_0");
					put(grammarAccess.getActividadAccess().getCharlaAssignment_5_1(), "rule__Actividad__CharlaAssignment_5_1");
					put(grammarAccess.getActividadAccess().getTallerAssignment_5_2(), "rule__Actividad__TallerAssignment_5_2");
					put(grammarAccess.getActividadAccess().getTituloAssignment_6(), "rule__Actividad__TituloAssignment_6");
					put(grammarAccess.getActividadAccess().getOradoresAssignment_8(), "rule__Actividad__OradoresAssignment_8");
					put(grammarAccess.getActividadAccess().getOyentesAssignment_10(), "rule__Actividad__OyentesAssignment_10");
					put(grammarAccess.getOrganizacionAccess().getNameAssignment_1(), "rule__Organizacion__NameAssignment_1");
					put(grammarAccess.getOrganizacionAccess().getRazonSocialAssignment_3(), "rule__Organizacion__RazonSocialAssignment_3");
					put(grammarAccess.getOradorAccess().getNameAssignment_1(), "rule__Orador__NameAssignment_1");
					put(grammarAccess.getOradorAccess().getNombreCompletoAssignment_3(), "rule__Orador__NombreCompletoAssignment_3");
					put(grammarAccess.getOradorAccess().getOrganizacionAssignment_4(), "rule__Orador__OrganizacionAssignment_4");
				}
			};
		}
		return nameMappings.get(element);
	}
	
	@Override
	protected Collection<FollowElement> getFollowElements(AbstractInternalContentAssistParser parser) {
		try {
			ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal.InternalPlanificacionParser typedParser = (ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal.InternalPlanificacionParser) parser;
			typedParser.entryRuleEventoModel();
			return typedParser.getFollowElements();
		} catch(RecognitionException ex) {
			throw new RuntimeException(ex);
		}		
	}
	
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}
	
	public PlanificacionGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(PlanificacionGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
