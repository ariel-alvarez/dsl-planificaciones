package ar.edu.unq.obj3.dsl.planificacion.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import ar.edu.unq.obj3.dsl.planificacion.services.PlanificacionGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPlanificacionParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Evento'", "'{'", "'espacios'", "'organizaciones'", "'oradores'", "'}'", "'Dia'", "'Espacio'", "'capacidad'", "'Bloque'", "'en'", "'['", "'-'", "']'", "'se esperan'", "'oyentes'", "'Org'", "':'", "'Orador'", "'con computadoras'", "'mesa'", "'charla'", "'taller'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalPlanificacionParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPlanificacionParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPlanificacionParser.tokenNames; }
    public String getGrammarFileName() { return "../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g"; }


     
     	private PlanificacionGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(PlanificacionGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleEventoModel"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:60:1: entryRuleEventoModel : ruleEventoModel EOF ;
    public final void entryRuleEventoModel() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:61:1: ( ruleEventoModel EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:62:1: ruleEventoModel EOF
            {
             before(grammarAccess.getEventoModelRule()); 
            pushFollow(FOLLOW_ruleEventoModel_in_entryRuleEventoModel61);
            ruleEventoModel();

            state._fsp--;

             after(grammarAccess.getEventoModelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEventoModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEventoModel"


    // $ANTLR start "ruleEventoModel"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:69:1: ruleEventoModel : ( ( rule__EventoModel__Group__0 ) ) ;
    public final void ruleEventoModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:73:2: ( ( ( rule__EventoModel__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:74:1: ( ( rule__EventoModel__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:74:1: ( ( rule__EventoModel__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:75:1: ( rule__EventoModel__Group__0 )
            {
             before(grammarAccess.getEventoModelAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:76:1: ( rule__EventoModel__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:76:2: rule__EventoModel__Group__0
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__0_in_ruleEventoModel94);
            rule__EventoModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEventoModel"


    // $ANTLR start "entryRuleDia"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:88:1: entryRuleDia : ruleDia EOF ;
    public final void entryRuleDia() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:89:1: ( ruleDia EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:90:1: ruleDia EOF
            {
             before(grammarAccess.getDiaRule()); 
            pushFollow(FOLLOW_ruleDia_in_entryRuleDia121);
            ruleDia();

            state._fsp--;

             after(grammarAccess.getDiaRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDia128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDia"


    // $ANTLR start "ruleDia"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:97:1: ruleDia : ( ( rule__Dia__Group__0 ) ) ;
    public final void ruleDia() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:101:2: ( ( ( rule__Dia__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:102:1: ( ( rule__Dia__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:102:1: ( ( rule__Dia__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:103:1: ( rule__Dia__Group__0 )
            {
             before(grammarAccess.getDiaAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:104:1: ( rule__Dia__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:104:2: rule__Dia__Group__0
            {
            pushFollow(FOLLOW_rule__Dia__Group__0_in_ruleDia154);
            rule__Dia__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDiaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDia"


    // $ANTLR start "entryRuleEspacio"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:116:1: entryRuleEspacio : ruleEspacio EOF ;
    public final void entryRuleEspacio() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:117:1: ( ruleEspacio EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:118:1: ruleEspacio EOF
            {
             before(grammarAccess.getEspacioRule()); 
            pushFollow(FOLLOW_ruleEspacio_in_entryRuleEspacio181);
            ruleEspacio();

            state._fsp--;

             after(grammarAccess.getEspacioRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleEspacio188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEspacio"


    // $ANTLR start "ruleEspacio"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:125:1: ruleEspacio : ( ( rule__Espacio__Group__0 ) ) ;
    public final void ruleEspacio() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:129:2: ( ( ( rule__Espacio__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:130:1: ( ( rule__Espacio__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:130:1: ( ( rule__Espacio__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:131:1: ( rule__Espacio__Group__0 )
            {
             before(grammarAccess.getEspacioAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:132:1: ( rule__Espacio__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:132:2: rule__Espacio__Group__0
            {
            pushFollow(FOLLOW_rule__Espacio__Group__0_in_ruleEspacio214);
            rule__Espacio__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEspacioAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEspacio"


    // $ANTLR start "entryRuleTrack"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:144:1: entryRuleTrack : ruleTrack EOF ;
    public final void entryRuleTrack() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:145:1: ( ruleTrack EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:146:1: ruleTrack EOF
            {
             before(grammarAccess.getTrackRule()); 
            pushFollow(FOLLOW_ruleTrack_in_entryRuleTrack241);
            ruleTrack();

            state._fsp--;

             after(grammarAccess.getTrackRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTrack248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTrack"


    // $ANTLR start "ruleTrack"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:153:1: ruleTrack : ( ( rule__Track__Group__0 ) ) ;
    public final void ruleTrack() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:157:2: ( ( ( rule__Track__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:158:1: ( ( rule__Track__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:158:1: ( ( rule__Track__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:159:1: ( rule__Track__Group__0 )
            {
             before(grammarAccess.getTrackAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:160:1: ( rule__Track__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:160:2: rule__Track__Group__0
            {
            pushFollow(FOLLOW_rule__Track__Group__0_in_ruleTrack274);
            rule__Track__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTrackAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTrack"


    // $ANTLR start "entryRuleActividad"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:172:1: entryRuleActividad : ruleActividad EOF ;
    public final void entryRuleActividad() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:173:1: ( ruleActividad EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:174:1: ruleActividad EOF
            {
             before(grammarAccess.getActividadRule()); 
            pushFollow(FOLLOW_ruleActividad_in_entryRuleActividad301);
            ruleActividad();

            state._fsp--;

             after(grammarAccess.getActividadRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleActividad308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleActividad"


    // $ANTLR start "ruleActividad"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:181:1: ruleActividad : ( ( rule__Actividad__Group__0 ) ) ;
    public final void ruleActividad() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:185:2: ( ( ( rule__Actividad__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:186:1: ( ( rule__Actividad__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:186:1: ( ( rule__Actividad__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:187:1: ( rule__Actividad__Group__0 )
            {
             before(grammarAccess.getActividadAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:188:1: ( rule__Actividad__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:188:2: rule__Actividad__Group__0
            {
            pushFollow(FOLLOW_rule__Actividad__Group__0_in_ruleActividad334);
            rule__Actividad__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleActividad"


    // $ANTLR start "entryRuleOrganizacion"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:200:1: entryRuleOrganizacion : ruleOrganizacion EOF ;
    public final void entryRuleOrganizacion() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:201:1: ( ruleOrganizacion EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:202:1: ruleOrganizacion EOF
            {
             before(grammarAccess.getOrganizacionRule()); 
            pushFollow(FOLLOW_ruleOrganizacion_in_entryRuleOrganizacion361);
            ruleOrganizacion();

            state._fsp--;

             after(grammarAccess.getOrganizacionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrganizacion368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrganizacion"


    // $ANTLR start "ruleOrganizacion"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:209:1: ruleOrganizacion : ( ( rule__Organizacion__Group__0 ) ) ;
    public final void ruleOrganizacion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:213:2: ( ( ( rule__Organizacion__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:214:1: ( ( rule__Organizacion__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:214:1: ( ( rule__Organizacion__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:215:1: ( rule__Organizacion__Group__0 )
            {
             before(grammarAccess.getOrganizacionAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:216:1: ( rule__Organizacion__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:216:2: rule__Organizacion__Group__0
            {
            pushFollow(FOLLOW_rule__Organizacion__Group__0_in_ruleOrganizacion394);
            rule__Organizacion__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrganizacionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrganizacion"


    // $ANTLR start "entryRuleOrador"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:228:1: entryRuleOrador : ruleOrador EOF ;
    public final void entryRuleOrador() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:229:1: ( ruleOrador EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:230:1: ruleOrador EOF
            {
             before(grammarAccess.getOradorRule()); 
            pushFollow(FOLLOW_ruleOrador_in_entryRuleOrador421);
            ruleOrador();

            state._fsp--;

             after(grammarAccess.getOradorRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrador428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrador"


    // $ANTLR start "ruleOrador"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:237:1: ruleOrador : ( ( rule__Orador__Group__0 ) ) ;
    public final void ruleOrador() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:241:2: ( ( ( rule__Orador__Group__0 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:242:1: ( ( rule__Orador__Group__0 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:242:1: ( ( rule__Orador__Group__0 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:243:1: ( rule__Orador__Group__0 )
            {
             before(grammarAccess.getOradorAccess().getGroup()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:244:1: ( rule__Orador__Group__0 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:244:2: rule__Orador__Group__0
            {
            pushFollow(FOLLOW_rule__Orador__Group__0_in_ruleOrador454);
            rule__Orador__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOradorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrador"


    // $ANTLR start "entryRuleHOUR"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:256:1: entryRuleHOUR : ruleHOUR EOF ;
    public final void entryRuleHOUR() throws RecognitionException {
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:257:1: ( ruleHOUR EOF )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:258:1: ruleHOUR EOF
            {
             before(grammarAccess.getHOURRule()); 
            pushFollow(FOLLOW_ruleHOUR_in_entryRuleHOUR481);
            ruleHOUR();

            state._fsp--;

             after(grammarAccess.getHOURRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleHOUR488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHOUR"


    // $ANTLR start "ruleHOUR"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:265:1: ruleHOUR : ( RULE_STRING ) ;
    public final void ruleHOUR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:269:2: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:270:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:270:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:271:1: RULE_STRING
            {
             before(grammarAccess.getHOURAccess().getSTRINGTerminalRuleCall()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleHOUR514); 
             after(grammarAccess.getHOURAccess().getSTRINGTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHOUR"


    // $ANTLR start "rule__Actividad__Alternatives_5"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:284:1: rule__Actividad__Alternatives_5 : ( ( ( rule__Actividad__MesaAssignment_5_0 ) ) | ( ( rule__Actividad__CharlaAssignment_5_1 ) ) | ( ( rule__Actividad__TallerAssignment_5_2 ) ) );
    public final void rule__Actividad__Alternatives_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:288:1: ( ( ( rule__Actividad__MesaAssignment_5_0 ) ) | ( ( rule__Actividad__CharlaAssignment_5_1 ) ) | ( ( rule__Actividad__TallerAssignment_5_2 ) ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt1=1;
                }
                break;
            case 32:
                {
                alt1=2;
                }
                break;
            case 33:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:289:1: ( ( rule__Actividad__MesaAssignment_5_0 ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:289:1: ( ( rule__Actividad__MesaAssignment_5_0 ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:290:1: ( rule__Actividad__MesaAssignment_5_0 )
                    {
                     before(grammarAccess.getActividadAccess().getMesaAssignment_5_0()); 
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:291:1: ( rule__Actividad__MesaAssignment_5_0 )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:291:2: rule__Actividad__MesaAssignment_5_0
                    {
                    pushFollow(FOLLOW_rule__Actividad__MesaAssignment_5_0_in_rule__Actividad__Alternatives_5549);
                    rule__Actividad__MesaAssignment_5_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getActividadAccess().getMesaAssignment_5_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:295:6: ( ( rule__Actividad__CharlaAssignment_5_1 ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:295:6: ( ( rule__Actividad__CharlaAssignment_5_1 ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:296:1: ( rule__Actividad__CharlaAssignment_5_1 )
                    {
                     before(grammarAccess.getActividadAccess().getCharlaAssignment_5_1()); 
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:297:1: ( rule__Actividad__CharlaAssignment_5_1 )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:297:2: rule__Actividad__CharlaAssignment_5_1
                    {
                    pushFollow(FOLLOW_rule__Actividad__CharlaAssignment_5_1_in_rule__Actividad__Alternatives_5567);
                    rule__Actividad__CharlaAssignment_5_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getActividadAccess().getCharlaAssignment_5_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:301:6: ( ( rule__Actividad__TallerAssignment_5_2 ) )
                    {
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:301:6: ( ( rule__Actividad__TallerAssignment_5_2 ) )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:302:1: ( rule__Actividad__TallerAssignment_5_2 )
                    {
                     before(grammarAccess.getActividadAccess().getTallerAssignment_5_2()); 
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:303:1: ( rule__Actividad__TallerAssignment_5_2 )
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:303:2: rule__Actividad__TallerAssignment_5_2
                    {
                    pushFollow(FOLLOW_rule__Actividad__TallerAssignment_5_2_in_rule__Actividad__Alternatives_5585);
                    rule__Actividad__TallerAssignment_5_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getActividadAccess().getTallerAssignment_5_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Alternatives_5"


    // $ANTLR start "rule__EventoModel__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:314:1: rule__EventoModel__Group__0 : rule__EventoModel__Group__0__Impl rule__EventoModel__Group__1 ;
    public final void rule__EventoModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:318:1: ( rule__EventoModel__Group__0__Impl rule__EventoModel__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:319:2: rule__EventoModel__Group__0__Impl rule__EventoModel__Group__1
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__0__Impl_in_rule__EventoModel__Group__0616);
            rule__EventoModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__1_in_rule__EventoModel__Group__0619);
            rule__EventoModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__0"


    // $ANTLR start "rule__EventoModel__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:326:1: rule__EventoModel__Group__0__Impl : ( 'Evento' ) ;
    public final void rule__EventoModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:330:1: ( ( 'Evento' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:331:1: ( 'Evento' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:331:1: ( 'Evento' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:332:1: 'Evento'
            {
             before(grammarAccess.getEventoModelAccess().getEventoKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__EventoModel__Group__0__Impl647); 
             after(grammarAccess.getEventoModelAccess().getEventoKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__0__Impl"


    // $ANTLR start "rule__EventoModel__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:345:1: rule__EventoModel__Group__1 : rule__EventoModel__Group__1__Impl rule__EventoModel__Group__2 ;
    public final void rule__EventoModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:349:1: ( rule__EventoModel__Group__1__Impl rule__EventoModel__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:350:2: rule__EventoModel__Group__1__Impl rule__EventoModel__Group__2
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__1__Impl_in_rule__EventoModel__Group__1678);
            rule__EventoModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__2_in_rule__EventoModel__Group__1681);
            rule__EventoModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__1"


    // $ANTLR start "rule__EventoModel__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:357:1: rule__EventoModel__Group__1__Impl : ( ( rule__EventoModel__TitleAssignment_1 ) ) ;
    public final void rule__EventoModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:361:1: ( ( ( rule__EventoModel__TitleAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:362:1: ( ( rule__EventoModel__TitleAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:362:1: ( ( rule__EventoModel__TitleAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:363:1: ( rule__EventoModel__TitleAssignment_1 )
            {
             before(grammarAccess.getEventoModelAccess().getTitleAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:364:1: ( rule__EventoModel__TitleAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:364:2: rule__EventoModel__TitleAssignment_1
            {
            pushFollow(FOLLOW_rule__EventoModel__TitleAssignment_1_in_rule__EventoModel__Group__1__Impl708);
            rule__EventoModel__TitleAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getTitleAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__1__Impl"


    // $ANTLR start "rule__EventoModel__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:374:1: rule__EventoModel__Group__2 : rule__EventoModel__Group__2__Impl rule__EventoModel__Group__3 ;
    public final void rule__EventoModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:378:1: ( rule__EventoModel__Group__2__Impl rule__EventoModel__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:379:2: rule__EventoModel__Group__2__Impl rule__EventoModel__Group__3
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__2__Impl_in_rule__EventoModel__Group__2738);
            rule__EventoModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__3_in_rule__EventoModel__Group__2741);
            rule__EventoModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__2"


    // $ANTLR start "rule__EventoModel__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:386:1: rule__EventoModel__Group__2__Impl : ( '{' ) ;
    public final void rule__EventoModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:390:1: ( ( '{' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:391:1: ( '{' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:391:1: ( '{' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:392:1: '{'
            {
             before(grammarAccess.getEventoModelAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__EventoModel__Group__2__Impl769); 
             after(grammarAccess.getEventoModelAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__2__Impl"


    // $ANTLR start "rule__EventoModel__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:405:1: rule__EventoModel__Group__3 : rule__EventoModel__Group__3__Impl rule__EventoModel__Group__4 ;
    public final void rule__EventoModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:409:1: ( rule__EventoModel__Group__3__Impl rule__EventoModel__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:410:2: rule__EventoModel__Group__3__Impl rule__EventoModel__Group__4
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__3__Impl_in_rule__EventoModel__Group__3800);
            rule__EventoModel__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__4_in_rule__EventoModel__Group__3803);
            rule__EventoModel__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__3"


    // $ANTLR start "rule__EventoModel__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:417:1: rule__EventoModel__Group__3__Impl : ( 'espacios' ) ;
    public final void rule__EventoModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:421:1: ( ( 'espacios' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:422:1: ( 'espacios' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:422:1: ( 'espacios' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:423:1: 'espacios'
            {
             before(grammarAccess.getEventoModelAccess().getEspaciosKeyword_3()); 
            match(input,13,FOLLOW_13_in_rule__EventoModel__Group__3__Impl831); 
             after(grammarAccess.getEventoModelAccess().getEspaciosKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__3__Impl"


    // $ANTLR start "rule__EventoModel__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:436:1: rule__EventoModel__Group__4 : rule__EventoModel__Group__4__Impl rule__EventoModel__Group__5 ;
    public final void rule__EventoModel__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:440:1: ( rule__EventoModel__Group__4__Impl rule__EventoModel__Group__5 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:441:2: rule__EventoModel__Group__4__Impl rule__EventoModel__Group__5
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__4__Impl_in_rule__EventoModel__Group__4862);
            rule__EventoModel__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__5_in_rule__EventoModel__Group__4865);
            rule__EventoModel__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__4"


    // $ANTLR start "rule__EventoModel__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:448:1: rule__EventoModel__Group__4__Impl : ( ( ( rule__EventoModel__EspaciosAssignment_4 ) ) ( ( rule__EventoModel__EspaciosAssignment_4 )* ) ) ;
    public final void rule__EventoModel__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:452:1: ( ( ( ( rule__EventoModel__EspaciosAssignment_4 ) ) ( ( rule__EventoModel__EspaciosAssignment_4 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:453:1: ( ( ( rule__EventoModel__EspaciosAssignment_4 ) ) ( ( rule__EventoModel__EspaciosAssignment_4 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:453:1: ( ( ( rule__EventoModel__EspaciosAssignment_4 ) ) ( ( rule__EventoModel__EspaciosAssignment_4 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:454:1: ( ( rule__EventoModel__EspaciosAssignment_4 ) ) ( ( rule__EventoModel__EspaciosAssignment_4 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:454:1: ( ( rule__EventoModel__EspaciosAssignment_4 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:455:1: ( rule__EventoModel__EspaciosAssignment_4 )
            {
             before(grammarAccess.getEventoModelAccess().getEspaciosAssignment_4()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:456:1: ( rule__EventoModel__EspaciosAssignment_4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:456:2: rule__EventoModel__EspaciosAssignment_4
            {
            pushFollow(FOLLOW_rule__EventoModel__EspaciosAssignment_4_in_rule__EventoModel__Group__4__Impl894);
            rule__EventoModel__EspaciosAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getEspaciosAssignment_4()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:459:1: ( ( rule__EventoModel__EspaciosAssignment_4 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:460:1: ( rule__EventoModel__EspaciosAssignment_4 )*
            {
             before(grammarAccess.getEventoModelAccess().getEspaciosAssignment_4()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:461:1: ( rule__EventoModel__EspaciosAssignment_4 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==18) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:461:2: rule__EventoModel__EspaciosAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__EventoModel__EspaciosAssignment_4_in_rule__EventoModel__Group__4__Impl906);
            	    rule__EventoModel__EspaciosAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getEventoModelAccess().getEspaciosAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__4__Impl"


    // $ANTLR start "rule__EventoModel__Group__5"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:472:1: rule__EventoModel__Group__5 : rule__EventoModel__Group__5__Impl rule__EventoModel__Group__6 ;
    public final void rule__EventoModel__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:476:1: ( rule__EventoModel__Group__5__Impl rule__EventoModel__Group__6 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:477:2: rule__EventoModel__Group__5__Impl rule__EventoModel__Group__6
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__5__Impl_in_rule__EventoModel__Group__5939);
            rule__EventoModel__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__6_in_rule__EventoModel__Group__5942);
            rule__EventoModel__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__5"


    // $ANTLR start "rule__EventoModel__Group__5__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:484:1: rule__EventoModel__Group__5__Impl : ( 'organizaciones' ) ;
    public final void rule__EventoModel__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:488:1: ( ( 'organizaciones' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:489:1: ( 'organizaciones' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:489:1: ( 'organizaciones' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:490:1: 'organizaciones'
            {
             before(grammarAccess.getEventoModelAccess().getOrganizacionesKeyword_5()); 
            match(input,14,FOLLOW_14_in_rule__EventoModel__Group__5__Impl970); 
             after(grammarAccess.getEventoModelAccess().getOrganizacionesKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__5__Impl"


    // $ANTLR start "rule__EventoModel__Group__6"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:503:1: rule__EventoModel__Group__6 : rule__EventoModel__Group__6__Impl rule__EventoModel__Group__7 ;
    public final void rule__EventoModel__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:507:1: ( rule__EventoModel__Group__6__Impl rule__EventoModel__Group__7 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:508:2: rule__EventoModel__Group__6__Impl rule__EventoModel__Group__7
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__6__Impl_in_rule__EventoModel__Group__61001);
            rule__EventoModel__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__7_in_rule__EventoModel__Group__61004);
            rule__EventoModel__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__6"


    // $ANTLR start "rule__EventoModel__Group__6__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:515:1: rule__EventoModel__Group__6__Impl : ( ( ( rule__EventoModel__OrganizacionesAssignment_6 ) ) ( ( rule__EventoModel__OrganizacionesAssignment_6 )* ) ) ;
    public final void rule__EventoModel__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:519:1: ( ( ( ( rule__EventoModel__OrganizacionesAssignment_6 ) ) ( ( rule__EventoModel__OrganizacionesAssignment_6 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:520:1: ( ( ( rule__EventoModel__OrganizacionesAssignment_6 ) ) ( ( rule__EventoModel__OrganizacionesAssignment_6 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:520:1: ( ( ( rule__EventoModel__OrganizacionesAssignment_6 ) ) ( ( rule__EventoModel__OrganizacionesAssignment_6 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:521:1: ( ( rule__EventoModel__OrganizacionesAssignment_6 ) ) ( ( rule__EventoModel__OrganizacionesAssignment_6 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:521:1: ( ( rule__EventoModel__OrganizacionesAssignment_6 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:522:1: ( rule__EventoModel__OrganizacionesAssignment_6 )
            {
             before(grammarAccess.getEventoModelAccess().getOrganizacionesAssignment_6()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:523:1: ( rule__EventoModel__OrganizacionesAssignment_6 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:523:2: rule__EventoModel__OrganizacionesAssignment_6
            {
            pushFollow(FOLLOW_rule__EventoModel__OrganizacionesAssignment_6_in_rule__EventoModel__Group__6__Impl1033);
            rule__EventoModel__OrganizacionesAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getOrganizacionesAssignment_6()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:526:1: ( ( rule__EventoModel__OrganizacionesAssignment_6 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:527:1: ( rule__EventoModel__OrganizacionesAssignment_6 )*
            {
             before(grammarAccess.getEventoModelAccess().getOrganizacionesAssignment_6()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:528:1: ( rule__EventoModel__OrganizacionesAssignment_6 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==27) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:528:2: rule__EventoModel__OrganizacionesAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__EventoModel__OrganizacionesAssignment_6_in_rule__EventoModel__Group__6__Impl1045);
            	    rule__EventoModel__OrganizacionesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getEventoModelAccess().getOrganizacionesAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__6__Impl"


    // $ANTLR start "rule__EventoModel__Group__7"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:539:1: rule__EventoModel__Group__7 : rule__EventoModel__Group__7__Impl rule__EventoModel__Group__8 ;
    public final void rule__EventoModel__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:543:1: ( rule__EventoModel__Group__7__Impl rule__EventoModel__Group__8 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:544:2: rule__EventoModel__Group__7__Impl rule__EventoModel__Group__8
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__7__Impl_in_rule__EventoModel__Group__71078);
            rule__EventoModel__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__8_in_rule__EventoModel__Group__71081);
            rule__EventoModel__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__7"


    // $ANTLR start "rule__EventoModel__Group__7__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:551:1: rule__EventoModel__Group__7__Impl : ( 'oradores' ) ;
    public final void rule__EventoModel__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:555:1: ( ( 'oradores' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:556:1: ( 'oradores' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:556:1: ( 'oradores' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:557:1: 'oradores'
            {
             before(grammarAccess.getEventoModelAccess().getOradoresKeyword_7()); 
            match(input,15,FOLLOW_15_in_rule__EventoModel__Group__7__Impl1109); 
             after(grammarAccess.getEventoModelAccess().getOradoresKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__7__Impl"


    // $ANTLR start "rule__EventoModel__Group__8"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:570:1: rule__EventoModel__Group__8 : rule__EventoModel__Group__8__Impl rule__EventoModel__Group__9 ;
    public final void rule__EventoModel__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:574:1: ( rule__EventoModel__Group__8__Impl rule__EventoModel__Group__9 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:575:2: rule__EventoModel__Group__8__Impl rule__EventoModel__Group__9
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__8__Impl_in_rule__EventoModel__Group__81140);
            rule__EventoModel__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__9_in_rule__EventoModel__Group__81143);
            rule__EventoModel__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__8"


    // $ANTLR start "rule__EventoModel__Group__8__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:582:1: rule__EventoModel__Group__8__Impl : ( ( ( rule__EventoModel__OradoresAssignment_8 ) ) ( ( rule__EventoModel__OradoresAssignment_8 )* ) ) ;
    public final void rule__EventoModel__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:586:1: ( ( ( ( rule__EventoModel__OradoresAssignment_8 ) ) ( ( rule__EventoModel__OradoresAssignment_8 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:587:1: ( ( ( rule__EventoModel__OradoresAssignment_8 ) ) ( ( rule__EventoModel__OradoresAssignment_8 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:587:1: ( ( ( rule__EventoModel__OradoresAssignment_8 ) ) ( ( rule__EventoModel__OradoresAssignment_8 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:588:1: ( ( rule__EventoModel__OradoresAssignment_8 ) ) ( ( rule__EventoModel__OradoresAssignment_8 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:588:1: ( ( rule__EventoModel__OradoresAssignment_8 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:589:1: ( rule__EventoModel__OradoresAssignment_8 )
            {
             before(grammarAccess.getEventoModelAccess().getOradoresAssignment_8()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:590:1: ( rule__EventoModel__OradoresAssignment_8 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:590:2: rule__EventoModel__OradoresAssignment_8
            {
            pushFollow(FOLLOW_rule__EventoModel__OradoresAssignment_8_in_rule__EventoModel__Group__8__Impl1172);
            rule__EventoModel__OradoresAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getOradoresAssignment_8()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:593:1: ( ( rule__EventoModel__OradoresAssignment_8 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:594:1: ( rule__EventoModel__OradoresAssignment_8 )*
            {
             before(grammarAccess.getEventoModelAccess().getOradoresAssignment_8()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:595:1: ( rule__EventoModel__OradoresAssignment_8 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==29) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:595:2: rule__EventoModel__OradoresAssignment_8
            	    {
            	    pushFollow(FOLLOW_rule__EventoModel__OradoresAssignment_8_in_rule__EventoModel__Group__8__Impl1184);
            	    rule__EventoModel__OradoresAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getEventoModelAccess().getOradoresAssignment_8()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__8__Impl"


    // $ANTLR start "rule__EventoModel__Group__9"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:606:1: rule__EventoModel__Group__9 : rule__EventoModel__Group__9__Impl rule__EventoModel__Group__10 ;
    public final void rule__EventoModel__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:610:1: ( rule__EventoModel__Group__9__Impl rule__EventoModel__Group__10 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:611:2: rule__EventoModel__Group__9__Impl rule__EventoModel__Group__10
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__9__Impl_in_rule__EventoModel__Group__91217);
            rule__EventoModel__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__EventoModel__Group__10_in_rule__EventoModel__Group__91220);
            rule__EventoModel__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__9"


    // $ANTLR start "rule__EventoModel__Group__9__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:618:1: rule__EventoModel__Group__9__Impl : ( ( ( rule__EventoModel__DiasAssignment_9 ) ) ( ( rule__EventoModel__DiasAssignment_9 )* ) ) ;
    public final void rule__EventoModel__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:622:1: ( ( ( ( rule__EventoModel__DiasAssignment_9 ) ) ( ( rule__EventoModel__DiasAssignment_9 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:623:1: ( ( ( rule__EventoModel__DiasAssignment_9 ) ) ( ( rule__EventoModel__DiasAssignment_9 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:623:1: ( ( ( rule__EventoModel__DiasAssignment_9 ) ) ( ( rule__EventoModel__DiasAssignment_9 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:624:1: ( ( rule__EventoModel__DiasAssignment_9 ) ) ( ( rule__EventoModel__DiasAssignment_9 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:624:1: ( ( rule__EventoModel__DiasAssignment_9 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:625:1: ( rule__EventoModel__DiasAssignment_9 )
            {
             before(grammarAccess.getEventoModelAccess().getDiasAssignment_9()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:626:1: ( rule__EventoModel__DiasAssignment_9 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:626:2: rule__EventoModel__DiasAssignment_9
            {
            pushFollow(FOLLOW_rule__EventoModel__DiasAssignment_9_in_rule__EventoModel__Group__9__Impl1249);
            rule__EventoModel__DiasAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getEventoModelAccess().getDiasAssignment_9()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:629:1: ( ( rule__EventoModel__DiasAssignment_9 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:630:1: ( rule__EventoModel__DiasAssignment_9 )*
            {
             before(grammarAccess.getEventoModelAccess().getDiasAssignment_9()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:631:1: ( rule__EventoModel__DiasAssignment_9 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:631:2: rule__EventoModel__DiasAssignment_9
            	    {
            	    pushFollow(FOLLOW_rule__EventoModel__DiasAssignment_9_in_rule__EventoModel__Group__9__Impl1261);
            	    rule__EventoModel__DiasAssignment_9();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getEventoModelAccess().getDiasAssignment_9()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__9__Impl"


    // $ANTLR start "rule__EventoModel__Group__10"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:642:1: rule__EventoModel__Group__10 : rule__EventoModel__Group__10__Impl ;
    public final void rule__EventoModel__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:646:1: ( rule__EventoModel__Group__10__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:647:2: rule__EventoModel__Group__10__Impl
            {
            pushFollow(FOLLOW_rule__EventoModel__Group__10__Impl_in_rule__EventoModel__Group__101294);
            rule__EventoModel__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__10"


    // $ANTLR start "rule__EventoModel__Group__10__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:653:1: rule__EventoModel__Group__10__Impl : ( '}' ) ;
    public final void rule__EventoModel__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:657:1: ( ( '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:658:1: ( '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:658:1: ( '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:659:1: '}'
            {
             before(grammarAccess.getEventoModelAccess().getRightCurlyBracketKeyword_10()); 
            match(input,16,FOLLOW_16_in_rule__EventoModel__Group__10__Impl1322); 
             after(grammarAccess.getEventoModelAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__Group__10__Impl"


    // $ANTLR start "rule__Dia__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:694:1: rule__Dia__Group__0 : rule__Dia__Group__0__Impl rule__Dia__Group__1 ;
    public final void rule__Dia__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:698:1: ( rule__Dia__Group__0__Impl rule__Dia__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:699:2: rule__Dia__Group__0__Impl rule__Dia__Group__1
            {
            pushFollow(FOLLOW_rule__Dia__Group__0__Impl_in_rule__Dia__Group__01375);
            rule__Dia__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dia__Group__1_in_rule__Dia__Group__01378);
            rule__Dia__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__0"


    // $ANTLR start "rule__Dia__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:706:1: rule__Dia__Group__0__Impl : ( 'Dia' ) ;
    public final void rule__Dia__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:710:1: ( ( 'Dia' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:711:1: ( 'Dia' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:711:1: ( 'Dia' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:712:1: 'Dia'
            {
             before(grammarAccess.getDiaAccess().getDiaKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__Dia__Group__0__Impl1406); 
             after(grammarAccess.getDiaAccess().getDiaKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__0__Impl"


    // $ANTLR start "rule__Dia__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:725:1: rule__Dia__Group__1 : rule__Dia__Group__1__Impl rule__Dia__Group__2 ;
    public final void rule__Dia__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:729:1: ( rule__Dia__Group__1__Impl rule__Dia__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:730:2: rule__Dia__Group__1__Impl rule__Dia__Group__2
            {
            pushFollow(FOLLOW_rule__Dia__Group__1__Impl_in_rule__Dia__Group__11437);
            rule__Dia__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dia__Group__2_in_rule__Dia__Group__11440);
            rule__Dia__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__1"


    // $ANTLR start "rule__Dia__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:737:1: rule__Dia__Group__1__Impl : ( ( rule__Dia__DiaAssignment_1 ) ) ;
    public final void rule__Dia__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:741:1: ( ( ( rule__Dia__DiaAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:742:1: ( ( rule__Dia__DiaAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:742:1: ( ( rule__Dia__DiaAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:743:1: ( rule__Dia__DiaAssignment_1 )
            {
             before(grammarAccess.getDiaAccess().getDiaAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:744:1: ( rule__Dia__DiaAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:744:2: rule__Dia__DiaAssignment_1
            {
            pushFollow(FOLLOW_rule__Dia__DiaAssignment_1_in_rule__Dia__Group__1__Impl1467);
            rule__Dia__DiaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDiaAccess().getDiaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__1__Impl"


    // $ANTLR start "rule__Dia__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:754:1: rule__Dia__Group__2 : rule__Dia__Group__2__Impl rule__Dia__Group__3 ;
    public final void rule__Dia__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:758:1: ( rule__Dia__Group__2__Impl rule__Dia__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:759:2: rule__Dia__Group__2__Impl rule__Dia__Group__3
            {
            pushFollow(FOLLOW_rule__Dia__Group__2__Impl_in_rule__Dia__Group__21497);
            rule__Dia__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dia__Group__3_in_rule__Dia__Group__21500);
            rule__Dia__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__2"


    // $ANTLR start "rule__Dia__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:766:1: rule__Dia__Group__2__Impl : ( '{' ) ;
    public final void rule__Dia__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:770:1: ( ( '{' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:771:1: ( '{' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:771:1: ( '{' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:772:1: '{'
            {
             before(grammarAccess.getDiaAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Dia__Group__2__Impl1528); 
             after(grammarAccess.getDiaAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__2__Impl"


    // $ANTLR start "rule__Dia__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:785:1: rule__Dia__Group__3 : rule__Dia__Group__3__Impl rule__Dia__Group__4 ;
    public final void rule__Dia__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:789:1: ( rule__Dia__Group__3__Impl rule__Dia__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:790:2: rule__Dia__Group__3__Impl rule__Dia__Group__4
            {
            pushFollow(FOLLOW_rule__Dia__Group__3__Impl_in_rule__Dia__Group__31559);
            rule__Dia__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Dia__Group__4_in_rule__Dia__Group__31562);
            rule__Dia__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__3"


    // $ANTLR start "rule__Dia__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:797:1: rule__Dia__Group__3__Impl : ( ( ( rule__Dia__BloquesAssignment_3 ) ) ( ( rule__Dia__BloquesAssignment_3 )* ) ) ;
    public final void rule__Dia__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:801:1: ( ( ( ( rule__Dia__BloquesAssignment_3 ) ) ( ( rule__Dia__BloquesAssignment_3 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:802:1: ( ( ( rule__Dia__BloquesAssignment_3 ) ) ( ( rule__Dia__BloquesAssignment_3 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:802:1: ( ( ( rule__Dia__BloquesAssignment_3 ) ) ( ( rule__Dia__BloquesAssignment_3 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:803:1: ( ( rule__Dia__BloquesAssignment_3 ) ) ( ( rule__Dia__BloquesAssignment_3 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:803:1: ( ( rule__Dia__BloquesAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:804:1: ( rule__Dia__BloquesAssignment_3 )
            {
             before(grammarAccess.getDiaAccess().getBloquesAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:805:1: ( rule__Dia__BloquesAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:805:2: rule__Dia__BloquesAssignment_3
            {
            pushFollow(FOLLOW_rule__Dia__BloquesAssignment_3_in_rule__Dia__Group__3__Impl1591);
            rule__Dia__BloquesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDiaAccess().getBloquesAssignment_3()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:808:1: ( ( rule__Dia__BloquesAssignment_3 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:809:1: ( rule__Dia__BloquesAssignment_3 )*
            {
             before(grammarAccess.getDiaAccess().getBloquesAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:810:1: ( rule__Dia__BloquesAssignment_3 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==20) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:810:2: rule__Dia__BloquesAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Dia__BloquesAssignment_3_in_rule__Dia__Group__3__Impl1603);
            	    rule__Dia__BloquesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getDiaAccess().getBloquesAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__3__Impl"


    // $ANTLR start "rule__Dia__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:821:1: rule__Dia__Group__4 : rule__Dia__Group__4__Impl ;
    public final void rule__Dia__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:825:1: ( rule__Dia__Group__4__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:826:2: rule__Dia__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Dia__Group__4__Impl_in_rule__Dia__Group__41636);
            rule__Dia__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__4"


    // $ANTLR start "rule__Dia__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:832:1: rule__Dia__Group__4__Impl : ( '}' ) ;
    public final void rule__Dia__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:836:1: ( ( '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:837:1: ( '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:837:1: ( '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:838:1: '}'
            {
             before(grammarAccess.getDiaAccess().getRightCurlyBracketKeyword_4()); 
            match(input,16,FOLLOW_16_in_rule__Dia__Group__4__Impl1664); 
             after(grammarAccess.getDiaAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__Group__4__Impl"


    // $ANTLR start "rule__Espacio__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:861:1: rule__Espacio__Group__0 : rule__Espacio__Group__0__Impl rule__Espacio__Group__1 ;
    public final void rule__Espacio__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:865:1: ( rule__Espacio__Group__0__Impl rule__Espacio__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:866:2: rule__Espacio__Group__0__Impl rule__Espacio__Group__1
            {
            pushFollow(FOLLOW_rule__Espacio__Group__0__Impl_in_rule__Espacio__Group__01705);
            rule__Espacio__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Espacio__Group__1_in_rule__Espacio__Group__01708);
            rule__Espacio__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__0"


    // $ANTLR start "rule__Espacio__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:873:1: rule__Espacio__Group__0__Impl : ( 'Espacio' ) ;
    public final void rule__Espacio__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:877:1: ( ( 'Espacio' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:878:1: ( 'Espacio' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:878:1: ( 'Espacio' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:879:1: 'Espacio'
            {
             before(grammarAccess.getEspacioAccess().getEspacioKeyword_0()); 
            match(input,18,FOLLOW_18_in_rule__Espacio__Group__0__Impl1736); 
             after(grammarAccess.getEspacioAccess().getEspacioKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__0__Impl"


    // $ANTLR start "rule__Espacio__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:892:1: rule__Espacio__Group__1 : rule__Espacio__Group__1__Impl rule__Espacio__Group__2 ;
    public final void rule__Espacio__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:896:1: ( rule__Espacio__Group__1__Impl rule__Espacio__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:897:2: rule__Espacio__Group__1__Impl rule__Espacio__Group__2
            {
            pushFollow(FOLLOW_rule__Espacio__Group__1__Impl_in_rule__Espacio__Group__11767);
            rule__Espacio__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Espacio__Group__2_in_rule__Espacio__Group__11770);
            rule__Espacio__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__1"


    // $ANTLR start "rule__Espacio__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:904:1: rule__Espacio__Group__1__Impl : ( ( rule__Espacio__NameAssignment_1 ) ) ;
    public final void rule__Espacio__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:908:1: ( ( ( rule__Espacio__NameAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:909:1: ( ( rule__Espacio__NameAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:909:1: ( ( rule__Espacio__NameAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:910:1: ( rule__Espacio__NameAssignment_1 )
            {
             before(grammarAccess.getEspacioAccess().getNameAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:911:1: ( rule__Espacio__NameAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:911:2: rule__Espacio__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Espacio__NameAssignment_1_in_rule__Espacio__Group__1__Impl1797);
            rule__Espacio__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEspacioAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__1__Impl"


    // $ANTLR start "rule__Espacio__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:921:1: rule__Espacio__Group__2 : rule__Espacio__Group__2__Impl rule__Espacio__Group__3 ;
    public final void rule__Espacio__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:925:1: ( rule__Espacio__Group__2__Impl rule__Espacio__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:926:2: rule__Espacio__Group__2__Impl rule__Espacio__Group__3
            {
            pushFollow(FOLLOW_rule__Espacio__Group__2__Impl_in_rule__Espacio__Group__21827);
            rule__Espacio__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Espacio__Group__3_in_rule__Espacio__Group__21830);
            rule__Espacio__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__2"


    // $ANTLR start "rule__Espacio__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:933:1: rule__Espacio__Group__2__Impl : ( 'capacidad' ) ;
    public final void rule__Espacio__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:937:1: ( ( 'capacidad' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:938:1: ( 'capacidad' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:938:1: ( 'capacidad' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:939:1: 'capacidad'
            {
             before(grammarAccess.getEspacioAccess().getCapacidadKeyword_2()); 
            match(input,19,FOLLOW_19_in_rule__Espacio__Group__2__Impl1858); 
             after(grammarAccess.getEspacioAccess().getCapacidadKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__2__Impl"


    // $ANTLR start "rule__Espacio__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:952:1: rule__Espacio__Group__3 : rule__Espacio__Group__3__Impl rule__Espacio__Group__4 ;
    public final void rule__Espacio__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:956:1: ( rule__Espacio__Group__3__Impl rule__Espacio__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:957:2: rule__Espacio__Group__3__Impl rule__Espacio__Group__4
            {
            pushFollow(FOLLOW_rule__Espacio__Group__3__Impl_in_rule__Espacio__Group__31889);
            rule__Espacio__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Espacio__Group__4_in_rule__Espacio__Group__31892);
            rule__Espacio__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__3"


    // $ANTLR start "rule__Espacio__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:964:1: rule__Espacio__Group__3__Impl : ( ( rule__Espacio__CapacidadAssignment_3 ) ) ;
    public final void rule__Espacio__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:968:1: ( ( ( rule__Espacio__CapacidadAssignment_3 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:969:1: ( ( rule__Espacio__CapacidadAssignment_3 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:969:1: ( ( rule__Espacio__CapacidadAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:970:1: ( rule__Espacio__CapacidadAssignment_3 )
            {
             before(grammarAccess.getEspacioAccess().getCapacidadAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:971:1: ( rule__Espacio__CapacidadAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:971:2: rule__Espacio__CapacidadAssignment_3
            {
            pushFollow(FOLLOW_rule__Espacio__CapacidadAssignment_3_in_rule__Espacio__Group__3__Impl1919);
            rule__Espacio__CapacidadAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getEspacioAccess().getCapacidadAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__3__Impl"


    // $ANTLR start "rule__Espacio__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:981:1: rule__Espacio__Group__4 : rule__Espacio__Group__4__Impl ;
    public final void rule__Espacio__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:985:1: ( rule__Espacio__Group__4__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:986:2: rule__Espacio__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Espacio__Group__4__Impl_in_rule__Espacio__Group__41949);
            rule__Espacio__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__4"


    // $ANTLR start "rule__Espacio__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:992:1: rule__Espacio__Group__4__Impl : ( ( rule__Espacio__LaboratorioAssignment_4 )? ) ;
    public final void rule__Espacio__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:996:1: ( ( ( rule__Espacio__LaboratorioAssignment_4 )? ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:997:1: ( ( rule__Espacio__LaboratorioAssignment_4 )? )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:997:1: ( ( rule__Espacio__LaboratorioAssignment_4 )? )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:998:1: ( rule__Espacio__LaboratorioAssignment_4 )?
            {
             before(grammarAccess.getEspacioAccess().getLaboratorioAssignment_4()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:999:1: ( rule__Espacio__LaboratorioAssignment_4 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==30) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:999:2: rule__Espacio__LaboratorioAssignment_4
                    {
                    pushFollow(FOLLOW_rule__Espacio__LaboratorioAssignment_4_in_rule__Espacio__Group__4__Impl1976);
                    rule__Espacio__LaboratorioAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEspacioAccess().getLaboratorioAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__Group__4__Impl"


    // $ANTLR start "rule__Track__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1019:1: rule__Track__Group__0 : rule__Track__Group__0__Impl rule__Track__Group__1 ;
    public final void rule__Track__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1023:1: ( rule__Track__Group__0__Impl rule__Track__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1024:2: rule__Track__Group__0__Impl rule__Track__Group__1
            {
            pushFollow(FOLLOW_rule__Track__Group__0__Impl_in_rule__Track__Group__02017);
            rule__Track__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__1_in_rule__Track__Group__02020);
            rule__Track__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__0"


    // $ANTLR start "rule__Track__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1031:1: rule__Track__Group__0__Impl : ( 'Bloque' ) ;
    public final void rule__Track__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1035:1: ( ( 'Bloque' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1036:1: ( 'Bloque' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1036:1: ( 'Bloque' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1037:1: 'Bloque'
            {
             before(grammarAccess.getTrackAccess().getBloqueKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__Track__Group__0__Impl2048); 
             after(grammarAccess.getTrackAccess().getBloqueKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__0__Impl"


    // $ANTLR start "rule__Track__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1050:1: rule__Track__Group__1 : rule__Track__Group__1__Impl rule__Track__Group__2 ;
    public final void rule__Track__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1054:1: ( rule__Track__Group__1__Impl rule__Track__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1055:2: rule__Track__Group__1__Impl rule__Track__Group__2
            {
            pushFollow(FOLLOW_rule__Track__Group__1__Impl_in_rule__Track__Group__12079);
            rule__Track__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__2_in_rule__Track__Group__12082);
            rule__Track__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__1"


    // $ANTLR start "rule__Track__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1062:1: rule__Track__Group__1__Impl : ( ( rule__Track__TitleAssignment_1 ) ) ;
    public final void rule__Track__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1066:1: ( ( ( rule__Track__TitleAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1067:1: ( ( rule__Track__TitleAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1067:1: ( ( rule__Track__TitleAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1068:1: ( rule__Track__TitleAssignment_1 )
            {
             before(grammarAccess.getTrackAccess().getTitleAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1069:1: ( rule__Track__TitleAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1069:2: rule__Track__TitleAssignment_1
            {
            pushFollow(FOLLOW_rule__Track__TitleAssignment_1_in_rule__Track__Group__1__Impl2109);
            rule__Track__TitleAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTrackAccess().getTitleAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__1__Impl"


    // $ANTLR start "rule__Track__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1079:1: rule__Track__Group__2 : rule__Track__Group__2__Impl rule__Track__Group__3 ;
    public final void rule__Track__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1083:1: ( rule__Track__Group__2__Impl rule__Track__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1084:2: rule__Track__Group__2__Impl rule__Track__Group__3
            {
            pushFollow(FOLLOW_rule__Track__Group__2__Impl_in_rule__Track__Group__22139);
            rule__Track__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__3_in_rule__Track__Group__22142);
            rule__Track__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__2"


    // $ANTLR start "rule__Track__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1091:1: rule__Track__Group__2__Impl : ( 'en' ) ;
    public final void rule__Track__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1095:1: ( ( 'en' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1096:1: ( 'en' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1096:1: ( 'en' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1097:1: 'en'
            {
             before(grammarAccess.getTrackAccess().getEnKeyword_2()); 
            match(input,21,FOLLOW_21_in_rule__Track__Group__2__Impl2170); 
             after(grammarAccess.getTrackAccess().getEnKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__2__Impl"


    // $ANTLR start "rule__Track__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1110:1: rule__Track__Group__3 : rule__Track__Group__3__Impl rule__Track__Group__4 ;
    public final void rule__Track__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1114:1: ( rule__Track__Group__3__Impl rule__Track__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1115:2: rule__Track__Group__3__Impl rule__Track__Group__4
            {
            pushFollow(FOLLOW_rule__Track__Group__3__Impl_in_rule__Track__Group__32201);
            rule__Track__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__4_in_rule__Track__Group__32204);
            rule__Track__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__3"


    // $ANTLR start "rule__Track__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1122:1: rule__Track__Group__3__Impl : ( ( rule__Track__EspacioAssignment_3 ) ) ;
    public final void rule__Track__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1126:1: ( ( ( rule__Track__EspacioAssignment_3 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1127:1: ( ( rule__Track__EspacioAssignment_3 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1127:1: ( ( rule__Track__EspacioAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1128:1: ( rule__Track__EspacioAssignment_3 )
            {
             before(grammarAccess.getTrackAccess().getEspacioAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1129:1: ( rule__Track__EspacioAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1129:2: rule__Track__EspacioAssignment_3
            {
            pushFollow(FOLLOW_rule__Track__EspacioAssignment_3_in_rule__Track__Group__3__Impl2231);
            rule__Track__EspacioAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getTrackAccess().getEspacioAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__3__Impl"


    // $ANTLR start "rule__Track__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1139:1: rule__Track__Group__4 : rule__Track__Group__4__Impl rule__Track__Group__5 ;
    public final void rule__Track__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1143:1: ( rule__Track__Group__4__Impl rule__Track__Group__5 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1144:2: rule__Track__Group__4__Impl rule__Track__Group__5
            {
            pushFollow(FOLLOW_rule__Track__Group__4__Impl_in_rule__Track__Group__42261);
            rule__Track__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__5_in_rule__Track__Group__42264);
            rule__Track__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__4"


    // $ANTLR start "rule__Track__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1151:1: rule__Track__Group__4__Impl : ( '{' ) ;
    public final void rule__Track__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1155:1: ( ( '{' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1156:1: ( '{' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1156:1: ( '{' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1157:1: '{'
            {
             before(grammarAccess.getTrackAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,12,FOLLOW_12_in_rule__Track__Group__4__Impl2292); 
             after(grammarAccess.getTrackAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__4__Impl"


    // $ANTLR start "rule__Track__Group__5"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1170:1: rule__Track__Group__5 : rule__Track__Group__5__Impl rule__Track__Group__6 ;
    public final void rule__Track__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1174:1: ( rule__Track__Group__5__Impl rule__Track__Group__6 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1175:2: rule__Track__Group__5__Impl rule__Track__Group__6
            {
            pushFollow(FOLLOW_rule__Track__Group__5__Impl_in_rule__Track__Group__52323);
            rule__Track__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Track__Group__6_in_rule__Track__Group__52326);
            rule__Track__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__5"


    // $ANTLR start "rule__Track__Group__5__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1182:1: rule__Track__Group__5__Impl : ( ( ( rule__Track__ActividadesAssignment_5 ) ) ( ( rule__Track__ActividadesAssignment_5 )* ) ) ;
    public final void rule__Track__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1186:1: ( ( ( ( rule__Track__ActividadesAssignment_5 ) ) ( ( rule__Track__ActividadesAssignment_5 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1187:1: ( ( ( rule__Track__ActividadesAssignment_5 ) ) ( ( rule__Track__ActividadesAssignment_5 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1187:1: ( ( ( rule__Track__ActividadesAssignment_5 ) ) ( ( rule__Track__ActividadesAssignment_5 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1188:1: ( ( rule__Track__ActividadesAssignment_5 ) ) ( ( rule__Track__ActividadesAssignment_5 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1188:1: ( ( rule__Track__ActividadesAssignment_5 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1189:1: ( rule__Track__ActividadesAssignment_5 )
            {
             before(grammarAccess.getTrackAccess().getActividadesAssignment_5()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1190:1: ( rule__Track__ActividadesAssignment_5 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1190:2: rule__Track__ActividadesAssignment_5
            {
            pushFollow(FOLLOW_rule__Track__ActividadesAssignment_5_in_rule__Track__Group__5__Impl2355);
            rule__Track__ActividadesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getTrackAccess().getActividadesAssignment_5()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1193:1: ( ( rule__Track__ActividadesAssignment_5 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1194:1: ( rule__Track__ActividadesAssignment_5 )*
            {
             before(grammarAccess.getTrackAccess().getActividadesAssignment_5()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1195:1: ( rule__Track__ActividadesAssignment_5 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==22) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1195:2: rule__Track__ActividadesAssignment_5
            	    {
            	    pushFollow(FOLLOW_rule__Track__ActividadesAssignment_5_in_rule__Track__Group__5__Impl2367);
            	    rule__Track__ActividadesAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getTrackAccess().getActividadesAssignment_5()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__5__Impl"


    // $ANTLR start "rule__Track__Group__6"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1206:1: rule__Track__Group__6 : rule__Track__Group__6__Impl ;
    public final void rule__Track__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1210:1: ( rule__Track__Group__6__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1211:2: rule__Track__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Track__Group__6__Impl_in_rule__Track__Group__62400);
            rule__Track__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__6"


    // $ANTLR start "rule__Track__Group__6__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1217:1: rule__Track__Group__6__Impl : ( '}' ) ;
    public final void rule__Track__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1221:1: ( ( '}' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1222:1: ( '}' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1222:1: ( '}' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1223:1: '}'
            {
             before(grammarAccess.getTrackAccess().getRightCurlyBracketKeyword_6()); 
            match(input,16,FOLLOW_16_in_rule__Track__Group__6__Impl2428); 
             after(grammarAccess.getTrackAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__Group__6__Impl"


    // $ANTLR start "rule__Actividad__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1250:1: rule__Actividad__Group__0 : rule__Actividad__Group__0__Impl rule__Actividad__Group__1 ;
    public final void rule__Actividad__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1254:1: ( rule__Actividad__Group__0__Impl rule__Actividad__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1255:2: rule__Actividad__Group__0__Impl rule__Actividad__Group__1
            {
            pushFollow(FOLLOW_rule__Actividad__Group__0__Impl_in_rule__Actividad__Group__02473);
            rule__Actividad__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__1_in_rule__Actividad__Group__02476);
            rule__Actividad__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__0"


    // $ANTLR start "rule__Actividad__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1262:1: rule__Actividad__Group__0__Impl : ( '[' ) ;
    public final void rule__Actividad__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1266:1: ( ( '[' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1267:1: ( '[' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1267:1: ( '[' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1268:1: '['
            {
             before(grammarAccess.getActividadAccess().getLeftSquareBracketKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__Actividad__Group__0__Impl2504); 
             after(grammarAccess.getActividadAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__0__Impl"


    // $ANTLR start "rule__Actividad__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1281:1: rule__Actividad__Group__1 : rule__Actividad__Group__1__Impl rule__Actividad__Group__2 ;
    public final void rule__Actividad__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1285:1: ( rule__Actividad__Group__1__Impl rule__Actividad__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1286:2: rule__Actividad__Group__1__Impl rule__Actividad__Group__2
            {
            pushFollow(FOLLOW_rule__Actividad__Group__1__Impl_in_rule__Actividad__Group__12535);
            rule__Actividad__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__2_in_rule__Actividad__Group__12538);
            rule__Actividad__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__1"


    // $ANTLR start "rule__Actividad__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1293:1: rule__Actividad__Group__1__Impl : ( ( rule__Actividad__ComienzoAssignment_1 ) ) ;
    public final void rule__Actividad__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1297:1: ( ( ( rule__Actividad__ComienzoAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1298:1: ( ( rule__Actividad__ComienzoAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1298:1: ( ( rule__Actividad__ComienzoAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1299:1: ( rule__Actividad__ComienzoAssignment_1 )
            {
             before(grammarAccess.getActividadAccess().getComienzoAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1300:1: ( rule__Actividad__ComienzoAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1300:2: rule__Actividad__ComienzoAssignment_1
            {
            pushFollow(FOLLOW_rule__Actividad__ComienzoAssignment_1_in_rule__Actividad__Group__1__Impl2565);
            rule__Actividad__ComienzoAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getComienzoAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__1__Impl"


    // $ANTLR start "rule__Actividad__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1310:1: rule__Actividad__Group__2 : rule__Actividad__Group__2__Impl rule__Actividad__Group__3 ;
    public final void rule__Actividad__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1314:1: ( rule__Actividad__Group__2__Impl rule__Actividad__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1315:2: rule__Actividad__Group__2__Impl rule__Actividad__Group__3
            {
            pushFollow(FOLLOW_rule__Actividad__Group__2__Impl_in_rule__Actividad__Group__22595);
            rule__Actividad__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__3_in_rule__Actividad__Group__22598);
            rule__Actividad__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__2"


    // $ANTLR start "rule__Actividad__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1322:1: rule__Actividad__Group__2__Impl : ( '-' ) ;
    public final void rule__Actividad__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1326:1: ( ( '-' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1327:1: ( '-' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1327:1: ( '-' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1328:1: '-'
            {
             before(grammarAccess.getActividadAccess().getHyphenMinusKeyword_2()); 
            match(input,23,FOLLOW_23_in_rule__Actividad__Group__2__Impl2626); 
             after(grammarAccess.getActividadAccess().getHyphenMinusKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__2__Impl"


    // $ANTLR start "rule__Actividad__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1341:1: rule__Actividad__Group__3 : rule__Actividad__Group__3__Impl rule__Actividad__Group__4 ;
    public final void rule__Actividad__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1345:1: ( rule__Actividad__Group__3__Impl rule__Actividad__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1346:2: rule__Actividad__Group__3__Impl rule__Actividad__Group__4
            {
            pushFollow(FOLLOW_rule__Actividad__Group__3__Impl_in_rule__Actividad__Group__32657);
            rule__Actividad__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__4_in_rule__Actividad__Group__32660);
            rule__Actividad__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__3"


    // $ANTLR start "rule__Actividad__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1353:1: rule__Actividad__Group__3__Impl : ( ( rule__Actividad__FinAssignment_3 ) ) ;
    public final void rule__Actividad__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1357:1: ( ( ( rule__Actividad__FinAssignment_3 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1358:1: ( ( rule__Actividad__FinAssignment_3 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1358:1: ( ( rule__Actividad__FinAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1359:1: ( rule__Actividad__FinAssignment_3 )
            {
             before(grammarAccess.getActividadAccess().getFinAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1360:1: ( rule__Actividad__FinAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1360:2: rule__Actividad__FinAssignment_3
            {
            pushFollow(FOLLOW_rule__Actividad__FinAssignment_3_in_rule__Actividad__Group__3__Impl2687);
            rule__Actividad__FinAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getFinAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__3__Impl"


    // $ANTLR start "rule__Actividad__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1370:1: rule__Actividad__Group__4 : rule__Actividad__Group__4__Impl rule__Actividad__Group__5 ;
    public final void rule__Actividad__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1374:1: ( rule__Actividad__Group__4__Impl rule__Actividad__Group__5 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1375:2: rule__Actividad__Group__4__Impl rule__Actividad__Group__5
            {
            pushFollow(FOLLOW_rule__Actividad__Group__4__Impl_in_rule__Actividad__Group__42717);
            rule__Actividad__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__5_in_rule__Actividad__Group__42720);
            rule__Actividad__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__4"


    // $ANTLR start "rule__Actividad__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1382:1: rule__Actividad__Group__4__Impl : ( ']' ) ;
    public final void rule__Actividad__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1386:1: ( ( ']' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1387:1: ( ']' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1387:1: ( ']' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1388:1: ']'
            {
             before(grammarAccess.getActividadAccess().getRightSquareBracketKeyword_4()); 
            match(input,24,FOLLOW_24_in_rule__Actividad__Group__4__Impl2748); 
             after(grammarAccess.getActividadAccess().getRightSquareBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__4__Impl"


    // $ANTLR start "rule__Actividad__Group__5"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1401:1: rule__Actividad__Group__5 : rule__Actividad__Group__5__Impl rule__Actividad__Group__6 ;
    public final void rule__Actividad__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1405:1: ( rule__Actividad__Group__5__Impl rule__Actividad__Group__6 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1406:2: rule__Actividad__Group__5__Impl rule__Actividad__Group__6
            {
            pushFollow(FOLLOW_rule__Actividad__Group__5__Impl_in_rule__Actividad__Group__52779);
            rule__Actividad__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__6_in_rule__Actividad__Group__52782);
            rule__Actividad__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__5"


    // $ANTLR start "rule__Actividad__Group__5__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1413:1: rule__Actividad__Group__5__Impl : ( ( rule__Actividad__Alternatives_5 ) ) ;
    public final void rule__Actividad__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1417:1: ( ( ( rule__Actividad__Alternatives_5 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1418:1: ( ( rule__Actividad__Alternatives_5 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1418:1: ( ( rule__Actividad__Alternatives_5 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1419:1: ( rule__Actividad__Alternatives_5 )
            {
             before(grammarAccess.getActividadAccess().getAlternatives_5()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1420:1: ( rule__Actividad__Alternatives_5 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1420:2: rule__Actividad__Alternatives_5
            {
            pushFollow(FOLLOW_rule__Actividad__Alternatives_5_in_rule__Actividad__Group__5__Impl2809);
            rule__Actividad__Alternatives_5();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getAlternatives_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__5__Impl"


    // $ANTLR start "rule__Actividad__Group__6"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1430:1: rule__Actividad__Group__6 : rule__Actividad__Group__6__Impl rule__Actividad__Group__7 ;
    public final void rule__Actividad__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1434:1: ( rule__Actividad__Group__6__Impl rule__Actividad__Group__7 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1435:2: rule__Actividad__Group__6__Impl rule__Actividad__Group__7
            {
            pushFollow(FOLLOW_rule__Actividad__Group__6__Impl_in_rule__Actividad__Group__62839);
            rule__Actividad__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__7_in_rule__Actividad__Group__62842);
            rule__Actividad__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__6"


    // $ANTLR start "rule__Actividad__Group__6__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1442:1: rule__Actividad__Group__6__Impl : ( ( rule__Actividad__TituloAssignment_6 ) ) ;
    public final void rule__Actividad__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1446:1: ( ( ( rule__Actividad__TituloAssignment_6 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1447:1: ( ( rule__Actividad__TituloAssignment_6 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1447:1: ( ( rule__Actividad__TituloAssignment_6 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1448:1: ( rule__Actividad__TituloAssignment_6 )
            {
             before(grammarAccess.getActividadAccess().getTituloAssignment_6()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1449:1: ( rule__Actividad__TituloAssignment_6 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1449:2: rule__Actividad__TituloAssignment_6
            {
            pushFollow(FOLLOW_rule__Actividad__TituloAssignment_6_in_rule__Actividad__Group__6__Impl2869);
            rule__Actividad__TituloAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getTituloAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__6__Impl"


    // $ANTLR start "rule__Actividad__Group__7"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1459:1: rule__Actividad__Group__7 : rule__Actividad__Group__7__Impl rule__Actividad__Group__8 ;
    public final void rule__Actividad__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1463:1: ( rule__Actividad__Group__7__Impl rule__Actividad__Group__8 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1464:2: rule__Actividad__Group__7__Impl rule__Actividad__Group__8
            {
            pushFollow(FOLLOW_rule__Actividad__Group__7__Impl_in_rule__Actividad__Group__72899);
            rule__Actividad__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__8_in_rule__Actividad__Group__72902);
            rule__Actividad__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__7"


    // $ANTLR start "rule__Actividad__Group__7__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1471:1: rule__Actividad__Group__7__Impl : ( 'oradores' ) ;
    public final void rule__Actividad__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1475:1: ( ( 'oradores' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1476:1: ( 'oradores' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1476:1: ( 'oradores' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1477:1: 'oradores'
            {
             before(grammarAccess.getActividadAccess().getOradoresKeyword_7()); 
            match(input,15,FOLLOW_15_in_rule__Actividad__Group__7__Impl2930); 
             after(grammarAccess.getActividadAccess().getOradoresKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__7__Impl"


    // $ANTLR start "rule__Actividad__Group__8"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1490:1: rule__Actividad__Group__8 : rule__Actividad__Group__8__Impl rule__Actividad__Group__9 ;
    public final void rule__Actividad__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1494:1: ( rule__Actividad__Group__8__Impl rule__Actividad__Group__9 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1495:2: rule__Actividad__Group__8__Impl rule__Actividad__Group__9
            {
            pushFollow(FOLLOW_rule__Actividad__Group__8__Impl_in_rule__Actividad__Group__82961);
            rule__Actividad__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__9_in_rule__Actividad__Group__82964);
            rule__Actividad__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__8"


    // $ANTLR start "rule__Actividad__Group__8__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1502:1: rule__Actividad__Group__8__Impl : ( ( ( rule__Actividad__OradoresAssignment_8 ) ) ( ( rule__Actividad__OradoresAssignment_8 )* ) ) ;
    public final void rule__Actividad__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1506:1: ( ( ( ( rule__Actividad__OradoresAssignment_8 ) ) ( ( rule__Actividad__OradoresAssignment_8 )* ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1507:1: ( ( ( rule__Actividad__OradoresAssignment_8 ) ) ( ( rule__Actividad__OradoresAssignment_8 )* ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1507:1: ( ( ( rule__Actividad__OradoresAssignment_8 ) ) ( ( rule__Actividad__OradoresAssignment_8 )* ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1508:1: ( ( rule__Actividad__OradoresAssignment_8 ) ) ( ( rule__Actividad__OradoresAssignment_8 )* )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1508:1: ( ( rule__Actividad__OradoresAssignment_8 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1509:1: ( rule__Actividad__OradoresAssignment_8 )
            {
             before(grammarAccess.getActividadAccess().getOradoresAssignment_8()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1510:1: ( rule__Actividad__OradoresAssignment_8 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1510:2: rule__Actividad__OradoresAssignment_8
            {
            pushFollow(FOLLOW_rule__Actividad__OradoresAssignment_8_in_rule__Actividad__Group__8__Impl2993);
            rule__Actividad__OradoresAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getOradoresAssignment_8()); 

            }

            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1513:1: ( ( rule__Actividad__OradoresAssignment_8 )* )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1514:1: ( rule__Actividad__OradoresAssignment_8 )*
            {
             before(grammarAccess.getActividadAccess().getOradoresAssignment_8()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1515:1: ( rule__Actividad__OradoresAssignment_8 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1515:2: rule__Actividad__OradoresAssignment_8
            	    {
            	    pushFollow(FOLLOW_rule__Actividad__OradoresAssignment_8_in_rule__Actividad__Group__8__Impl3005);
            	    rule__Actividad__OradoresAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getActividadAccess().getOradoresAssignment_8()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__8__Impl"


    // $ANTLR start "rule__Actividad__Group__9"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1526:1: rule__Actividad__Group__9 : rule__Actividad__Group__9__Impl rule__Actividad__Group__10 ;
    public final void rule__Actividad__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1530:1: ( rule__Actividad__Group__9__Impl rule__Actividad__Group__10 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1531:2: rule__Actividad__Group__9__Impl rule__Actividad__Group__10
            {
            pushFollow(FOLLOW_rule__Actividad__Group__9__Impl_in_rule__Actividad__Group__93038);
            rule__Actividad__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__10_in_rule__Actividad__Group__93041);
            rule__Actividad__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__9"


    // $ANTLR start "rule__Actividad__Group__9__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1538:1: rule__Actividad__Group__9__Impl : ( 'se esperan' ) ;
    public final void rule__Actividad__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1542:1: ( ( 'se esperan' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1543:1: ( 'se esperan' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1543:1: ( 'se esperan' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1544:1: 'se esperan'
            {
             before(grammarAccess.getActividadAccess().getSeEsperanKeyword_9()); 
            match(input,25,FOLLOW_25_in_rule__Actividad__Group__9__Impl3069); 
             after(grammarAccess.getActividadAccess().getSeEsperanKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__9__Impl"


    // $ANTLR start "rule__Actividad__Group__10"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1557:1: rule__Actividad__Group__10 : rule__Actividad__Group__10__Impl rule__Actividad__Group__11 ;
    public final void rule__Actividad__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1561:1: ( rule__Actividad__Group__10__Impl rule__Actividad__Group__11 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1562:2: rule__Actividad__Group__10__Impl rule__Actividad__Group__11
            {
            pushFollow(FOLLOW_rule__Actividad__Group__10__Impl_in_rule__Actividad__Group__103100);
            rule__Actividad__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Actividad__Group__11_in_rule__Actividad__Group__103103);
            rule__Actividad__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__10"


    // $ANTLR start "rule__Actividad__Group__10__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1569:1: rule__Actividad__Group__10__Impl : ( ( rule__Actividad__OyentesAssignment_10 ) ) ;
    public final void rule__Actividad__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1573:1: ( ( ( rule__Actividad__OyentesAssignment_10 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1574:1: ( ( rule__Actividad__OyentesAssignment_10 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1574:1: ( ( rule__Actividad__OyentesAssignment_10 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1575:1: ( rule__Actividad__OyentesAssignment_10 )
            {
             before(grammarAccess.getActividadAccess().getOyentesAssignment_10()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1576:1: ( rule__Actividad__OyentesAssignment_10 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1576:2: rule__Actividad__OyentesAssignment_10
            {
            pushFollow(FOLLOW_rule__Actividad__OyentesAssignment_10_in_rule__Actividad__Group__10__Impl3130);
            rule__Actividad__OyentesAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getActividadAccess().getOyentesAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__10__Impl"


    // $ANTLR start "rule__Actividad__Group__11"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1586:1: rule__Actividad__Group__11 : rule__Actividad__Group__11__Impl ;
    public final void rule__Actividad__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1590:1: ( rule__Actividad__Group__11__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1591:2: rule__Actividad__Group__11__Impl
            {
            pushFollow(FOLLOW_rule__Actividad__Group__11__Impl_in_rule__Actividad__Group__113160);
            rule__Actividad__Group__11__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__11"


    // $ANTLR start "rule__Actividad__Group__11__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1597:1: rule__Actividad__Group__11__Impl : ( 'oyentes' ) ;
    public final void rule__Actividad__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1601:1: ( ( 'oyentes' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1602:1: ( 'oyentes' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1602:1: ( 'oyentes' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1603:1: 'oyentes'
            {
             before(grammarAccess.getActividadAccess().getOyentesKeyword_11()); 
            match(input,26,FOLLOW_26_in_rule__Actividad__Group__11__Impl3188); 
             after(grammarAccess.getActividadAccess().getOyentesKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__Group__11__Impl"


    // $ANTLR start "rule__Organizacion__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1640:1: rule__Organizacion__Group__0 : rule__Organizacion__Group__0__Impl rule__Organizacion__Group__1 ;
    public final void rule__Organizacion__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1644:1: ( rule__Organizacion__Group__0__Impl rule__Organizacion__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1645:2: rule__Organizacion__Group__0__Impl rule__Organizacion__Group__1
            {
            pushFollow(FOLLOW_rule__Organizacion__Group__0__Impl_in_rule__Organizacion__Group__03243);
            rule__Organizacion__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Organizacion__Group__1_in_rule__Organizacion__Group__03246);
            rule__Organizacion__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__0"


    // $ANTLR start "rule__Organizacion__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1652:1: rule__Organizacion__Group__0__Impl : ( 'Org' ) ;
    public final void rule__Organizacion__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1656:1: ( ( 'Org' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1657:1: ( 'Org' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1657:1: ( 'Org' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1658:1: 'Org'
            {
             before(grammarAccess.getOrganizacionAccess().getOrgKeyword_0()); 
            match(input,27,FOLLOW_27_in_rule__Organizacion__Group__0__Impl3274); 
             after(grammarAccess.getOrganizacionAccess().getOrgKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__0__Impl"


    // $ANTLR start "rule__Organizacion__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1671:1: rule__Organizacion__Group__1 : rule__Organizacion__Group__1__Impl rule__Organizacion__Group__2 ;
    public final void rule__Organizacion__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1675:1: ( rule__Organizacion__Group__1__Impl rule__Organizacion__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1676:2: rule__Organizacion__Group__1__Impl rule__Organizacion__Group__2
            {
            pushFollow(FOLLOW_rule__Organizacion__Group__1__Impl_in_rule__Organizacion__Group__13305);
            rule__Organizacion__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Organizacion__Group__2_in_rule__Organizacion__Group__13308);
            rule__Organizacion__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__1"


    // $ANTLR start "rule__Organizacion__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1683:1: rule__Organizacion__Group__1__Impl : ( ( rule__Organizacion__NameAssignment_1 ) ) ;
    public final void rule__Organizacion__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1687:1: ( ( ( rule__Organizacion__NameAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1688:1: ( ( rule__Organizacion__NameAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1688:1: ( ( rule__Organizacion__NameAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1689:1: ( rule__Organizacion__NameAssignment_1 )
            {
             before(grammarAccess.getOrganizacionAccess().getNameAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1690:1: ( rule__Organizacion__NameAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1690:2: rule__Organizacion__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Organizacion__NameAssignment_1_in_rule__Organizacion__Group__1__Impl3335);
            rule__Organizacion__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOrganizacionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__1__Impl"


    // $ANTLR start "rule__Organizacion__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1700:1: rule__Organizacion__Group__2 : rule__Organizacion__Group__2__Impl rule__Organizacion__Group__3 ;
    public final void rule__Organizacion__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1704:1: ( rule__Organizacion__Group__2__Impl rule__Organizacion__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1705:2: rule__Organizacion__Group__2__Impl rule__Organizacion__Group__3
            {
            pushFollow(FOLLOW_rule__Organizacion__Group__2__Impl_in_rule__Organizacion__Group__23365);
            rule__Organizacion__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Organizacion__Group__3_in_rule__Organizacion__Group__23368);
            rule__Organizacion__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__2"


    // $ANTLR start "rule__Organizacion__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1712:1: rule__Organizacion__Group__2__Impl : ( ':' ) ;
    public final void rule__Organizacion__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1716:1: ( ( ':' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1717:1: ( ':' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1717:1: ( ':' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1718:1: ':'
            {
             before(grammarAccess.getOrganizacionAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_28_in_rule__Organizacion__Group__2__Impl3396); 
             after(grammarAccess.getOrganizacionAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__2__Impl"


    // $ANTLR start "rule__Organizacion__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1731:1: rule__Organizacion__Group__3 : rule__Organizacion__Group__3__Impl ;
    public final void rule__Organizacion__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1735:1: ( rule__Organizacion__Group__3__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1736:2: rule__Organizacion__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Organizacion__Group__3__Impl_in_rule__Organizacion__Group__33427);
            rule__Organizacion__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__3"


    // $ANTLR start "rule__Organizacion__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1742:1: rule__Organizacion__Group__3__Impl : ( ( rule__Organizacion__RazonSocialAssignment_3 ) ) ;
    public final void rule__Organizacion__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1746:1: ( ( ( rule__Organizacion__RazonSocialAssignment_3 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1747:1: ( ( rule__Organizacion__RazonSocialAssignment_3 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1747:1: ( ( rule__Organizacion__RazonSocialAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1748:1: ( rule__Organizacion__RazonSocialAssignment_3 )
            {
             before(grammarAccess.getOrganizacionAccess().getRazonSocialAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1749:1: ( rule__Organizacion__RazonSocialAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1749:2: rule__Organizacion__RazonSocialAssignment_3
            {
            pushFollow(FOLLOW_rule__Organizacion__RazonSocialAssignment_3_in_rule__Organizacion__Group__3__Impl3454);
            rule__Organizacion__RazonSocialAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getOrganizacionAccess().getRazonSocialAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__Group__3__Impl"


    // $ANTLR start "rule__Orador__Group__0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1767:1: rule__Orador__Group__0 : rule__Orador__Group__0__Impl rule__Orador__Group__1 ;
    public final void rule__Orador__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1771:1: ( rule__Orador__Group__0__Impl rule__Orador__Group__1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1772:2: rule__Orador__Group__0__Impl rule__Orador__Group__1
            {
            pushFollow(FOLLOW_rule__Orador__Group__0__Impl_in_rule__Orador__Group__03492);
            rule__Orador__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Orador__Group__1_in_rule__Orador__Group__03495);
            rule__Orador__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__0"


    // $ANTLR start "rule__Orador__Group__0__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1779:1: rule__Orador__Group__0__Impl : ( 'Orador' ) ;
    public final void rule__Orador__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1783:1: ( ( 'Orador' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1784:1: ( 'Orador' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1784:1: ( 'Orador' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1785:1: 'Orador'
            {
             before(grammarAccess.getOradorAccess().getOradorKeyword_0()); 
            match(input,29,FOLLOW_29_in_rule__Orador__Group__0__Impl3523); 
             after(grammarAccess.getOradorAccess().getOradorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__0__Impl"


    // $ANTLR start "rule__Orador__Group__1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1798:1: rule__Orador__Group__1 : rule__Orador__Group__1__Impl rule__Orador__Group__2 ;
    public final void rule__Orador__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1802:1: ( rule__Orador__Group__1__Impl rule__Orador__Group__2 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1803:2: rule__Orador__Group__1__Impl rule__Orador__Group__2
            {
            pushFollow(FOLLOW_rule__Orador__Group__1__Impl_in_rule__Orador__Group__13554);
            rule__Orador__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Orador__Group__2_in_rule__Orador__Group__13557);
            rule__Orador__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__1"


    // $ANTLR start "rule__Orador__Group__1__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1810:1: rule__Orador__Group__1__Impl : ( ( rule__Orador__NameAssignment_1 ) ) ;
    public final void rule__Orador__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1814:1: ( ( ( rule__Orador__NameAssignment_1 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1815:1: ( ( rule__Orador__NameAssignment_1 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1815:1: ( ( rule__Orador__NameAssignment_1 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1816:1: ( rule__Orador__NameAssignment_1 )
            {
             before(grammarAccess.getOradorAccess().getNameAssignment_1()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1817:1: ( rule__Orador__NameAssignment_1 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1817:2: rule__Orador__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Orador__NameAssignment_1_in_rule__Orador__Group__1__Impl3584);
            rule__Orador__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOradorAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__1__Impl"


    // $ANTLR start "rule__Orador__Group__2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1827:1: rule__Orador__Group__2 : rule__Orador__Group__2__Impl rule__Orador__Group__3 ;
    public final void rule__Orador__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1831:1: ( rule__Orador__Group__2__Impl rule__Orador__Group__3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1832:2: rule__Orador__Group__2__Impl rule__Orador__Group__3
            {
            pushFollow(FOLLOW_rule__Orador__Group__2__Impl_in_rule__Orador__Group__23614);
            rule__Orador__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Orador__Group__3_in_rule__Orador__Group__23617);
            rule__Orador__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__2"


    // $ANTLR start "rule__Orador__Group__2__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1839:1: rule__Orador__Group__2__Impl : ( ':' ) ;
    public final void rule__Orador__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1843:1: ( ( ':' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1844:1: ( ':' )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1844:1: ( ':' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1845:1: ':'
            {
             before(grammarAccess.getOradorAccess().getColonKeyword_2()); 
            match(input,28,FOLLOW_28_in_rule__Orador__Group__2__Impl3645); 
             after(grammarAccess.getOradorAccess().getColonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__2__Impl"


    // $ANTLR start "rule__Orador__Group__3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1858:1: rule__Orador__Group__3 : rule__Orador__Group__3__Impl rule__Orador__Group__4 ;
    public final void rule__Orador__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1862:1: ( rule__Orador__Group__3__Impl rule__Orador__Group__4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1863:2: rule__Orador__Group__3__Impl rule__Orador__Group__4
            {
            pushFollow(FOLLOW_rule__Orador__Group__3__Impl_in_rule__Orador__Group__33676);
            rule__Orador__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Orador__Group__4_in_rule__Orador__Group__33679);
            rule__Orador__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__3"


    // $ANTLR start "rule__Orador__Group__3__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1870:1: rule__Orador__Group__3__Impl : ( ( rule__Orador__NombreCompletoAssignment_3 ) ) ;
    public final void rule__Orador__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1874:1: ( ( ( rule__Orador__NombreCompletoAssignment_3 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1875:1: ( ( rule__Orador__NombreCompletoAssignment_3 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1875:1: ( ( rule__Orador__NombreCompletoAssignment_3 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1876:1: ( rule__Orador__NombreCompletoAssignment_3 )
            {
             before(grammarAccess.getOradorAccess().getNombreCompletoAssignment_3()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1877:1: ( rule__Orador__NombreCompletoAssignment_3 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1877:2: rule__Orador__NombreCompletoAssignment_3
            {
            pushFollow(FOLLOW_rule__Orador__NombreCompletoAssignment_3_in_rule__Orador__Group__3__Impl3706);
            rule__Orador__NombreCompletoAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getOradorAccess().getNombreCompletoAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__3__Impl"


    // $ANTLR start "rule__Orador__Group__4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1887:1: rule__Orador__Group__4 : rule__Orador__Group__4__Impl ;
    public final void rule__Orador__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1891:1: ( rule__Orador__Group__4__Impl )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1892:2: rule__Orador__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Orador__Group__4__Impl_in_rule__Orador__Group__43736);
            rule__Orador__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__4"


    // $ANTLR start "rule__Orador__Group__4__Impl"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1898:1: rule__Orador__Group__4__Impl : ( ( rule__Orador__OrganizacionAssignment_4 ) ) ;
    public final void rule__Orador__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1902:1: ( ( ( rule__Orador__OrganizacionAssignment_4 ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1903:1: ( ( rule__Orador__OrganizacionAssignment_4 ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1903:1: ( ( rule__Orador__OrganizacionAssignment_4 ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1904:1: ( rule__Orador__OrganizacionAssignment_4 )
            {
             before(grammarAccess.getOradorAccess().getOrganizacionAssignment_4()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1905:1: ( rule__Orador__OrganizacionAssignment_4 )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1905:2: rule__Orador__OrganizacionAssignment_4
            {
            pushFollow(FOLLOW_rule__Orador__OrganizacionAssignment_4_in_rule__Orador__Group__4__Impl3763);
            rule__Orador__OrganizacionAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getOradorAccess().getOrganizacionAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__Group__4__Impl"


    // $ANTLR start "rule__EventoModel__TitleAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1926:1: rule__EventoModel__TitleAssignment_1 : ( RULE_STRING ) ;
    public final void rule__EventoModel__TitleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1930:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1931:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1931:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1932:1: RULE_STRING
            {
             before(grammarAccess.getEventoModelAccess().getTitleSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__EventoModel__TitleAssignment_13808); 
             after(grammarAccess.getEventoModelAccess().getTitleSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__TitleAssignment_1"


    // $ANTLR start "rule__EventoModel__EspaciosAssignment_4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1941:1: rule__EventoModel__EspaciosAssignment_4 : ( ruleEspacio ) ;
    public final void rule__EventoModel__EspaciosAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1945:1: ( ( ruleEspacio ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1946:1: ( ruleEspacio )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1946:1: ( ruleEspacio )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1947:1: ruleEspacio
            {
             before(grammarAccess.getEventoModelAccess().getEspaciosEspacioParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleEspacio_in_rule__EventoModel__EspaciosAssignment_43839);
            ruleEspacio();

            state._fsp--;

             after(grammarAccess.getEventoModelAccess().getEspaciosEspacioParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__EspaciosAssignment_4"


    // $ANTLR start "rule__EventoModel__OrganizacionesAssignment_6"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1956:1: rule__EventoModel__OrganizacionesAssignment_6 : ( ruleOrganizacion ) ;
    public final void rule__EventoModel__OrganizacionesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1960:1: ( ( ruleOrganizacion ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1961:1: ( ruleOrganizacion )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1961:1: ( ruleOrganizacion )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1962:1: ruleOrganizacion
            {
             before(grammarAccess.getEventoModelAccess().getOrganizacionesOrganizacionParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleOrganizacion_in_rule__EventoModel__OrganizacionesAssignment_63870);
            ruleOrganizacion();

            state._fsp--;

             after(grammarAccess.getEventoModelAccess().getOrganizacionesOrganizacionParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__OrganizacionesAssignment_6"


    // $ANTLR start "rule__EventoModel__OradoresAssignment_8"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1971:1: rule__EventoModel__OradoresAssignment_8 : ( ruleOrador ) ;
    public final void rule__EventoModel__OradoresAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1975:1: ( ( ruleOrador ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1976:1: ( ruleOrador )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1976:1: ( ruleOrador )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1977:1: ruleOrador
            {
             before(grammarAccess.getEventoModelAccess().getOradoresOradorParserRuleCall_8_0()); 
            pushFollow(FOLLOW_ruleOrador_in_rule__EventoModel__OradoresAssignment_83901);
            ruleOrador();

            state._fsp--;

             after(grammarAccess.getEventoModelAccess().getOradoresOradorParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__OradoresAssignment_8"


    // $ANTLR start "rule__EventoModel__DiasAssignment_9"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1986:1: rule__EventoModel__DiasAssignment_9 : ( ruleDia ) ;
    public final void rule__EventoModel__DiasAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1990:1: ( ( ruleDia ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1991:1: ( ruleDia )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1991:1: ( ruleDia )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:1992:1: ruleDia
            {
             before(grammarAccess.getEventoModelAccess().getDiasDiaParserRuleCall_9_0()); 
            pushFollow(FOLLOW_ruleDia_in_rule__EventoModel__DiasAssignment_93932);
            ruleDia();

            state._fsp--;

             after(grammarAccess.getEventoModelAccess().getDiasDiaParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventoModel__DiasAssignment_9"


    // $ANTLR start "rule__Dia__DiaAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2001:1: rule__Dia__DiaAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Dia__DiaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2005:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2006:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2006:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2007:1: RULE_STRING
            {
             before(grammarAccess.getDiaAccess().getDiaSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Dia__DiaAssignment_13963); 
             after(grammarAccess.getDiaAccess().getDiaSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__DiaAssignment_1"


    // $ANTLR start "rule__Dia__BloquesAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2016:1: rule__Dia__BloquesAssignment_3 : ( ruleTrack ) ;
    public final void rule__Dia__BloquesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2020:1: ( ( ruleTrack ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2021:1: ( ruleTrack )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2021:1: ( ruleTrack )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2022:1: ruleTrack
            {
             before(grammarAccess.getDiaAccess().getBloquesTrackParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleTrack_in_rule__Dia__BloquesAssignment_33994);
            ruleTrack();

            state._fsp--;

             after(grammarAccess.getDiaAccess().getBloquesTrackParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Dia__BloquesAssignment_3"


    // $ANTLR start "rule__Espacio__NameAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2031:1: rule__Espacio__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Espacio__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2035:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2036:1: ( RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2036:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2037:1: RULE_ID
            {
             before(grammarAccess.getEspacioAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Espacio__NameAssignment_14025); 
             after(grammarAccess.getEspacioAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__NameAssignment_1"


    // $ANTLR start "rule__Espacio__CapacidadAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2046:1: rule__Espacio__CapacidadAssignment_3 : ( RULE_INT ) ;
    public final void rule__Espacio__CapacidadAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2050:1: ( ( RULE_INT ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2051:1: ( RULE_INT )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2051:1: ( RULE_INT )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2052:1: RULE_INT
            {
             before(grammarAccess.getEspacioAccess().getCapacidadINTTerminalRuleCall_3_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Espacio__CapacidadAssignment_34056); 
             after(grammarAccess.getEspacioAccess().getCapacidadINTTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__CapacidadAssignment_3"


    // $ANTLR start "rule__Espacio__LaboratorioAssignment_4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2061:1: rule__Espacio__LaboratorioAssignment_4 : ( ( 'con computadoras' ) ) ;
    public final void rule__Espacio__LaboratorioAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2065:1: ( ( ( 'con computadoras' ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2066:1: ( ( 'con computadoras' ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2066:1: ( ( 'con computadoras' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2067:1: ( 'con computadoras' )
            {
             before(grammarAccess.getEspacioAccess().getLaboratorioConComputadorasKeyword_4_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2068:1: ( 'con computadoras' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2069:1: 'con computadoras'
            {
             before(grammarAccess.getEspacioAccess().getLaboratorioConComputadorasKeyword_4_0()); 
            match(input,30,FOLLOW_30_in_rule__Espacio__LaboratorioAssignment_44092); 
             after(grammarAccess.getEspacioAccess().getLaboratorioConComputadorasKeyword_4_0()); 

            }

             after(grammarAccess.getEspacioAccess().getLaboratorioConComputadorasKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Espacio__LaboratorioAssignment_4"


    // $ANTLR start "rule__Track__TitleAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2084:1: rule__Track__TitleAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Track__TitleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2088:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2089:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2089:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2090:1: RULE_STRING
            {
             before(grammarAccess.getTrackAccess().getTitleSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Track__TitleAssignment_14131); 
             after(grammarAccess.getTrackAccess().getTitleSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__TitleAssignment_1"


    // $ANTLR start "rule__Track__EspacioAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2099:1: rule__Track__EspacioAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Track__EspacioAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2103:1: ( ( ( RULE_ID ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2104:1: ( ( RULE_ID ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2104:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2105:1: ( RULE_ID )
            {
             before(grammarAccess.getTrackAccess().getEspacioEspacioCrossReference_3_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2106:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2107:1: RULE_ID
            {
             before(grammarAccess.getTrackAccess().getEspacioEspacioIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Track__EspacioAssignment_34166); 
             after(grammarAccess.getTrackAccess().getEspacioEspacioIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getTrackAccess().getEspacioEspacioCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__EspacioAssignment_3"


    // $ANTLR start "rule__Track__ActividadesAssignment_5"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2118:1: rule__Track__ActividadesAssignment_5 : ( ruleActividad ) ;
    public final void rule__Track__ActividadesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2122:1: ( ( ruleActividad ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2123:1: ( ruleActividad )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2123:1: ( ruleActividad )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2124:1: ruleActividad
            {
             before(grammarAccess.getTrackAccess().getActividadesActividadParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleActividad_in_rule__Track__ActividadesAssignment_54201);
            ruleActividad();

            state._fsp--;

             after(grammarAccess.getTrackAccess().getActividadesActividadParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Track__ActividadesAssignment_5"


    // $ANTLR start "rule__Actividad__ComienzoAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2133:1: rule__Actividad__ComienzoAssignment_1 : ( ruleHOUR ) ;
    public final void rule__Actividad__ComienzoAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2137:1: ( ( ruleHOUR ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2138:1: ( ruleHOUR )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2138:1: ( ruleHOUR )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2139:1: ruleHOUR
            {
             before(grammarAccess.getActividadAccess().getComienzoHOURParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleHOUR_in_rule__Actividad__ComienzoAssignment_14232);
            ruleHOUR();

            state._fsp--;

             after(grammarAccess.getActividadAccess().getComienzoHOURParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__ComienzoAssignment_1"


    // $ANTLR start "rule__Actividad__FinAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2148:1: rule__Actividad__FinAssignment_3 : ( ruleHOUR ) ;
    public final void rule__Actividad__FinAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2152:1: ( ( ruleHOUR ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2153:1: ( ruleHOUR )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2153:1: ( ruleHOUR )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2154:1: ruleHOUR
            {
             before(grammarAccess.getActividadAccess().getFinHOURParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleHOUR_in_rule__Actividad__FinAssignment_34263);
            ruleHOUR();

            state._fsp--;

             after(grammarAccess.getActividadAccess().getFinHOURParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__FinAssignment_3"


    // $ANTLR start "rule__Actividad__MesaAssignment_5_0"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2163:1: rule__Actividad__MesaAssignment_5_0 : ( ( 'mesa' ) ) ;
    public final void rule__Actividad__MesaAssignment_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2167:1: ( ( ( 'mesa' ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2168:1: ( ( 'mesa' ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2168:1: ( ( 'mesa' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2169:1: ( 'mesa' )
            {
             before(grammarAccess.getActividadAccess().getMesaMesaKeyword_5_0_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2170:1: ( 'mesa' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2171:1: 'mesa'
            {
             before(grammarAccess.getActividadAccess().getMesaMesaKeyword_5_0_0()); 
            match(input,31,FOLLOW_31_in_rule__Actividad__MesaAssignment_5_04299); 
             after(grammarAccess.getActividadAccess().getMesaMesaKeyword_5_0_0()); 

            }

             after(grammarAccess.getActividadAccess().getMesaMesaKeyword_5_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__MesaAssignment_5_0"


    // $ANTLR start "rule__Actividad__CharlaAssignment_5_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2186:1: rule__Actividad__CharlaAssignment_5_1 : ( ( 'charla' ) ) ;
    public final void rule__Actividad__CharlaAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2190:1: ( ( ( 'charla' ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2191:1: ( ( 'charla' ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2191:1: ( ( 'charla' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2192:1: ( 'charla' )
            {
             before(grammarAccess.getActividadAccess().getCharlaCharlaKeyword_5_1_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2193:1: ( 'charla' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2194:1: 'charla'
            {
             before(grammarAccess.getActividadAccess().getCharlaCharlaKeyword_5_1_0()); 
            match(input,32,FOLLOW_32_in_rule__Actividad__CharlaAssignment_5_14343); 
             after(grammarAccess.getActividadAccess().getCharlaCharlaKeyword_5_1_0()); 

            }

             after(grammarAccess.getActividadAccess().getCharlaCharlaKeyword_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__CharlaAssignment_5_1"


    // $ANTLR start "rule__Actividad__TallerAssignment_5_2"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2209:1: rule__Actividad__TallerAssignment_5_2 : ( ( 'taller' ) ) ;
    public final void rule__Actividad__TallerAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2213:1: ( ( ( 'taller' ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2214:1: ( ( 'taller' ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2214:1: ( ( 'taller' ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2215:1: ( 'taller' )
            {
             before(grammarAccess.getActividadAccess().getTallerTallerKeyword_5_2_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2216:1: ( 'taller' )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2217:1: 'taller'
            {
             before(grammarAccess.getActividadAccess().getTallerTallerKeyword_5_2_0()); 
            match(input,33,FOLLOW_33_in_rule__Actividad__TallerAssignment_5_24387); 
             after(grammarAccess.getActividadAccess().getTallerTallerKeyword_5_2_0()); 

            }

             after(grammarAccess.getActividadAccess().getTallerTallerKeyword_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__TallerAssignment_5_2"


    // $ANTLR start "rule__Actividad__TituloAssignment_6"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2232:1: rule__Actividad__TituloAssignment_6 : ( RULE_STRING ) ;
    public final void rule__Actividad__TituloAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2236:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2237:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2237:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2238:1: RULE_STRING
            {
             before(grammarAccess.getActividadAccess().getTituloSTRINGTerminalRuleCall_6_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Actividad__TituloAssignment_64426); 
             after(grammarAccess.getActividadAccess().getTituloSTRINGTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__TituloAssignment_6"


    // $ANTLR start "rule__Actividad__OradoresAssignment_8"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2247:1: rule__Actividad__OradoresAssignment_8 : ( ( RULE_ID ) ) ;
    public final void rule__Actividad__OradoresAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2251:1: ( ( ( RULE_ID ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2252:1: ( ( RULE_ID ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2252:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2253:1: ( RULE_ID )
            {
             before(grammarAccess.getActividadAccess().getOradoresOradorCrossReference_8_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2254:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2255:1: RULE_ID
            {
             before(grammarAccess.getActividadAccess().getOradoresOradorIDTerminalRuleCall_8_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Actividad__OradoresAssignment_84461); 
             after(grammarAccess.getActividadAccess().getOradoresOradorIDTerminalRuleCall_8_0_1()); 

            }

             after(grammarAccess.getActividadAccess().getOradoresOradorCrossReference_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__OradoresAssignment_8"


    // $ANTLR start "rule__Actividad__OyentesAssignment_10"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2266:1: rule__Actividad__OyentesAssignment_10 : ( RULE_INT ) ;
    public final void rule__Actividad__OyentesAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2270:1: ( ( RULE_INT ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2271:1: ( RULE_INT )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2271:1: ( RULE_INT )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2272:1: RULE_INT
            {
             before(grammarAccess.getActividadAccess().getOyentesINTTerminalRuleCall_10_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Actividad__OyentesAssignment_104496); 
             after(grammarAccess.getActividadAccess().getOyentesINTTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Actividad__OyentesAssignment_10"


    // $ANTLR start "rule__Organizacion__NameAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2281:1: rule__Organizacion__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Organizacion__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2285:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2286:1: ( RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2286:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2287:1: RULE_ID
            {
             before(grammarAccess.getOrganizacionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Organizacion__NameAssignment_14527); 
             after(grammarAccess.getOrganizacionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__NameAssignment_1"


    // $ANTLR start "rule__Organizacion__RazonSocialAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2296:1: rule__Organizacion__RazonSocialAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Organizacion__RazonSocialAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2300:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2301:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2301:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2302:1: RULE_STRING
            {
             before(grammarAccess.getOrganizacionAccess().getRazonSocialSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Organizacion__RazonSocialAssignment_34558); 
             after(grammarAccess.getOrganizacionAccess().getRazonSocialSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Organizacion__RazonSocialAssignment_3"


    // $ANTLR start "rule__Orador__NameAssignment_1"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2311:1: rule__Orador__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Orador__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2315:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2316:1: ( RULE_ID )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2316:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2317:1: RULE_ID
            {
             before(grammarAccess.getOradorAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Orador__NameAssignment_14589); 
             after(grammarAccess.getOradorAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__NameAssignment_1"


    // $ANTLR start "rule__Orador__NombreCompletoAssignment_3"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2326:1: rule__Orador__NombreCompletoAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Orador__NombreCompletoAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2330:1: ( ( RULE_STRING ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2331:1: ( RULE_STRING )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2331:1: ( RULE_STRING )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2332:1: RULE_STRING
            {
             before(grammarAccess.getOradorAccess().getNombreCompletoSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Orador__NombreCompletoAssignment_34620); 
             after(grammarAccess.getOradorAccess().getNombreCompletoSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__NombreCompletoAssignment_3"


    // $ANTLR start "rule__Orador__OrganizacionAssignment_4"
    // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2341:1: rule__Orador__OrganizacionAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Orador__OrganizacionAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2345:1: ( ( ( RULE_ID ) ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2346:1: ( ( RULE_ID ) )
            {
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2346:1: ( ( RULE_ID ) )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2347:1: ( RULE_ID )
            {
             before(grammarAccess.getOradorAccess().getOrganizacionOrganizacionCrossReference_4_0()); 
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2348:1: ( RULE_ID )
            // ../ar.edu.unq.obj3.dsl.planificacion.ui/src-gen/ar/edu/unq/obj3/dsl/planificacion/ui/contentassist/antlr/internal/InternalPlanificacion.g:2349:1: RULE_ID
            {
             before(grammarAccess.getOradorAccess().getOrganizacionOrganizacionIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Orador__OrganizacionAssignment_44655); 
             after(grammarAccess.getOradorAccess().getOrganizacionOrganizacionIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getOradorAccess().getOrganizacionOrganizacionCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Orador__OrganizacionAssignment_4"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleEventoModel_in_entryRuleEventoModel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEventoModel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__0_in_ruleEventoModel94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDia_in_entryRuleDia121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDia128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__Group__0_in_ruleDia154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEspacio_in_entryRuleEspacio181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEspacio188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__0_in_ruleEspacio214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTrack_in_entryRuleTrack241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTrack248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__0_in_ruleTrack274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActividad_in_entryRuleActividad301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleActividad308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__0_in_ruleActividad334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrganizacion_in_entryRuleOrganizacion361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrganizacion368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__0_in_ruleOrganizacion394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrador_in_entryRuleOrador421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrador428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__0_in_ruleOrador454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHOUR_in_entryRuleHOUR481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleHOUR488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleHOUR514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__MesaAssignment_5_0_in_rule__Actividad__Alternatives_5549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__CharlaAssignment_5_1_in_rule__Actividad__Alternatives_5567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__TallerAssignment_5_2_in_rule__Actividad__Alternatives_5585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__0__Impl_in_rule__EventoModel__Group__0616 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__1_in_rule__EventoModel__Group__0619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__EventoModel__Group__0__Impl647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__1__Impl_in_rule__EventoModel__Group__1678 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__2_in_rule__EventoModel__Group__1681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__TitleAssignment_1_in_rule__EventoModel__Group__1__Impl708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__2__Impl_in_rule__EventoModel__Group__2738 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__3_in_rule__EventoModel__Group__2741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__EventoModel__Group__2__Impl769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__3__Impl_in_rule__EventoModel__Group__3800 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__4_in_rule__EventoModel__Group__3803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__EventoModel__Group__3__Impl831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__4__Impl_in_rule__EventoModel__Group__4862 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__5_in_rule__EventoModel__Group__4865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__EspaciosAssignment_4_in_rule__EventoModel__Group__4__Impl894 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_rule__EventoModel__EspaciosAssignment_4_in_rule__EventoModel__Group__4__Impl906 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__5__Impl_in_rule__EventoModel__Group__5939 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__6_in_rule__EventoModel__Group__5942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__EventoModel__Group__5__Impl970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__6__Impl_in_rule__EventoModel__Group__61001 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__7_in_rule__EventoModel__Group__61004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__OrganizacionesAssignment_6_in_rule__EventoModel__Group__6__Impl1033 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_rule__EventoModel__OrganizacionesAssignment_6_in_rule__EventoModel__Group__6__Impl1045 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__7__Impl_in_rule__EventoModel__Group__71078 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__8_in_rule__EventoModel__Group__71081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__EventoModel__Group__7__Impl1109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__8__Impl_in_rule__EventoModel__Group__81140 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__9_in_rule__EventoModel__Group__81143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__OradoresAssignment_8_in_rule__EventoModel__Group__8__Impl1172 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_rule__EventoModel__OradoresAssignment_8_in_rule__EventoModel__Group__8__Impl1184 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__9__Impl_in_rule__EventoModel__Group__91217 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__10_in_rule__EventoModel__Group__91220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EventoModel__DiasAssignment_9_in_rule__EventoModel__Group__9__Impl1249 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__EventoModel__DiasAssignment_9_in_rule__EventoModel__Group__9__Impl1261 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__EventoModel__Group__10__Impl_in_rule__EventoModel__Group__101294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__EventoModel__Group__10__Impl1322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__Group__0__Impl_in_rule__Dia__Group__01375 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Dia__Group__1_in_rule__Dia__Group__01378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Dia__Group__0__Impl1406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__Group__1__Impl_in_rule__Dia__Group__11437 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Dia__Group__2_in_rule__Dia__Group__11440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__DiaAssignment_1_in_rule__Dia__Group__1__Impl1467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__Group__2__Impl_in_rule__Dia__Group__21497 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Dia__Group__3_in_rule__Dia__Group__21500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Dia__Group__2__Impl1528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__Group__3__Impl_in_rule__Dia__Group__31559 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Dia__Group__4_in_rule__Dia__Group__31562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Dia__BloquesAssignment_3_in_rule__Dia__Group__3__Impl1591 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_rule__Dia__BloquesAssignment_3_in_rule__Dia__Group__3__Impl1603 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_rule__Dia__Group__4__Impl_in_rule__Dia__Group__41636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Dia__Group__4__Impl1664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__0__Impl_in_rule__Espacio__Group__01705 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Espacio__Group__1_in_rule__Espacio__Group__01708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Espacio__Group__0__Impl1736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__1__Impl_in_rule__Espacio__Group__11767 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__Espacio__Group__2_in_rule__Espacio__Group__11770 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__NameAssignment_1_in_rule__Espacio__Group__1__Impl1797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__2__Impl_in_rule__Espacio__Group__21827 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Espacio__Group__3_in_rule__Espacio__Group__21830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Espacio__Group__2__Impl1858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__3__Impl_in_rule__Espacio__Group__31889 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_rule__Espacio__Group__4_in_rule__Espacio__Group__31892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__CapacidadAssignment_3_in_rule__Espacio__Group__3__Impl1919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__Group__4__Impl_in_rule__Espacio__Group__41949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Espacio__LaboratorioAssignment_4_in_rule__Espacio__Group__4__Impl1976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__0__Impl_in_rule__Track__Group__02017 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Track__Group__1_in_rule__Track__Group__02020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Track__Group__0__Impl2048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__1__Impl_in_rule__Track__Group__12079 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Track__Group__2_in_rule__Track__Group__12082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__TitleAssignment_1_in_rule__Track__Group__1__Impl2109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__2__Impl_in_rule__Track__Group__22139 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Track__Group__3_in_rule__Track__Group__22142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Track__Group__2__Impl2170 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__3__Impl_in_rule__Track__Group__32201 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Track__Group__4_in_rule__Track__Group__32204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__EspacioAssignment_3_in_rule__Track__Group__3__Impl2231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__4__Impl_in_rule__Track__Group__42261 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Track__Group__5_in_rule__Track__Group__42264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Track__Group__4__Impl2292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__Group__5__Impl_in_rule__Track__Group__52323 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Track__Group__6_in_rule__Track__Group__52326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Track__ActividadesAssignment_5_in_rule__Track__Group__5__Impl2355 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__Track__ActividadesAssignment_5_in_rule__Track__Group__5__Impl2367 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__Track__Group__6__Impl_in_rule__Track__Group__62400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Track__Group__6__Impl2428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__0__Impl_in_rule__Actividad__Group__02473 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Actividad__Group__1_in_rule__Actividad__Group__02476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Actividad__Group__0__Impl2504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__1__Impl_in_rule__Actividad__Group__12535 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__2_in_rule__Actividad__Group__12538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__ComienzoAssignment_1_in_rule__Actividad__Group__1__Impl2565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__2__Impl_in_rule__Actividad__Group__22595 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Actividad__Group__3_in_rule__Actividad__Group__22598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Actividad__Group__2__Impl2626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__3__Impl_in_rule__Actividad__Group__32657 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__4_in_rule__Actividad__Group__32660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__FinAssignment_3_in_rule__Actividad__Group__3__Impl2687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__4__Impl_in_rule__Actividad__Group__42717 = new BitSet(new long[]{0x0000000380000000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__5_in_rule__Actividad__Group__42720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Actividad__Group__4__Impl2748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__5__Impl_in_rule__Actividad__Group__52779 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Actividad__Group__6_in_rule__Actividad__Group__52782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Alternatives_5_in_rule__Actividad__Group__5__Impl2809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__6__Impl_in_rule__Actividad__Group__62839 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__7_in_rule__Actividad__Group__62842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__TituloAssignment_6_in_rule__Actividad__Group__6__Impl2869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__7__Impl_in_rule__Actividad__Group__72899 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Actividad__Group__8_in_rule__Actividad__Group__72902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Actividad__Group__7__Impl2930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__8__Impl_in_rule__Actividad__Group__82961 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__9_in_rule__Actividad__Group__82964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__OradoresAssignment_8_in_rule__Actividad__Group__8__Impl2993 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_rule__Actividad__OradoresAssignment_8_in_rule__Actividad__Group__8__Impl3005 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_rule__Actividad__Group__9__Impl_in_rule__Actividad__Group__93038 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Actividad__Group__10_in_rule__Actividad__Group__93041 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__Actividad__Group__9__Impl3069 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__10__Impl_in_rule__Actividad__Group__103100 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_rule__Actividad__Group__11_in_rule__Actividad__Group__103103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__OyentesAssignment_10_in_rule__Actividad__Group__10__Impl3130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Actividad__Group__11__Impl_in_rule__Actividad__Group__113160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Actividad__Group__11__Impl3188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__0__Impl_in_rule__Organizacion__Group__03243 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__1_in_rule__Organizacion__Group__03246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Organizacion__Group__0__Impl3274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__1__Impl_in_rule__Organizacion__Group__13305 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__2_in_rule__Organizacion__Group__13308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__NameAssignment_1_in_rule__Organizacion__Group__1__Impl3335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__2__Impl_in_rule__Organizacion__Group__23365 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__3_in_rule__Organizacion__Group__23368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Organizacion__Group__2__Impl3396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__Group__3__Impl_in_rule__Organizacion__Group__33427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Organizacion__RazonSocialAssignment_3_in_rule__Organizacion__Group__3__Impl3454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__0__Impl_in_rule__Orador__Group__03492 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Orador__Group__1_in_rule__Orador__Group__03495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Orador__Group__0__Impl3523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__1__Impl_in_rule__Orador__Group__13554 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__Orador__Group__2_in_rule__Orador__Group__13557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__NameAssignment_1_in_rule__Orador__Group__1__Impl3584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__2__Impl_in_rule__Orador__Group__23614 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Orador__Group__3_in_rule__Orador__Group__23617 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Orador__Group__2__Impl3645 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__3__Impl_in_rule__Orador__Group__33676 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Orador__Group__4_in_rule__Orador__Group__33679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__NombreCompletoAssignment_3_in_rule__Orador__Group__3__Impl3706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__Group__4__Impl_in_rule__Orador__Group__43736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Orador__OrganizacionAssignment_4_in_rule__Orador__Group__4__Impl3763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__EventoModel__TitleAssignment_13808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEspacio_in_rule__EventoModel__EspaciosAssignment_43839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrganizacion_in_rule__EventoModel__OrganizacionesAssignment_63870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrador_in_rule__EventoModel__OradoresAssignment_83901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDia_in_rule__EventoModel__DiasAssignment_93932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Dia__DiaAssignment_13963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTrack_in_rule__Dia__BloquesAssignment_33994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Espacio__NameAssignment_14025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Espacio__CapacidadAssignment_34056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__Espacio__LaboratorioAssignment_44092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Track__TitleAssignment_14131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Track__EspacioAssignment_34166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleActividad_in_rule__Track__ActividadesAssignment_54201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHOUR_in_rule__Actividad__ComienzoAssignment_14232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHOUR_in_rule__Actividad__FinAssignment_34263 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Actividad__MesaAssignment_5_04299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Actividad__CharlaAssignment_5_14343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__Actividad__TallerAssignment_5_24387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Actividad__TituloAssignment_64426 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Actividad__OradoresAssignment_84461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Actividad__OyentesAssignment_104496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Organizacion__NameAssignment_14527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Organizacion__RazonSocialAssignment_34558 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Orador__NameAssignment_14589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Orador__NombreCompletoAssignment_34620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Orador__OrganizacionAssignment_44655 = new BitSet(new long[]{0x0000000000000002L});

}